<?php 
    session_start();

    include '../Functions/Authentication.php'; //Incluye la función de autentificación
    include '../Models/PISTA_Model.php';//Incluye el modelo de PISTA
    include '../Views/PISTA_Showall.php';//Incluye la vista con la lista de pistas
    include '../Views/PISTA_Add.php';//Incluye la vista para añadir una pista
    include '../Views/PISTA_Edit.php';//Incluye la vista para visualizar una pista en concreto
    
    
    include '../Views/MESSAGE_View.php'; //Incluye la vista de mensaje
    include '../Models/HORARIOSPISTA_Model.php';//Incluye el modelo de HORARIO

function cambiaf_a_mysql($fecha)//Función que cambia el formato de la fecha
{
    $mifecha = explode("/", $fecha);
    $lafecha = $mifecha[2] . "-" . $mifecha[1] . "-" . $mifecha[0];
    return $lafecha;
}

$HORARIO = new HORARIOSPISTA_Model( '', '', '', '', '', '', '', '', '', '', '');//Crea un objeto HORARIO vacío
$fechafin  = $HORARIO->FINFECHA(); //Guarda la variable de la función FINFECHA

if (!IsAuthenticated()){
        new MESSAGE("Debes autenticarte", '../index.php');
}else{
    switch ($_REQUEST['action']){//Estructura de control que realiza un caso en función del valor de action
        //acción showall
        case 'Showall':
            $datos; //crea un objecto del modelo
            $valores; //almacena los datos tras rellenarlos
            
               $PISTA = new PISTA_Model('', '', '');
               $HORARIO = new HORARIOSPISTA_Model( '', '', '', '', '', '', '', '', '', '', '');
            
            
            $datos = $PISTA->AllData(); //Variable que guarda todas las tuplas de una tabla
            $valores = array('idPista', 'idHorario', 'descripcion'); //array con los nombres de los diferentes datos del objeto

            $datos2 = $HORARIO->AllData(); //Variable que guarda todas las tuplas de una tabla
            $valores2 = array('idHorario', 'fecha', 'hora1', 'hora2', 'hora3', 'hora4', 'hora5', 'hora6', 'hora7', 'hora8', 'hora9'); //array con los nombres de los diferentes datos del objeto

            //Crea el showall con los datos a mostrar
            new Pista_Showall($valores, $datos, $valores2, $datos2, '../Controllers/ESCUELA_Controller.php'); //Crea el showall con los datos a mostrar

            break;
        
        //acción añadir
        case 'Add':
                if (!$_POST) { //Si es por get
                $PISTA =  new PISTA_Model('', '', '');
                $datos = $PISTA->AllData();//Guarda todas las tuplas de ESCUELA
                
                new PISTA_Add($datos);//Muestra el Add 

            } else { //Si viene de un form
                
                $pista = new PISTA_Model( '', '1', $_POST['descripcion']);//Crea un objeto ESCUELA
                $respuesta = $pista->ADD();
                
               if ($respuesta == 'error insertando') { //Si hay error en la inserción
                    
                        new MESSAGE('Error en la insercion', '../Controllers/PISTA_Controller.php?action=Showall');

                }else{ //Si hay éxito en la inserción
            
                        new MESSAGE('Insercion realizada correctamente', '../Controllers/PISTA_Controller.php?action=Showall');
                }
            }

            break;  

        //acción eliminar
        case 'Delete':
                
                $PISTA = new PISTA_Model($_GET['idPista'], '', '');
                $valores = $PISTA->RellenaDatos();//Guarda la tupla seleccionada
                $respuesta = $PISTA->DELETE();  

                if ($respuesta == 'Error en la eliminación'){
                        new MESSAGE("no se ha eliminado", '../Controllers/PISTA_Controller.php?action=Showall');
                }
                else{
                        new MESSAGE("Se ha eliminado correctamente", '../Controllers/PISTA_Controller.php?action=Showall');
                }   
            break;

        default:
        break;

        //acción editar
        case 'Edit':

            if (isset($_GET['idPista'])) {
                $PISTA = new PISTA_Model($_GET['idPista'], '', '');
                $valores = $PISTA->RellenaDatos();//Guarda la tupla seleccionada
                $edit = new PISTA_Edit($valores);

            } else {

                $pista = new PISTA_Model( $_REQUEST['idPista'], '', $_REQUEST['descripcion']);

                $respuesta = $pista->EDIT();
                if ($respuesta == 'Error en la inserción') {//Si el Edit no se realizó, muestra el mensaje
                    new MESSAGE("Error en la edicion", '../Controllers/PISTA_Controller.php?action=Showall');//Mensaje de error
                } else {//Si el Edit se realizó
                    new MESSAGE("Edicion relizada con exito", '../Controllers/PISTA_Controller.php?action=Showall');//Mensaje de acción realizada
                }
            }
        break;
        }            
    }

?>