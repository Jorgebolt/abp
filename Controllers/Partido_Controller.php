<?php 
    session_start();

    include '../Functions/Authentication.php'; //Incluye la función de autentificación
    include '../Models/PARTIDO_Model.php';//Incluye el modelo de PARTIDO
    include '../Views/PARTIDO_Showall.php';//Incluye la vista con la lista de partidos
    include '../Views/PARTIDO_Add.php';//Incluye la vista para añadir un partido
    
    include '../Views/MESSAGE_View.php'; //Incluye la vista de mensaje
    include '../Models/USUARIO_Model.php';//Incluye el modelo de USUARIO

function cambiaf_a_mysql($fecha)//Función que cambia el formato de la fecha
{
    $mifecha = explode("/", $fecha);
    $lafecha = $mifecha[2] . "-" . $mifecha[1] . "-" . $mifecha[0];
    return $lafecha;
}

$fechafin  = date("Y-m-d");
//$_SESSION['fechaFin'] = $fechafin->num_rows ;

if (!IsAuthenticated()){
        new MESSAGE("Debes autenticarte", '../index.php');
}else{
    switch ($_REQUEST['action']){//Estructura de control que realiza un caso en función del valor de action
        //acción showall
        case 'Showall':
            $datos; //crea un objecto del modelo
            $valores; //almacena los datos tras rellenarlos
            
               $PARTIDO = new PARTIDO_Model('', '', '', '', '', '', '', '', '');//Crea un objeto PARTIDO vacío
            
            
            $datos = $PARTIDO->AllData(); //Variable que guarda todas las tuplas de una tabla
            $valores = array('idPartido', 'fecha', 'numPart', 'hora', 'login1', 'login2', 'login3', 'login4', 'idPista'); //array con los nombres de los diferentes datos del objeto
            //new Partido_Showall($valores, $datos, '../Controllers/Partido_Controller.php', $fechafin); //Crea el showall con los datos a mostrar
            new Partido_Showall($valores, $datos, '../Controllers/PARTIDO_Controller.php', $fechafin); //Crea el showall con los datos a mostrar

            break;
        
        //acción añadir
        case 'Add':
                if (!$_POST) { //Si es por get
                $PARTIDO = new PARTIDO_Model('', '', '', '', '', '', '', '', '');//Crea un objeto PARTIDO vacío
                $datos = $PARTIDO->AllData();//Guarda todas las tuplas de PARTIDO
                
                new PARTIDO_Add($datos);//Muestra el Add 

            } else { //Si viene de un form
                $fecha = cambiaf_a_mysql($_POST['fecha']);//Guearda la variable de fechaDesde
                $pista = rand(1,10);
                
                $partido = new PARTIDO_Model( '', $fecha, '0', $_POST['hora'], '', '', '', '', $pista);//Crea un objeto PARTIDO
                $respuesta = $partido->ADD();
                
               if ($respuesta == 'error insertando') { //Si hay error en la inserción
                    
                        new MESSAGE('Error en la insercion', '../Controllers/PARTIDO_Controller.php?action=Showall');

                }else{ //Si hay éxito en la inserción
            
                        new MESSAGE('Insercion realizada correctamente', '../Controllers/PARTIDO_Controller.php?action=Showall');
                }
            }

            break;  

        //acción AddPlayer
        case 'AddPlayer':
            $PARTIDO = new PARTIDO_Model($_GET['idPartido'], '', $_GET['numPart'], '', $_GET['login1'], '','', '', '');//Crea un objeto PARTIDO
            $respuesta = $PARTIDO->ADDPLAYER();  

           if ($respuesta == 'error insertando') { //Si hay error en la inserción     
                new MESSAGE('Error en la insercion', '../Controllers/PARTIDO_Controller.php?action=Showall');

            }else{ //Si hay éxito en la inserción
                new MESSAGE('Insercion realizada correctamente', '../Controllers/PARTIDO_Controller.php?action=Showall');
            }
            break;

        //acción DeletePlayer
        case 'DeletePlayer':
            $PARTIDO = new PARTIDO_Model($_GET['idPartido'], '', '', '', $_GET['login1'], $_GET['login2'],
                                         $_GET['login3'], $_GET['login4'], '');//Crea un objeto PARTIDO
            $num = $_GET['numPart'];
            $num--;
            $playerlogin = $_SESSION['login'];
            $respuesta = $PARTIDO->DELETEPLAYER($playerlogin, $num);  

            if ($respuesta == 'Error en la eliminación'){
                        new MESSAGE("no se ha eliminado", '../Controllers/PARTIDO_Controller.php?action=Showall');
                }
                else{
                        new MESSAGE("Se ha eliminado correctamente", '../Controllers/PARTIDO_Controller.php?action=Showall');
                }
            break;

        //acción eliminar
        case 'Delete':
                
                $PARTIDO = new PARTIDO_Model($_GET['idPartido'], '', '', '', '', '', '', '', '');//Crea un objeto PARTIDO
                $valores = $PARTIDO->RellenaDatos();//Guarda la tupla seleccionada
                $respuesta = $PARTIDO->DELETE();  

                if ($respuesta == 'Error en la eliminación'){
                        new MESSAGE("no se ha eliminado", '../Controllers/PARTIDO_Controller.php?action=Showall');
                }
                else{
                        new MESSAGE("Se ha eliminado correctamente", '../Controllers/PARTIDO_Controller.php?action=Showall');
                }   
            break;

        default:
        break;
        }            
    }

?>