<?php

    session_start();

    include '../Functions/Authentication.php';
    include '../Models/CAMPEONATO_Model.php';
    include '../Models/GRUPO_Model.php';
    include '../Models/PAREJA_Model.php';
    include '../Models/PLAYOFF_Model.php';
    include '../Views/CAMPEONATO_Showall.php';
    include '../Views/CAMPEONATO_Showcurrent.php';
    include '../Views/PLAYOFF_Enfrentamientos.php';
    include '../Views/GANADOR_PLAYOFF.php';
    include '../Views/PLAYOFF_Enfrentamientos_Fase2.php';
    include '../Views/PLAYOFF_Enfrentamientos_Final.php';
    include '../Views/MESSAGE_View.php';

    function cambiaf_a_mysql($fecha) //Funcion que cambia el formato de la fecha
    {
        $mifecha = explode("/", $fecha);
        $lafecha = $mifecha[2] . "-" . $mifecha[1] . "-" . $mifecha[0];
        return $lafecha;
    }

    if(!IsAuthenticated()){
        new MESSAGE("Debes autenticarte", '../index.php');
    }else{
        switch ($_REQUEST['action']) {
            case 'ShowCruces':

                $PLAYOFF = new PLAYOFF_Model($_REQUEST['idPlayoff'], $_REQUEST['idGrupo'], $_REQUEST['idCampeonato'], $_REQUEST['categoria'], $_REQUEST['nivel'], NULL);
                
                $parejas = $PLAYOFF->getParejas();

                $usuario = $_SESSION['login'];
                $estaInscrito = $PLAYOFF->estaInscrito($usuario, $parejas);

                $hayGanador = $PLAYOFF->getGanador();

                if(!$hayGanador)
                    echo 'NO HAY GANADOR AUN';
                else
                    echo 'HAY GANADOR, ES ' . $hayGanador;

                if(!$estaInscrito && $_SESSION['login'] != 'root'){
                    $volverA = '../Controllers/CAMPEONATO_Controller.php?action=Showcurrent&idCampeonato=' . $_REQUEST['idCampeonato'];
                    new MESSAGE('No estas inscrito en este playoff', $volverA);

                }else{
                    
                    //recuperamos las parejas de cada enfrentamiento
                    /*
                    $enfrentamiento1 = $PLAYOFF->getEnfrentamiento1();
                    $enfrentamiento2 = $PLAYOFF->getEnfrentamiento2();
                    $enfrentamiento3 = $PLAYOFF->getEnfrentamiento3();
                    $enfrentamiento4 = $PLAYOFF->getEnfrentamiento4();
                    */
                    
                    $parejas = $PLAYOFF->getParejas();
                    $parejasCruce1 = $PLAYOFF->getParejas();
                    $parejasCruce2 = $PLAYOFF->getParejas();
                    $parejasCruce3 = $PLAYOFF->getParejas();
                    $parejasCruce4 = $PLAYOFF->getParejas();
                    
                    new PLAYOFF_Enfrentamientos($parejas, $estaInscrito, $parejasCruce1, $parejasCruce2, $parejasCruce3, $parejasCruce4);
                }

            break;

            case 'RegistrarResultadoJornada1':
               
                $cruce1_ganadora = $_REQUEST['cruce1_ganadora'];
                $cruce2_ganadora = $_REQUEST['cruce2_ganadora'];
                $cruce3_ganadora = $_REQUEST['cruce3_ganadora'];
                $cruce4_ganadora = $_REQUEST['cruce4_ganadora'];
                
                $PLAYOFF = new PLAYOFF_Model($_REQUEST['idPlayoff'], $_REQUEST['idGrupo'], $_REQUEST['idCampeonato'], NULL, NULL, NULL);
                
                $parejas = $PLAYOFF->getParejas();
                
                //echo 'ganadores = ' . $cruce1_ganadora . ' // ' . $cruce2_ganadora . ' // ' . $cruce3_ganadora . ' // ' . $cruce4_ganadora;
                //echo 'idCampeonato = ' . $_REQUEST['idCampeonato'];
                //echo 'idGrupo = ' . $_REQUEST['idGrupo'];
                //echo 'idPlayOff = ' . $_REQUEST['idPlayoff'];

                new PLAYOFF_Enfrentamientos_Fase2($parejas, $cruce1_ganadora, $cruce2_ganadora, $cruce3_ganadora, $cruce4_ganadora);
            
                break;

            case 'RegistrarResultadoJornada2':

                $cruce1_ganadora = $_REQUEST['cruce1_ganadora'];
                $cruce2_ganadora = $_REQUEST['cruce2_ganadora'];

                //echo 'ganadores = ' . $cruce1_ganadora . ' // ' . $cruce2_ganadora;

                $PLAYOFF = new PLAYOFF_Model($_REQUEST['idPlayoff'], $_REQUEST['idGrupo'], $_REQUEST['idCampeonato'], NULL, NULL, NULL);

                $parejas = $PLAYOFF->getParejas();

                new PLAYOFF_Enfrentamientos_Final($parejas, $cruce1_ganadora, $cruce2_ganadora);

                break;

            case 'RegistrarResultadoFinal':

                $ganador_playoff = $_REQUEST['ganador_playoff'];

                $PLAYOFF = new PLAYOFF_Model($_REQUEST['idPlayoff'], $_REQUEST['idGrupo'], $_REQUEST['idCampeonato'], NULL, NULL, NULL);

                $setGanador = $PLAYOFF->setGanador($ganador_playoff);
                
                $volverA = './CAMPEONATO_Controller.php?action=Showcurrent&idCampeonato=' . $_REQUEST['idCampeonato'];
                new MESSAGE('Ganador de playoff registrado con exito', $volverA);

                break;

            case 'ShowGanador':

                $ganador = $_REQUEST['ganador'];

                new GANADOR_PLAYOFF($ganador);

                break;

            default:
                echo 'ESTAS EN EL DEFAULT DE PLAYOFF_Controller';
                
                break;
        }
    }
?>