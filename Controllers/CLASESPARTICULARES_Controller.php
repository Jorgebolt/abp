<?php

    session_start();

    include '../Functions/Authentication.php';
    include '../Models/CLASESPARTICULARES_Model.php';
    include '../Models/HORARIOS_Model.php';
    include '../Models/USUARIO_Model.php';
    
    
    
    include '../Views/MESSAGE_View.php';

    
    function cambiaf_a_mysql($fecha) //Funcion que cambia el formato de la fecha
    {
        $mifecha = explode("/", $fecha);
        $lafecha = $mifecha[2] . "-" . $mifecha[1] . "-" . $mifecha[0];
        return $lafecha;
    }

    

    //$RESERVAPISTA = new RESERVAPISTA_Model('', '', ''); //Crea un objeto vacio
    if(!IsAuthenticated()){
        new MESSAGE("Debes autenticarte", '../index.php');
    }else{
        if(!isset($_REQUEST['action'])){ // Si no tiene ninguna opcion mustra la tabla SHOWALL.
            
                include '../Views/CLASESPARTICULARES_Showall.php';
                $objeto2 = new USUARIO_Model('','','','','','','');
                $resultado2 = $objeto2->devolverDeportista();
                $objeto = new CLASESPARTICULARES_Model('','','','','','');
                $resultado = $objeto->AllData2();

                new CLASESPARTICULARES_Showall($resultado,$resultado2);
            
        
        
    }else{
        $action=$_REQUEST['action'];
        if($action =='empezar'){ 


            $objeto = new CLASESPARTICULARES_Model('','','','','','');
            $resultado = $objeto->AllData();
            include '../Views/CLASESPARTICULARES_View.php';
            
            new CLASESPARTICULARES_View($resultado);

        }
        if($action =='ver'){ 
            if($_SESSION['login'] == 'root'){
                
                $objeto = new CLASESPARTICULARES_Model('','','','','');
                $resultado = $objeto->AllData2();
                include '../Views/CLASESPARTICULARES_View.php';
            
            new RESERVASREALIZADAS_View($resultado);
            }else{
                //$login = $_SESSION['login'];
                $objeto = new CLASESPARTICULARES_Model('','','','','','');
                $resultado = $objeto->AllData2();
                $objeto2 = new USUARIO_Model('','','','','','','');
                $resultado2 = $objeto2->devolverDeportista();
                include '../Views/CLASESPARTICULARES_Showall.php';
            
                new CLASESPARTICULARES_Showall($resultado,$resultado2);
            }
            
        }
        if($action =='delete'){ 


            $CLASE = new CLASESPARTICULARES_Model($_REQUEST['idClase'],'','','','',''); //crea un modelo
                        $respuesta = $CLASE->DELETE();//devuelve true o false

                        if ($respuesta) { 
                            new MESSAGE('exito eliminando', '../Controllers/CLASESPARTICULARES_Controller.php?action=ver'); //Mensaje de error
                        } else {
                            new MESSAGE('error eliminando', '../Controllers/CLASESPARTICULARES_Controller.php?action=ver'); //Mensaje de borrado exitoso
                        }   
        }
        if($action =='add'){ 

            //$hora = $horaInicio;
            $objeto2 = new USUARIO_Model('','','','','','','');
            $posicionEntrenador = rand(1,10);
            $entrenadores = $objeto2->devolverEntrenador();
            $cont = 0;
            while (($row = $entrenadores->fetch_array()) && ($cont<$posicionEntrenador)) {
                $entrenador = $row['login'];
                $cont++;
            }
            $login = $_SESSION['login'];
            $pista = rand(1,10);
            $objeto = new CLASESPARTICULARES_Model('',$login,$entrenador,$_GET['fecha'],$_GET['horaInicio'],$pista);
            $resultado = $objeto->ADD();
            //include '../Views/CLASESPARTICULARES_Showall.php';
            
            //new CLASESPARTICULARES_Showall($resultado);
            new MESSAGE($resultado, '../Controllers/CLASESPARTICULARES_Controller.php?action=ver'); //Mensaje de acción realizada

        }

        if($action =='add2'){ 

            //$login = $_SESSION['login'];
            $fecha = cambiaf_a_mysql($_POST['fecha']);
            //$objeto2 = new CLASESPARTICULARES_Model('',$login,'',$fecha,'','');
            //$resultado2 = $objeto2->ADD();
            $objeto = new HORARIOS_Model('','','',$fecha);
            $objeto->UPDATE();
            $resultado = $objeto->AllData2();
            include '../Views/HORARIOS_Showall.php';

            new HORARIOS_Showall($resultado);

        }
    }
    }
?>