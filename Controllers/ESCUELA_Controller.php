<?php 
    session_start();

    include '../Functions/Authentication.php'; //Incluye la función de autentificación
    include '../Models/ESCUELA_Model.php';//Incluye el modelo de ESCUELA
    include '../Views/ESCUELA_Showall.php';//Incluye la vista con la lista de escuelas deportivas
    include '../Views/ESCUELA_Add.php';//Incluye la vista para añadir una escuela deportiva
    include '../Views/ESCUELA_Showcurrent.php';//Incluye la vista para visualizar una escuela en concreto
    include '../Views/ESCUELA_Edit.php';//Incluye la vista para modificar una escuela en concreto
    
    include '../Views/MESSAGE_View.php'; //Incluye la vista de mensaje
    include '../Models/USUARIO_Model.php';//Incluye el modelo de USUARIO

function cambiaf_a_mysql($fecha)//Función que cambia el formato de la fecha
{
    $mifecha = explode("/", $fecha);
    $lafecha = $mifecha[2] . "-" . $mifecha[1] . "-" . $mifecha[0];
    return $lafecha;
}

$fechafin  = date("Y-m-d");

if (!IsAuthenticated()){
        new MESSAGE("Debes autenticarte", '../index.php');
}else{
    switch ($_REQUEST['action']){//Estructura de control que realiza un caso en función del valor de action
        //acción showall
        case 'Showall':
            $datos; //crea un objecto del modelo
            $valores; //almacena los datos tras rellenarlos
            
               $ESCUELA = new ESCUELA_Model('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');//Crea un objeto ESCUELA vacío
            
            
            $datos = $ESCUELA->AllData(); //Variable que guarda todas las tuplas de una tabla
            $valores = array('idEscuela', 'nombre', 'telefono', 'dia', 'hora', 'idPista', 'numPart', 'nivel', 'loginEnt', 'login1', 'login2', 'login3', 'login4', 'login5', 'login6', 'login7', 'login8'); //array con los nombres de los diferentes datos del objeto

            //Crea el showall con los datos a mostrar
            new Escuela_Showall($valores, $datos, '../Controllers/ESCUELA_Controller.php', $fechafin); //Crea el showall con los datos a mostrar

            break;
        
        //acción añadir
        case 'Add':
                if (!$_POST) { //Si es por get
                $ESCUELA = new ESCUELA_Model('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
                $datos = $ESCUELA->AllData();//Guarda todas las tuplas de ESCUELA
                
                new ESCUELA_Add($datos);//Muestra el Add 

            } else { //Si viene de un form
                $fecha = cambiaf_a_mysql($_POST['fecha']);//Guearda la variable de fechaDesde
                $entrenador = $_SESSION['login'];
                
                $escuela = new ESCUELA_Model( '', $_POST['nombre'], $_POST['telefono'], $fecha, $_POST['hora'], $_POST['idPista'], '0', $_POST['nivel'], $entrenador, '', '', '', '', '', '', '', '');//Crea un objeto ESCUELA
                $respuesta = $escuela->ADD();
                
               if ($respuesta == 'error insertando') { //Si hay error en la inserción
                    
                        new MESSAGE('Error en la insercion', '../Controllers/ESCUELA_Controller.php?action=Showall');

                }else{ //Si hay éxito en la inserción
            
                        new MESSAGE('Insercion realizada correctamente', '../Controllers/ESCUELA_Controller.php?action=Showall');
                }
            }

            break;  

        //acción Showcurrent
        case 'Showcurrent':
            $ESCUELA = new ESCUELA_Model($_GET['idEscuela'], '', '', '', '', '', '', '', '', '', '','', '', '', '', '', '');
            $datos = $ESCUELA->RellenaDatos();//Guarda la tupla seleccionada

            $showc = new ESCUELA_Showcurrent($datos);
            break;

        //acción AddPlayer
        case 'AddPlayer':
            $ESCUELA = new ESCUELA_Model($_GET['idEscuela'], '', '', '', '', '', $_GET['numPart'], '', '', $_GET['login1'], '','', '', '', '', '', '');
            $respuesta = $ESCUELA->ADDPLAYER();  

           if ($respuesta == 'error insertando') { //Si hay error en la inserción     
                new MESSAGE('Error en la insercion', '../Controllers/ESCUELA_Controller.php?action=Showall');

            }else{ //Si hay éxito en la inserción
                new MESSAGE('Insercion realizada correctamente', '../Controllers/ESCUELA_Controller.php?action=Showall');
            }
            break;

        //acción DeletePlayer
        case 'DeletePlayer':
            $ESCUELA = new ESCUELA_Model($_GET['idEscuela'], '', '', '', '', '', '', '', '', $_GET['login1'], $_GET['login2'], $_GET['login3'], $_GET['login4'], $_GET['login5'], $_GET['login6'], $_GET['login7'], 
                $_GET['login8']);
            $num = $_GET['numPart'];
            $num--;
            $playerlogin = $_SESSION['login'];
            $respuesta = $ESCUELA->DELETEPLAYER($playerlogin, $num);  

            if ($respuesta == 'Error en la eliminación'){
                        new MESSAGE("no se ha eliminado", '../Controllers/ESCUELA_Controller.php?action=Showall');
                }
                else{
                        new MESSAGE("Se ha eliminado correctamente", '../Controllers/ESCUELA_Controller.php?action=Showall');
                }
            break;

        //acción eliminar
        case 'Delete':
                
                $ESCUELA = new ESCUELA_Model($_GET['idEscuela'], '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
                $valores = $ESCUELA->RellenaDatos();//Guarda la tupla seleccionada
                $respuesta = $ESCUELA->DELETE();  

                if ($respuesta == 'Error en la eliminación'){
                        new MESSAGE("no se ha eliminado", '../Controllers/ESCUELA_Controller.php?action=Showall');
                }
                else{
                        new MESSAGE("Se ha eliminado correctamente", '../Controllers/ESCUELA_Controller.php?action=Showall');
                }   
            break;

        default:
        break;

        //acción editar
        case 'Edit':

            if (isset($_GET['idEscuela'])) {
                $ESCUELA = new ESCUELA_Model($_GET['idEscuela'], '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
                $valores = $ESCUELA->RellenaDatos();//Guarda la tupla seleccionada
                $edit = new ESCUELA_Edit($valores);

            } else {
                $fecha = cambiaf_a_mysql($_REQUEST['fecha']);//Guarda la variable de fecha
                $entrenador = $_SESSION['login'];
                
                $escuela = new ESCUELA_Model( $_REQUEST['idEscuela'], $_REQUEST['nombre'], $_REQUEST['telefono'], 
                        $fecha, $_REQUEST['hora'], $_REQUEST['idPista'], $_REQUEST['numPart'], $_REQUEST['nivel'], 
                        $entrenador, $_REQUEST['login1'], $_REQUEST['login2'], $_REQUEST['login3'], 
                        $_REQUEST['login4'], $_REQUEST['login5'], $_REQUEST['login6'], $_REQUEST['login7'], 
                        $_REQUEST['login8']);

                $respuesta = $escuela->EDIT();
                if ($respuesta == 'Error en la inserción') {//Si el Edit no se realizó, muestra el mensaje
                    new MESSAGE("Error en la edicion", '../Controllers/ESCUELA_Controller.php?action=Showall');//Mensaje de error
                } else {//Si el Edit se realizó
                    new MESSAGE("Edicion relizada con exito", '../Controllers/ESCUELA_Controller.php?action=Showall');//Mensaje de acción realizada
                }
            }
        break;
        }            
    }

?>