<?php

    session_start();

    include '../Functions/Authentication.php';
    include '../Models/CAMPEONATO_Model.php';
    include '../Views/CAMPEONATO_Showall.php';
    include '../Views/CAMPEONATO_Showcurrent.php';
    include '../Models/GRUPO_Model.php';
    include '../Views/GRUPO_Showall.php';
    include '../Views/GRUPO_Showcurrent.php';
    include '../Views/GRUPO_Clasificacion.php';
    include '../Views/APUNTAR_RESULTADO.php';
    include '../Models/PAREJA_Model.php';
    include '../Views/PAREJA_Showall.php';
    include '../Views/LIGAREGULAR_Showall.php';
    include '../Views/MESSAGE_View.php';

    function cambiaf_a_mysql($fecha) //Funcion que cambia el formato de la fecha
    {
        $mifecha = explode("/", $fecha);
        $lafecha = $mifecha[2] . "-" . $mifecha[1] . "-" . $mifecha[0];
        return $lafecha;
    }

    if(!IsAuthenticated()){
        new MESSAGE("Debes autenticarte", '../index.php');
    }else{
        switch ($_REQUEST['action']) {
            case 'Showall':

                $GRUPO = new GRUPO_Model('', '', '', '', null); //Crea un objeto vacio
                $datos = $GRUPO->AllData();

                new GRUPO_Showall($datos);

                break;

            case 'Showcurrent': //vista donde se ajusta que dos parejas juegan

                $GRUPO = new GRUPO_Model($_REQUEST['idGrupo'], $_REQUEST['categoria'], $_REQUEST['nivel'], $_REQUEST['idCampeonato'], null);

                $parejasFila = $GRUPO->getParejas();
                $parejasColumna= $GRUPO->getParejas();
                $idCampeonato = $_REQUEST['idCampeonato'];
                $idGrupo = $_REQUEST['idGrupo'];
                
                $parejas = $GRUPO->getParejas();
                $usuario = $_SESSION['login'];
                $estaInscrito = $GRUPO->estaInscrito($usuario, $parejas);

                if(!$estaInscrito){
                    $volverA = '../Controllers/CAMPEONATO_Controller.php?action=Showcurrent&idCampeonato=' . $idCampeonato;
                    new MESSAGE('No estas inscrito en este grupo', $volverA);

                }else{
                    new GRUPO_Showcurrent($parejasFila, $parejasColumna, $idCampeonato, $idGrupo, $estaInscrito);
                }
                
                break;

            case 'ApuntarGanador': //vista donde se ajusta el ganador del partido
                $parejaUser = $_REQUEST['parejaUser'];
                $parejaRival = $_REQUEST['parejaRival'];

                //recupera los nombres de la pareja del usuario
                $arrayParejaUser = explode('-', $parejaUser);
                $login1_parejaUser = $arrayParejaUser[0];
                $login2_parejaUser = $arrayParejaUser[1];

                //recupera los nombres de la pareja rival
                $arrayParejaRival = explode('-', $parejaRival);
                $login1_parejaRival = $arrayParejaRival[0];
                $login2_parejaRival = $arrayParejaRival[1];

                $idCampeonato = $_REQUEST['idCampeonato'];
                
                //envia los nombres a una vista para elegir el ganador
                if($parejaRival == ''){ //siempre que haya seleccionado una pareja
                    $volverA = '../Controllers/CAMPEONATO_Controller.php?action=Showcurrent&idCampeonato=' . $idCampeonato;
                    new MESSAGE('No has seleccionado la pareja rival', $volverA);

                }else{
                    new APUNTAR_RESULTADO($login1_parejaUser, $login2_parejaUser, $login1_parejaRival, $login2_parejaRival);
                }

                break;

            case 'RegistrarResultado':
                
                $parejaGanadora = $_REQUEST['pareja_ganadora'];
                $arrayParejaGanadora = explode(' ', $parejaGanadora);

                $login1_ganador = $arrayParejaGanadora[0];
                $login2_ganador = $arrayParejaGanadora[1];

                $idCampeonato = $_REQUEST['idCampeonato'];
                $idGrupo = $_REQUEST['idGrupo'];

                //Ruta para volver al showcurrent del campeonato que se esta consultando
                $volverA = '../Controllers/CAMPEONATO_Controller.php?action=Showcurrent&idCampeonato=' . $idCampeonato;

                if($parejaGanadora == ''){ //siempre que haya seleccionado una pareja ganadora
                    new MESSAGE('No has seleccionado la pareja ganadora', $volverA);

                }else{
                    //recupera la pareja ganadora
                    $GANADORA = new PAREJA_Model($idCampeonato, $idGrupo, null, $login1_ganador, $login2_ganador, '','', NULL, null);

                    //se le suman 3 puntos de la victoria
                    $sumarVictoria = $GANADORA->sumarVictoria();

                    //recupera la pareja perdedora
                    
                    $arraypareja1 = explode(' ', $_REQUEST['pareja1']);

                    //si el de la pareja 1 es el que gano
                    if ($login1_ganador == $arraypareja1[0]){

                        //la pareja 2 fue la perdedora
                        $arraypareja2 = explode(' ', $_REQUEST['pareja2']);
                        
                        $login1_perdedor = $arraypareja2[0];
                        $login2_perdedor = $arraypareja2[1];

                    }else{ //sino, la pareja 1 fue la perdedora
                        $login1_perdedor = $arraypareja1[0];
                        $login2_perdedor = $arraypareja1[1];
                    }

                    $PERDEDORA = new PAREJA_Model($idCampeonato, $idGrupo, null, $login1_perdedor,$login2_perdedor, '','', NULL, null);

                    //se le suma 1 punto de la derrota
                    $sumarDerrota = $PERDEDORA->sumarDerrota();

                    //si se sumo todo con exito
                    if($sumarVictoria && $sumarDerrota){
                        new MESSAGE('Resultado registrado con exito', $volverA);
                    }
                }
                
                break;

            case 'Add':

                echo 'ESTAS EN EL ADD DE GRUPO_Controller';
                break;

            case 'Clasificacion':

                $GRUPO = new GRUPO_Model($_REQUEST['idGrupo'], $_REQUEST['categoria'], $_REQUEST['nivel'], $_REQUEST['idCampeonato'], null);

                $parejasCheck = $GRUPO->getParejas();
                $usuario = $_SESSION['login'];
                $estaInscrito = $GRUPO->estaInscrito($usuario, $parejasCheck);

                $parejas = $GRUPO->getParejas();

                $idCampeonato = $_REQUEST['idCampeonato'];
                
                //si no esta inscrito y no es el root
                if(!$estaInscrito && $_SESSION['login'] != 'root'){
                    $volverA = '../Controllers/CAMPEONATO_Controller.php?action=Showcurrent&idCampeonato=' . $idCampeonato;
                    new MESSAGE('No estas inscrito en este grupo', $volverA);

                }else{
                    //si esta inscrito o es el root, puede ver la clasificacion
                    new GRUPO_Clasificacion($parejas);
                }

                break;

            default:
                echo 'ESTAS EN EL DEFAULT DE GRUPO_Controller';
                
                break;
        }
    }
?>