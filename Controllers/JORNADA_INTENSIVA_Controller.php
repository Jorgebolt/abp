<?php

    session_start();

    include '../Views/MESSAGE_View.php';
    include '../Functions/Authentication.php';
    include '../Models/USUARIO_Model.php';
    include '../Models/JORNADA_INTENSIVA_Model.php';
    include '../Views/JORNADA_INTENSIVA_Showall.php';
    include '../Views/JORNADA_INTENSIVA_Delete.php';
    include '../Views/JORNADA_INTENSIVA_Add.php';

    function cambiaf_a_mysql($fecha) //Funcion que cambia el formato de la fecha
    {
        $mifecha = explode("/", $fecha);
        $lafecha = $mifecha[2] . "-" . $mifecha[1] . "-" . $mifecha[0];
        return $lafecha;
    }

    if(!IsAuthenticated()){
        new MESSAGE("Debes autenticarte", '../index.php');
    }else{
        switch ($_REQUEST['action']) {
            
            case 'Showall':
                $JORNADA = new JORNADA_INTENSIVA_Model(null, null, null, null, null);

                $datos = $JORNADA->AllData();

                new JORNADA_INTENSIVA_Showall($datos);

                break;

            case 'Delete':

                $JORNADA = new JORNADA_INTENSIVA_Model($_REQUEST['idJornada'], '', '', '', '');

                $datos = $JORNADA->RellenaDatos();//Guarda la tupla seleccionada

                $borrado = $JORNADA->DELETE();

                if ($respuesta){
                    new MESSAGE('no se ha eliminado', '../Controllers/JORNADA_INTENSIVA_Controller.php?action=Showall');
                }
                else{
                    new MESSAGE('Se ha eliminado correctamente', '../Controllers/JORNADA_INTENSIVA_Controller.php?action=Showall');
                }
                break;

            case 'Add':
            if (!$_POST) { //Si es por get

                $JORNADA = new JORNADA_INTENSIVA_Model('', '', '', '', '');

                $usuarios = $JORNADA->getUsuarios();
                $entenadores = $JORNADA->getEntenadores();

                new JORNADA_INTENSIVA_Add($usuarios, $entenadores); //Muestra el Add 

            } else { //Si viene de un form

                $fecha = cambiaf_a_mysql($_POST['fecha']);//Guearda la variable de fechaDesde
                
                $JORNADA = new JORNADA_INTENSIVA_Model('', $_POST['nivel'], $_POST['loginUser'], $_POST['loginEntrenador'], $fecha);

                $crear = $JORNADA->ADD();

                if ($respuesta) { //Si hay error en la inserción
                    new MESSAGE('Error en la insercion', '../Controllers/JORNADA_INTENSIVA_Controller.php?action=Showall');
                
                } else { //Si hay éxito en la inserción

                    new MESSAGE('Insercion realizada correctamente', '../Controllers/JORNADA_INTENSIVA_Controller.php?action=Showall');
                }
            }

                

                break;

            default:
                echo 'ESTAS EN EL DEFAULT DE JORNADA_INTENSIVA_Controller';
                break;
        }
    }
?>