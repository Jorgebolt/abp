<?php

    session_start();

    include '../Functions/Authentication.php';
    include '../Models/USUARIO_Model.php';
    include '../Views/MESSAGE_View.php';
	include '../Views/USUARIO_Showall.php';
        include '../Views/USUARIO_Showcurrent.php';
    include '../Views/USUARIO_Add.php';
        include '../Views/USUARIO_Edit.php';
    include '../Views/Inicio.php';//Incluye la vista con la pantalla inicial
   

    if(!IsAuthenticated()){
        new Inicio('../Controllers/USUARIO_Controller.php'); 
    }else{
        switch ($_REQUEST['action']) {
           
            case 'Add':
            if(!$_POST){
                    new USUARIO_Add();
                }else{ //si viene de cubrir el formulario
              

                    $USUARIO = new USUARIO_Model($_REQUEST['login'],$_REQUEST['password'],$_REQUEST['nombre'],$_REQUEST['apellidos'],$_REQUEST['email'],$_REQUEST['sexo'],$_REQUEST['telefono']);

                        $respuesta = $USUARIO->AddUser();

                        $USUARIO->ChangePermisos(isset($_REQUEST['administrador']),isset($_REQUEST['entrenador']),isset($_REQUEST['deportista'])); 
                        new MESSAGE($respuesta, '../Controllers/USUARIO_Controller.php?action=Showall'); //Mensaje de acción realizada
                    
                }
                break;

            case 'Showcurrent':
               if ($_SESSION['login'] != 'root') {//cambiar a comprobar que es admin o root(por si te quedas sin admins).
                    new MESSAGE('Acceso denegado', '../Controllers/USUARIO_Controller.php');
                } else { //si eres admin
                $USUARIO = new USUARIO_Model($_REQUEST['login'],'','','','','','');
                $datos = $USUARIO->SHOWCURENT();
                new USUARIO_Showcurrent($datos);
                }
                break;

            case 'Edit'://falta añadir checked a los edits
               if ($_SESSION['login'] != 'root') {//cambiar a comprobar que es admin o root(por si te quedas sin admins).
                    new MESSAGE('Acceso denegado', '../Controllers/USUARIO_Controller.php');
                } else { //si eres admin
             if(!isset($_REQUEST['password'])){
                     $USUARIO = new USUARIO_Model($_REQUEST['login'],'','','','','','');
                       $datos = $USUARIO->SHOWCURENT();
                       new USUARIO_Edit($datos);
                 }else{
                     $USUARIO = new USUARIO_Model($_REQUEST['login'],$_REQUEST['password'],$_REQUEST['nombre'],$_REQUEST['apellidos'],$_REQUEST['email'],$_REQUEST['sexo'],$_REQUEST['telefono']);
                       $respuesta = $USUARIO->EDIT();

                        $USUARIO->ChangePermisos(isset($_REQUEST['administrador']),isset($_REQUEST['entrenador']),isset($_REQUEST['deportista'])); 
                        new MESSAGE($respuesta, '../Controllers/USUARIO_Controller.php?action=Showall'); //Mensaje de acción 
                 }
             }
                break;

            case 'Search':
                
                break;

            case 'Delete':
                 if ($_SESSION['login'] != 'root') {//cambiar a comprobar que es admin o root(por si te quedas sin admins).
                    new MESSAGE('Acceso denegado', '../Controllers/USUARIO_Controller.php');
                } else { //si eres admin


                        $USUARIO = new USUARIO_Model($_REQUEST['login'],'','','','','',''); //crea un modelo de contrato
                        $respuesta = $USUARIO->DELETE();//devuelve true o false

                        if ($respuesta) { 
                            new MESSAGE('exito eliminando', '../Controllers/USUARIO_Controller.php?action=Showall'); //Mensaje de error
                        } else {
                            new MESSAGE('error eliminando', '../Controllers/USUARIO_Controller.php?action=Showall'); //Mensaje de borrado exitoso
                        }   
                    }
                
                break;

             case 'ChangeType'://puede que ponerlo como parte de eddit y de add
                if ($_SESSION['login'] != 'root') {//cambiar a comprobar que es admin
                    new MESSAGE('Acceso denegado', '../Controllers/USUARIO_Controller.php');
                } else { //si eres admin


                        $USUARIO = new USUARIO_Model($_REQUEST['login'],'','','','','',''); //crea un modelo de contrato
                        $respuesta = $USUARIO->ChangePermisos($_REQUEST['permisos']);//devuelve true o false

                        if ($respuesta) { 
                            new MESSAGE('exito insercion', '../Controllers/USUARIO_Controller.php?action=Showall'); //Mensaje de error
                        } else {
                            new MESSAGE('error insercion', '../Controllers/USUARIO_Controller.php?action=Showall'); //Mensaje de borrado exitoso
                        }   
                    }
                break;

            case 'Inicio':
                new Inicio('../Controllers/USUARIO_Controller.php'); 
                break;

            default:
                    
                $USUARIO = new USUARIO_Model('','','','','','','');

                $datos = $USUARIO->SHOWALL();

                new USUARIO_Showall($datos);

                 
                    
                
                break;
        }
    }
?>