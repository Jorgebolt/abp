<?php
	
	session_start();
	
		include '../Views/MESSAGE_View.php';
		if(isset($_GET['reject'])){
			new MESSAGE('Aun no te ha registrado el administrador', './Login_Controller.php');	
		}
		else{
			if(!isset($_REQUEST['login']) && !(isset($_REQUEST['password']))){
				include '../Views/Login_View.php';
				$login = new Login();
			}
			else{

				include '../Models/Access_DB.php';
				include '../Models/USUARIO_Model.php';
				$usuario = new USUARIO_Model($_REQUEST['login'],$_REQUEST['password'],'','','','','');
				$respuesta = $usuario->login();
				
				if ($respuesta == 'true'){
					if (session_status() == PHP_SESSION_NONE) 
					session_start();

					$_SESSION['login'] = $usuario->getLogin();
					
					//header('Location:../index.php');
					include '../Views/Inicio.php';
					$login = new Inicio();
				}
				else{
					new MESSAGE('Datos incorrectos o tu peticion ha sido rechazada', './Login_Controller.php');
				}
			}
		}
