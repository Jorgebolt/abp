<?php

    session_start();

    include '../Functions/Authentication.php';
    include '../Models/LIGAREGULAR_Model.php';
    include '../Views/LIGAREGULAR_Showall.php';
    include '../Views/LIGAREGULAR_Showcurrent.php';
    include '../Views/MESSAGE_View.php';

    function cambiaf_a_mysql($fecha) //Funcion que cambia el formato de la fecha
    {
        $mifecha = explode("/", $fecha);
        $lafecha = $mifecha[2] . "-" . $mifecha[1] . "-" . $mifecha[0];
        return $lafecha;
    }

    if(!IsAuthenticated()){
        new MESSAGE("Debes autenticarte", '../index.php');
    }else{
        switch ($_REQUEST['action']) {
            case 'Showall':
                $LIGAREGULAR = new LIGAREGULAR_Model('', '', '', '', '', '', '', '', '', '', ''); //Crea un objeto vacio
                $datos = $LIGAREGULAR->AllData();

                new LIGAREGULAR_Showall($datos);

                break;

            case 'Add':
                
                if ($_SESSION['login'] != 'root') {
                    new MESSAGE('Acceso denegado', '../Controllers/CAMPEONATO_Controller.php');
                } else { //si eres el root
                    //crea una liga asociada al campeonato que se esta visualizando
                    $LIGAREGULAR = new LIGAREGULAR_Model('', $_REQUEST['idCampeonato'], '', '', '', '', '', '', '', '', '');

                    $result = $LIGAREGULAR->ADD();

                    if($result){

                        header('Location:./CAMPEONATO_Controller.php?action=Showcurrent&idCampeonato=' . $_REQUEST['idCampeonato']);
                    }else{

                        new MESSAGE('error insertando', '../Controllers/CAMPEONATO_Controller.php?action=Showall');
                    }
                }
                
                break;
            
            case 'Showcurrent':
                $LIGAREGULAR = new LIGAREGULAR_Model($_REQUEST['idLiga'], '', '', '', '', '', '', '', '', '', '');
                $datos = $LIGAREGULAR->RellenaDatos();

                $showc = new LIGAREGULAR_Showcurrent($datos);

                break;

            case 'AddPlayer':
                //PENDIENTE AÑADIR VARIABLE numPart A LAS LIGAS
                $LIGAREGULAR = new LIGAREGULAR_Model($_GET['idLiga'], '', $_GET['numPart'], $_GET['login1'], '', '', '', '', '', '', '');
                
                $respuesta = $LIGAREGULAR->ADDPLAYER();

                if ($respuesta == 'error insertando') { //Si hay error en la inserción     
                    new MESSAGE('Error en la insercion', '../Controllers/PARTIDO_Controller.php?action=Showall');
                } else { //Si hay éxito en la inserción
                    new MESSAGE('Insercion realizada correctamente', '../Controllers/PARTIDO_Controller.php?action=Showall');
                }
                break;
        }
    }
?>