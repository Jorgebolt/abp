<?php

    session_start();

    include '../Views/MESSAGE_View.php';
    include '../Functions/Authentication.php';
    include '../Models/Estadisticas_Model.php';
    include '../Models/USUARIO_Model.php';

    include '../Views/Estadisticas_View.php';


 


    if(!IsAuthenticated()){
        new MESSAGE("Debes autenticarte", '../index.php');
    }else{

        //balance de generos
    $Estadisticas = new Estadisticas_Model(); //Crea un objeto vacio
 

    $USUARIO = new USUARIO_Model('','','','','','','');
    $usuarios = $USUARIO->SHOWALL();
    $NumeroHombres=0;
    $NumeroMujeres=0;
    while ($row = $usuarios->fetch_array()) {
         if($row['sexo']=="hombre"){
             $NumeroHombres++;
         }else{
             $NumeroMujeres++;

         }
    }

 $balance = array(
    array("label"=> "Hombres", "y"=> $NumeroHombres),
    array("label"=> "Mujeres", "y"=> $NumeroMujeres),

);

      $deportistas = $USUARIO->devolverDeportista();
      $entrenadores = $USUARIO->devolverEntrenador();
    $NumeroEntrenadores=0;
    $NumeroDeportistas=0;
    while ($row = $deportistas->fetch_array()) {
                    
             $NumeroDeportistas++;
        }
    while ($row = $entrenadores->fetch_array()) {
             $NumeroEntrenadores++;
    }


 $Entrenadores = array(
    array("label"=> "Entrenadores", "y"=> $NumeroEntrenadores),
    array("label"=> "Deportistas", "y"=> $NumeroDeportistas),

);
       // `categoria` enum('masculina','femenina','mixta') NOT NULL,
  //  `nivel` enum('amateur','intermedio','profesional') NOT NULL,
    $amateurMasculina=0;
    $amateurFemenina=0;
    $amateurMixta=0;
    $intermedioMasculina=0;
    $intermedioFemenina=0;
    $intermedioMixta=0;
    $profesionalMasculina=0;
    $profesionalFemenina=0;
    $profesionalMixta=0;

     $categoriaNivel= $Estadisticas->DatosCampeonato();
  while ($row = $categoriaNivel->fetch_array()) {
    if($row['categoria']=="masculina"){

        if($row['nivel']=="amateur"){
            $amateurMasculina++;
        } 
        if($row['nivel']=="intermedio"){
             $intermedioMasculina++;
        } 
        if($row['nivel']=="profesional"){
             $profesionalMasculina++;
        }
    }
          if($row['categoria']=="femenina"){

        if($row['nivel']=="amateur"){
            $amateurFemenina++;
        } 
        if($row['nivel']=="intermedio"){
             $intermedioFemenina++;
        } 
        if($row['nivel']=="profesional"){
             $profesionalFemenina++;
        }
    }

    if($row['categoria']=="mixta"){

        if($row['nivel']=="amateur"){
            $amateurMixta++;
        } 
        if($row['nivel']=="intermedio"){
             $intermedioMixta++;
        } 
        if($row['nivel']=="profesional"){
             $profesionalMixta++;
        }
    }  

    }

    

     $popularidadCampeonatos = array(
    array("label"=> "amateurMasculina", "y"=> $amateurMasculina),
    array("label"=> "amateurFemenina", "y"=> $amateurFemenina),
        array("label"=> "amateurMixta", "y"=> $amateurMixta),
 array("label"=> "intermedioMasculina", "y"=> $intermedioMasculina),
  array("label"=> "intermedioFemenina", "y"=> $intermedioFemenina),
   array("label"=> "intermedioMixta", "y"=> $intermedioMixta),
    array("label"=> "profesionalMasculina", "y"=> $profesionalMasculina),
     array("label"=> "profesionalFemenina", "y"=> $profesionalFemenina),
      array("label"=> "profesionalMixta", "y"=> $profesionalMixta),

);
     /*
$partidos=array();
  $usuarios = $USUARIO->SHOWALL();
  while ($row = $usuarios->fetch_array()) {
                  $partidos[] =array($row['login'],$Estadisticas->Partidos($row['login']));
        }
  
*/
    $partidos=array();
    $SumaPartidos=0;
    $deportistas = $USUARIO->devolverDeportista();

  while ($row = $deportistas->fetch_array()) {
                  $SumaPartidos+=$Estadisticas->Partidos($row['login'])->num_rows;
        }
    $media=$SumaPartidos/$NumeroDeportistas;
    new Estadisticas_View($balance , $popularidadCampeonatos,$Entrenadores,$media);

           

    }
?>