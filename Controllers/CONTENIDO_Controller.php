<?php

    session_start();

    include '../Views/MESSAGE_View.php';
    include '../Functions/Authentication.php';
    include '../Models/CONTENIDO_Model.php';
    include '../Models/USUARIO_Model.php';

    include '../Views/CONTENIDO_Showall.php';
    include '../Views/CONTENIDO_Showcurrent.php';
    include '../Views/CONTENIDO_Add.php';
    include '../Views/CONTENIDO_Edit.php';


 

    function cambiaf_a_mysql($fecha) //Funcion que cambia el formato de la fecha
    {
        $mifecha = explode("/", $fecha);
        $lafecha = $mifecha[2] . "-" . $mifecha[1] . "-" . $mifecha[0];
        return $lafecha;
    }

    if(!IsAuthenticated()){
        new MESSAGE("Debes autenticarte", '../index.php');
    }else{
        switch ($_REQUEST['action']) {
            case 'Showall':
                $CONTENIDO = new CONTENIDO_Model('', '', '', ''); //Crea un objeto vacio
                $datos = $CONTENIDO->SHOWALL();
                
                new CONTENIDO_Showall($datos);

                break;

            case 'Add':
                if ($_SESSION['login'] != 'root') {
                    new MESSAGE('Acceso denegado', '../Controllers/CONTENIDO_Controller.php');

                } else { //si eres el root

                    if(!$_POST){
                        new CONTENIDO_Add();
                    }else{ //si viene de cubrir el formulario
                     
 
                        $CONTENIDO = new CONTENIDO_Model($_SESSION['login'] , $_REQUEST['idcontenido'], $_REQUEST['titulo'], $_REQUEST['descripcion']); 

                        $respuesta = $CONTENIDO->ADD();
                    
                        header('Location:./CONTENIDO_Controller.php?action=Showall');
               
                    }
                }
                break;

            case 'Showcurrent':
               if ($_SESSION['login'] != 'root') {//cambiar a comprobar que es admin o root(por si te quedas sin admins).
                    new MESSAGE('Acceso denegado', '../Controllers/USUARIO_Controller.php');
                } else { //si eres admin
                $CONTENIDO = new CONTENIDO_Model('',$_REQUEST['idcontenido'],'','');
                $datos = $CONTENIDO->SHOWCURENT();
                new CONTENIDO_Showcurrent($datos);
                }
                break;

        case 'Edit'://falta añadir checked a los edits
               if ($_SESSION['login'] != 'root') {//cambiar a comprobar que es admin o root(por si te quedas sin admins).
                    new MESSAGE('Acceso denegado', '../Controllers/USUARIO_Controller.php');
                } else { //si eres admin
             if(!isset($_REQUEST['titulo'])){
                      $CONTENIDO = new CONTENIDO_Model($_SESSION['login'],$_REQUEST['idcontenido'],'','');
                       $datos = $CONTENIDO->SHOWCURENT();
                       new CONTENIDO_Edit($datos);
                 }else{
                     $CONTENIDO = new CONTENIDO_Model($_SESSION['login'] , $_REQUEST['idcontenido'], $_REQUEST['titulo'], $_REQUEST['descripcion']);
                       $respuesta = $CONTENIDO->EDIT();

                        
                        new MESSAGE($respuesta, '../Controllers/CONTENIDO_Controller.php?action=Showall'); //Mensaje de acción 
                 }
             }
                    
            
           
           

                break;

            case 'Delete':
                if ($_SESSION['login'] != 'root') {
                    new MESSAGE('Acceso denegado', '../Controllers/CONTENIDO_Controller.php');
                } else { //si eres el root

                        $CONTENIDO = new CONTENIDO_Model($_SESSION['login'],$_REQUEST['idcontenido'],'','');
                        
                        $respuesta = $CONTENIDO->DELETE();

                        if (!$respuesta) { //Si el Delete no se realiza
                            new MESSAGE('error', '../Controllers/CONTENIDO_Controller.php?action=Showall'); //Mensaje de error
                        } else {
                            new MESSAGE('exito eliminando', '../Controllers/CONTENIDO_Controller.php?action=Showall'); //Mensaje de borrado exitoso
                        }	
                    }
             

                break;

            default:
                echo 'ESTAS EN EL DEFAULT DE CONTENIDO_Controller';
                new MESSAGE('ESTAS EN EL DEFAULT DE CONTENIDO', '../Controllers/CONTENIDO_Controller.php?action=Showall');
                break;
        }
    }
?>