<?php

    session_start();

    include '../Views/MESSAGE_View.php';
    include '../Functions/Authentication.php';
    include '../Models/CAMPEONATO_Model.php';
    include '../Models/PAREJA_Model.php';
    include '../Models/GRUPO_Model.php';
    include '../Models/LIGAREGULAR_Model.php';
    include '../Models/USUARIO_Model.php';
    include '../Models/PLAYOFF_Model.php';

    include '../Views/CAMPEONATO_Showall.php';
    include '../Views/CAMPEONATO_Add.php';
    include '../Views/CAMPEONATO_Delete.php';
    include '../Views/CAMPEONATO_Showcurrent.php';
    include '../Views/CAMPEONATO_SeleccionarPareja.php';
    include '../Views/CAMPEONATO_SeleccionarNivel.php';
    include '../Views/PAREJA_Showall.php';
    include '../Views/GRUPO_Showall.php';
    include '../Views/PLAYOFF_Showall.php';

    function cambiaf_a_mysql($fecha) //Funcion que cambia el formato de la fecha
    {
        $mifecha = explode("/", $fecha);
        $lafecha = $mifecha[2] . "-" . $mifecha[1] . "-" . $mifecha[0];
        return $lafecha;
    }

    if(!IsAuthenticated()){
        new MESSAGE("Debes autenticarte", '../index.php');
    }else{
        switch ($_REQUEST['action']) {
            case 'Showall':
                $CAMPEONATO = new CAMPEONATO_Model('', '', '', '', ''); //Crea un objeto vacio
                $datos = $CAMPEONATO->AllData();
                
                new CAMPEONATO_Showall($datos);

                break;

            case 'Add':
                if ($_SESSION['login'] != 'root') {
                    new MESSAGE('Acceso denegado', '../Controllers/CAMPEONATO_Controller.php');

                } else { //si eres el root

                    if(!$_POST){
                        new CAMPEONATO_Add();
                    }else{ //si viene de cubrir el formulario
                        $fechaLimite = cambiaf_a_mysql($_POST['fechaLimiteInscrip']); //Guarda la variable de la fecha
                        
                        $CAMPEONATO = new CAMPEONATO_Model('', 0, $fechaLimite, 'N', 'N'); //se crean con 0 participantes

                        $respuesta = $CAMPEONATO->ADD();
                        //Vuelve directamente al showall, no muestra un mensaje
                        header('Location:./CAMPEONATO_Controller.php?action=Showall');
                        //new MESSAGE($respuesta, '../Controllers/CAMPEONATO_Controller.php?action=Showall');
                    }
                }
                break;

                case 'Showcurrent':

                    $CAMPEONATO = new CAMPEONATO_Model($_GET['idCampeonato'], '', '', '','', '', '', '', '', '', '', '');
                    $datos = $CAMPEONATO->RellenaDatos(); //Guarda la tupla seleccionada
                    $participantes = $CAMPEONATO->getParticipantes();

                    //$datosLiga = $CAMPEONATO->RecuperaLigas();
                    $parejas = $CAMPEONATO->getParejas();
                    $grupos = $CAMPEONATO->getGrupos();

                    //comprobar si el usuario ya esta inscrito en el campeonato
                    $usuario = $_SESSION['login'];
                    $estaInscrito = $CAMPEONATO ->estaInscrito($usuario, $parejas);

                    $showc = new CAMPEONATO_Showcurrent($datos, $participantes, $estaInscrito);

                    $parejas = $CAMPEONATO->getParejas();
                    new PAREJA_Showall($parejas);
                    
                    $gruposGenerados = $CAMPEONATO->getGruposGenerados();
                    $playoffsGenerados = $CAMPEONATO->getPlayoffsGenerados();

                    //si eres el root o estan generados los grupos
                    if($_SESSION['login'] == 'root' || $gruposGenerados == 'S'){
                        //puede visualizar los grupos
                        new GRUPO_Showall($grupos, $gruposGenerados, $playoffsGenerados);
                    }

                    //si eres el root o estan generados los playoffs
                    if($_SESSION['login'] == 'root' || $playoffsGenerados == 'S'){

                        $playoffs = $CAMPEONATO->getPlayoffs();

                        //puede visualizar los grupos
                        new PLAYOFF_Showall($playoffs, $playoffsGenerados);
                    }
                   
                break;
            
            //asigna un grupo a todas las parejas
            case 'RellenarGrupos':
                //aqui ya se sabe que en el campeonato hay al menos una pareja inscrita

                //Creo un objeto vacio
                $CAMPEONATO = new CAMPEONATO_Model($_REQUEST['idCampeonato'], '', '', '', '');

                //recupero todas las parejas de este campeonato
                $parejas = $CAMPEONATO->getParejas();

                //recupero todos los grupos de este campeonato
                $grupos = $CAMPEONATO->getGrupos();

                //asigno grupos a las parejas
                $generarGrupos = $CAMPEONATO->generarGrupos($parejas, $grupos);

                //Actualizo el atributo gruposGenerados de ese campeonato
                $uptadeGenerarGrupos = $CAMPEONATO->updateGenerarGrupos();
                
                //para esos grupos
                //comprueba que hacer segun su numero de parejas inscritas
                $GRUPO = new GRUPO_Model('', '' ,'', '', null);
        
                //si se actualizo todo con exito, muestra un showcurrent
                if($generarGrupos && $uptadeGenerarGrupos){
                    $volverA = 'Location:./CAMPEONATO_Controller.php?action=Showcurrent&idCampeonato=' . $_REQUEST['idCampeonato'];
                    header($volverA);
                }
                break;

            case 'Inscribirse':
                if ($_SESSION['login'] == 'root') {
                    new MESSAGE('No eres un deportista', '../Controllers/CAMPEONATO_Controller.php?action=Showall');
                
                } else { //si eres un usuario
                
                if(!isset($_REQUEST['nivel'])){
                    //selecciona un nivel
                    new CAMPEONATO_SeleccionarNivel($_REQUEST['idCampeonato']);

                    }else if(!isset($_REQUEST['login'])){
                        //se le muestran los usuarios para que elija pareja

                        $USUARIO = new USUARIO_Model('','','','','','','');
                        $datos = $USUARIO->SHOWALL();

                        new CAMPEONATO_SeleccionarPareja($datos,$_REQUEST['idCampeonato'],$_REQUEST['nivel']);
                    }else{
                        $USUARIO1 = new USUARIO_Model($_SESSION['login'],'','','','','','');
                        $USUARIO2 = new USUARIO_Model($_REQUEST['login'],'','','','','','');
                        $sexoU1= $USUARIO1->ComprobarSexo();
                        $sexoU2= $USUARIO2->ComprobarSexo();

                        if($sexoU1!=$sexoU2){
                            $categoria="mixta";

                        }else if($sexoU1=="hombre"){
                            $categoria="masculina";

                        }else if($sexoU1=="mujer"){
                            $categoria="femenina";

                        }

                        //la pareja se crea
                        //no se le asigna un grupo hasta "generar" los grupos
                        //por eso lleva un null en el idGrupo
                        $PAREJA = new PAREJA_Model($_REQUEST['idCampeonato'], null, null, $_SESSION['login'], $_REQUEST['login'], $categoria ,$_REQUEST['nivel'], 0, 0);
                        
                        //la pareja se inserta con exito
                        $inscripcion = $PAREJA->ADD();

                        if ($inscripcion == 'Exito insertando'){ //se inscribio con exito una pareja

                            //hay que incrementar en dos el numParticipantes de ese campeonato
                            $CAMP = new CAMPEONATO_Model($_REQUEST['idCampeonato'], '','','', '');
                            $aumentarPart = $CAMP->incrementarPart();

                            $GRUPO = new GRUPO_Model('', $categoria ,$_REQUEST['nivel'], $_REQUEST['idCampeonato'], null);
                            
                            //si NO existe un grupo de esa categoria y nivel
                            //es decir, cuando devuelva false la funcion existe
                            if(!$GRUPO->existe()){
                                //hay que crearlo
                                $GRUPO->Add();
                                //echo 'grupo creado (en teoria)';

                            }
                            new MESSAGE('Inscripcion realizada correctamente', '../Controllers/CAMPEONATO_Controller.php?action=Showall');
                        
                        }else{ //erorr en la inscripcion
                            new MESSAGE($inscripcion, '../Controllers/CAMPEONATO_Controller.php?action=Showall');
                        
                        }
                    }
                }

                break;

            //asigna un playoff a todas las parejas
            case 'GenerarPlayoff':
                if ($_SESSION['login'] != 'root') {
                    new MESSAGE('Acceso denegado', '../Controllers/CAMPEONATO_Controller.php');

                } else { //si eres el root

                    $CAMP = new CAMPEONATO_Model($_REQUEST['idCampeonato'], '', '', '', '');

                    //coger los ids de los grupos con 8 o mas parejas de este campeonato
                    $gruposPlayoff = $CAMP->getGruposPlayoff();
                    
                    //por cada grupo
                    while ($id = $gruposPlayoff->fetch_array()) {

                        //echo ' // id ' . $id[0];
                        $GRUPO = new GRUPO_Model($id[0], '', '', $_REQUEST['idCampeonato'], null);
                        
                        //recuperar su nivel y su categoria
                        $categoria = $GRUPO->getCategoria();
                        $nivel = $GRUPO->getNivel();

                        //crea un nuevo playOff
                        $PLAYOFF = new PLAYOFF_Model(null,$id[0], $_REQUEST['idCampeonato'], $categoria, $nivel, null);
                        $crearPlayoff = $PLAYOFF->Add();

                        //si se creo el playoff con exito
                        if($crearPlayoff){

                            //recuperar el id del playoff que se acaba de crear
                            $idNewPlayoff = $PLAYOFF->getLastId();
                            //echo 'ultimo id playoff creado = ' . $idNewPlayoff;
                            
                            //coger las parejas del grupo que esta recorriendo 
                            $parejas = $GRUPO->getParejasPlayoff();

                            //posicion desde la que cada pareja jugara el playoff
                            $posPlayoff = 1;

                            while($row = $parejas->fetch_array()){
                                //a cada pareja
                                $PAREJA = new PAREJA_Model($_REQUEST['idCampeonato'], $row['idGrupo'], null, $row['login1'], $row['login2'], $row['categoria'], $row['nivel'], null, null);

                                //echo 'update pareja ' . $row['login1'] . ' - ' . $row['login2'];
                                
                                //le ajusta el atributo idPlayOff
                                $setIdPlayoff = $PAREJA->setIdPlayoff($idNewPlayoff);

                                //le ajustamos la posicion desde la que jugara el playoff
                                $setPosPlayoff = $PAREJA->setPosPlayoff($posPlayoff);
                                $posPlayoff = $posPlayoff + 1;
                            }
                        }
                    }
                    
                    //despues del while de cada grupo
                    //si hubo exito
                    if($setIdPlayoff && $setPosPlayoff){
                        //actualiza el atributo playoffsGenerados
                        $uptdateGenerarPlayoffs = $CAMP->updateGenerarPlayoffs();
                    }
                    
                    //si se actualizo todo con exito, muestra el showcurrent
                    if($uptdateGenerarPlayoffs){
                        $volverA = 'Location:./CAMPEONATO_Controller.php?action=Showcurrent&idCampeonato=' . $_REQUEST['idCampeonato'];
                        header($volverA);
                    }
                }
                break;

            case 'Delete':
                if ($_SESSION['login'] != 'root') {
                    new MESSAGE('Acceso denegado', '../Controllers/CAMPEONATO_Controller.php');
                } else { //si eres el root

                    if (!isset($_GET['borrado'])){

                        $CAMPEONATO = new CAMPEONATO_Model($_GET['idCampeonato'], '', '', '', ''); //Crea un objeto vacio
                        $datos = $CAMPEONATO->RellenaDatos();
                        $delete = new CAMPEONATO_Delete($datos);

                    }else{

                        $CAMPEONATO = new CAMPEONATO_Model($_GET['idCampeonato'], '', '', '', ''); //Crea un objeto vacio
                        
                        $respuesta = $CAMPEONATO->DELETE();

                        if ($respuesta == 'Error en la insercion') { //Si el Delete no se realiza
                            new MESSAGE('error eliminando', '../Controllers/CAMPEONATO_Controller.php?action=Showall'); //Mensaje de error
                        } else {
                            new MESSAGE('exito eliminando', '../Controllers/CAMPEONATO_Controller.php?action=Showall'); //Mensaje de borrado exitoso
                        }	
                    }
                }

                break;

            default:
                echo 'ESTAS EN EL DEFAULT DE CAMPEONATO_Controller';
                new MESSAGE('ESTAS EN EL DEFAULT DE CAMPEONATO', '../Controllers/CAMPEONATO_Controller.php?action=Showall');
                break;
        }
    }
?>