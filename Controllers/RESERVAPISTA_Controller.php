<?php

    session_start();

    include '../Functions/Authentication.php';
    include '../Models/RESERVAPISTA_Model.php';
    include '../Models/HORARIOSRESERVAS_Model.php';
    include '../Models/RESERVASREALIZADAS_Model.php';
    include '../Models/USUARIO_Model.php';
    
    
    include '../Views/MESSAGE_View.php';

    
    function cambiaf_a_mysql($fecha) //Funcion que cambia el formato de la fecha
    {
        $mifecha = explode("/", $fecha);
        $lafecha = $mifecha[2] . "-" . $mifecha[1] . "-" . $mifecha[0];
        return $lafecha;
    }

    

    //$RESERVAPISTA = new RESERVAPISTA_Model('', '', ''); //Crea un objeto vacio
    if(!IsAuthenticated()){
        new MESSAGE("Debes autenticarte", '../index.php');
    }else{
        if(!isset($_REQUEST['action'])){ // Si no tiene ninguna opcion mustra la tabla SHOWALL.
            if($_SESSION['login'] == 'root'){
                include '../Views/RESERVASREALIZADAS_View.php';
                
                $objeto = new RESERVASREALIZADAS_Model('','','','','');
                $resultado = $objeto->AllData2();

        new RESERVASREALIZADAS_View($resultado);
            }else{
               include '../Views/RESERVASREALIZADAS_View.php';
                $login = $_SESSION['login'];
                $objeto = new RESERVASREALIZADAS_Model('',$login,'','','');
                $resultado = $objeto->AllData();

        new RESERVASREALIZADAS_View($resultado); 
            }
        
        
    }else{
        $action=$_REQUEST['action'];
        if($action =='empezar'){ 


            $objeto = new RESERVASREALIZADAS_Model('','','','','');
            $resultado = $objeto->AllData();
            include '../Views/RESERVAPISTA_View.php';
            
            new RESERVAPISTA_View($resultado);

        }
        if($action =='ver'){ 
            if($_SESSION['login'] == 'root'){
                
                $objeto = new RESERVASREALIZADAS_Model('','','','','');
                $resultado = $objeto->AllData2();
                include '../Views/RESERVASREALIZADAS_View.php';
            
            new RESERVASREALIZADAS_View($resultado);
            }else{
                $login = $_SESSION['login'];
                $objeto = new RESERVASREALIZADAS_Model('',$login,'','','');
                $resultado = $objeto->AllData();
                include '../Views/RESERVASREALIZADAS_View.php';
            
                new RESERVASREALIZADAS_View($resultado);
            }
            
        }
        if($action =='delete'){ 


            $RESERVA = new RESERVASREALIZADAS_Model($_REQUEST['idReserva'],'','','',''); //crea un modelo de contrato
                        $respuesta = $RESERVA->DELETE();//devuelve true o false

                        if ($respuesta) { 
                            new MESSAGE('exito eliminando', '../Controllers/RESERVAPISTA_Controller.php?action=ver'); //Mensaje de error
                        } else {
                            new MESSAGE('error eliminando', '../Controllers/RESERVAPISTA_Controller.php?action=ver'); //Mensaje de borrado exitoso
                        }   
        }
        if($action =='add'){ 


            $login = $_SESSION['login'];
            $pista = rand(1,10);
            $objeto = new RESERVASREALIZADAS_Model('',$login,$pista,$_GET['fecha'],$_GET['horaInicio']);
            $resultado = $objeto->ADD();
            //include '../Views/CLASESPARTICULARES_Showall.php';
            
            //new CLASESPARTICULARES_Showall($resultado);
            new MESSAGE($resultado, '../Controllers/RESERVAPISTA_Controller.php?action=ver'); //Mensaje de acción realizada


        }

        if($action =='add2'){ 

            //$login = $_SESSION['login'];
            $fecha = cambiaf_a_mysql($_POST['fecha']);
            //$objeto = new RESERVASREALIZADAS_Model('',$login,$_REQUEST['idPista'],$fecha,$_REQUEST['hora']);
            //objeto = new RESERVASREALIZADAS_Model($_REQUEST['idPista'],$_REQUEST['fecha'],$_REQUEST['hora']);
            $objeto = new HORARIOSRESERVAS_Model('','','',$fecha);
            $objeto->UPDATE();
            $resultado = $objeto->AllData2();
            include '../Views/HORARIOSRESERVAS_Showall.php';

            new HORARIOSRESERVAS_Showall($resultado);

        }
    }
    }
?>