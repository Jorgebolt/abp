<?php
class PLAYOFF_Model{

    var $idPlayoff;
    var $idGrupo;
    var $idCampeonato;
    var $categoria;
    var $nivel;
    var $ganador;
    var $mysqli;

    function __construct($idPlayoff, $idGrupo, $idCampeonato, $categoria, $nivel, $ganador){
        $this->idPlayoff = $idPlayoff;
        $this->idGrupo = $idGrupo;
        $this->idCampeonato = $idCampeonato;
        $this->categoria = $categoria;
        $this->nivel = $nivel;
        $this->ganador = $ganador;

        include_once '../Models/Access_DB.php';
        $this->mysqli = ConnectDB();
    }

    function Add(){
        $sql = "INSERT INTO `playOff`
                (`idplayOff`,
                `idGrupo`,
                `idCampeonato`,
                `categoria`,
                `nivel`,
                `ganador`
                )
            VALUES 
                (NULL,
                " . $this->idGrupo . ",
                " . $this->idCampeonato . ",
                '" . $this->categoria . "',
                '" . $this->nivel . "',
                NULL
                )
        ";

        if (!$this->mysqli->query($sql)) { //si la insercion da error
            echo 'error insertando';
            return false;
        } else {
            return true;
        }
    }

    //funcion que devuele el ultimo id de un playoff
    function getLastId(){
        $sql = "SELECT `idPlayoff`
            FROM PLAYOFF
            ORDER BY `idPlayoff` DESC
            LIMIT 1
        ";
        
        $resultado = $this->mysqli->query($sql);

        if($resultado){ //si la busqueda da resultado
            $id = $resultado->fetch_array();
            return $id[0];

        }else{
            echo 'error recuperando el ultimo id';
            return false;
        }
    }

    //funcion que devuelve las parejas de un playoff
    function getParejas(){
        $sql = "SELECT *
            FROM PAREJA
            WHERE `idGrupo` = " . $this->idGrupo . "
                AND `idPlayoff` = " . $this->idPlayoff . "
                ORDER BY `puntosLiga` DESC
        ";

        $resultado = $this->mysqli->query($sql);

        if (!($resultado)) {
            echo 'error recuperando parejas';
            return false;

        } else {
            return $resultado;
        }
    }

    //funcion que, dado un usuario y un grupo de parejas, devuelve true si esta inscrito en una de ellas
    function estaInscrito($usuario, $parejas){
        $esta = false;  
        while ($row = $parejas->fetch_array()) {
            if ($row['login1'] == $usuario or $row['login2'] == $usuario) {
                $esta = true;
            }
        }

        return $esta;
    }

    //funcion que devuelve las parejas 1 y 8 de un playoff
    function getEnfrentamiento1(){
        $sql = "SELECT `login1`, `login2`
        FROM PAREJA
        WHERE (`posPlayoff` = 1 OR `posPlayoff` = 8)
            AND `idPlayoff` = " . $this->idPlayoff . "
        ";

        $resultado = $this->mysqli->query($sql);

        if (!($resultado)) {
            echo 'error recuperando parejas 1 y 8';
            return false;

        } else {
            return $resultado;
        }
    }

    //funcion que devuelve las parejas 2 y 7 de un playoff
    function getEnfrentamiento2(){
        $sql = "SELECT `login1`, `login2`
        FROM PAREJA
        WHERE (`posPlayoff` = 2 OR `posPlayoff` = 7)
            AND `idPlayoff` = " . $this->idPlayoff . "
        ";

        $resultado = $this->mysqli->query($sql);

        if (!($resultado)) {
            echo 'error recuperando parejas 2 y 7';
            return false;

        } else {
            return $resultado;
        }
    }

    //funcion que devuelve las parejas 3 y 6 de un playoff
    function getEnfrentamiento3()
    {
        $sql = "SELECT `login1`, `login2`
        FROM PAREJA
        WHERE (`posPlayoff` = 3 OR `posPlayoff` = 6)
            AND `idPlayoff` = " . $this->idPlayoff . "
        ";

        $resultado = $this->mysqli->query($sql);

        if (!($resultado)) {
            echo 'error recuperando parejas 3 y 6';
            return false;
        } else {
            return $resultado;
        }
    }

    //funcion que devuelve las parejas 4 y 5 de un playoff
    function getEnfrentamiento4(){
        $sql = "SELECT `login1`, `login2`
        FROM PAREJA
        WHERE (`posPlayoff` = 4 OR `posPlayoff` = 5)
            AND `idPlayoff` = " . $this->idPlayoff . "

        ";

        $resultado = $this->mysqli->query($sql);

        if (!($resultado)) {
            echo 'error recuperando parejas 4 y 5';
            return false;

        } else {
            return $resultado;
        }
    }

    //funcion que ajusta el ganador de un playoff
    function setGanador($ganador){
        $sql = "UPDATE `PLAYOFF`
            SET `ganador` = '" . $ganador . "'
            WHERE `idPlayoff` = '" . $this->idPlayoff . "'
            ";

        $resultado = $this->mysqli->query($sql);

        if ($resultado) {
            return true;
        } else {
            return false;
        } 
    }

    //funcion que devuelve el ganador
    function getGanador(){
        $sql = "SELECT `ganador` 
            FROM PLAYOFF
            WHERE `idPlayoff` = '" . $this->idPlayoff . "'
            ";

        $resultado = $this->mysqli->query($sql);

        if (!($resultado)) { // Si la busqueda no da resultados, se devuelve false
            return false;
            
        } else { // si existe se devuelve la tupla resultado
            $arrayRes = $resultado->fetch_array();
            return $arrayRes[0];
        }
    }
}
?>
