<?php
class RESERVASREALIZADAS_Model{

    var $idReserva;
    var $login;
    var $idPista;
    var $fecha;
    var $hora;
    var $mysqli;

    function __construct( $idReserva,$login,$idPista,$fecha, $hora){
        $this->idReserva = $idReserva;
        $this->login = $login;
        $this->idPista = $idPista;
        $this->fecha = $fecha;
        $this->hora = $hora;
        
        include '../Models/Access_DB.php';
        $this->mysqli = ConnectDB();
    }

    function ADD(){
        $sql = "INSERT INTO RESERVASREALIZADAS (
            login,
            idPista,
            fecha, 
            hora
            ) 
            VALUES (
                '" . $this->login . "',
                '" . $this->idPista . "',
                '" . $this->fecha . "',
                '" . $this->hora . "'

                )";
        if($this->mysqli->query($sql)){
            return "reserva realizada";
        }
        else{
            return 'error reserva';
        }       
    }
    

    //function SEARCH(){

    //}

    function EDIT(){

    }

    function DELETE(){
        $sql = "DELETE FROM RESERVASREALIZADAS WHERE (`idReserva` = '$this->idReserva')";

        if (!$this->mysqli->query($sql)) {//Si la ejecución del delete da error
            return 'Error en la eliminación';//Muesta un mensaje y vuelve al showall
        } else { //Si la ejecución del delete es correcta
            return 'Eliminación realizada con éxito';//Muesta un mensaje y vuelve al showall
        }
    }

    //Función que devuelve toda la tabla
    function AllData()
    {
        $sql; //variable que alberga la sentencia sql
        $result; //almacena el valor de la variable resultado
        // construimos el sql para buscar esa clave en la tabla
        

        $sql = "SELECT * FROM RESERVASREALIZADAS WHERE (`login` = '$this->login')";

        $resultado = $this->mysqli->query($sql);

        if (!($resultado)) { // Si la busqueda no da resultados, se devuelve el mensaje de que no existe
            return 'tupla inexistente';
        } else { // si existe se devuelve la tupla resultado
            $result = $resultado;
            return $result;
        }
    }

    function AllData2()
    {
        $sql; //variable que alberga la sentencia sql
        $result; //almacena el valor de la variable resultado
        // construimos el sql para buscar esa clave en la tabla
        
        
        $sql = "SELECT * FROM RESERVASREALIZADAS";

        $resultado = $this->mysqli->query($sql);

        if (!($resultado)) { // Si la busqueda no da resultados, se devuelve el mensaje de que no existe
            return 'tupla inexistente';
        } else { // si existe se devuelve la tupla resultado
            $result = $resultado;
            return $result;
        }
    }

    //Recupera todos los atributos de una tupla a partir de su clave
    function RellenaDatos()
    {
       //Sentencia SQL de búsqueda de la tupla
        $sql = "SELECT *
                FROM `reservasRealizadas`
                WHERE (`idReserva` = '" . $this->idReserva . "'
            )";

        $resultado = $this->mysqli->query($sql);

        if (!$resultado) { //Si la busqueda no da resultado (la tupla no está en la BD)
            return 'tupla inexistente';
        } else { //Si la búsqueda da resultado
            $result = $resultado->fetch_array();
            return $result; //Devuelve la tupla resultado
        }
    }

    
}
?>