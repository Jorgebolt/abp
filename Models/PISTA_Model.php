<?php
class PISTA_Model {

    var $idPista;//Declaración del atributo idPista
    var $idHorario;//Declaración del atributo idHorario
    var $descripcion;//Declaración del atributo descripcion
    
    var $mysqli;

    //Constructor de la clase
    function __construct($idPista,$idHorario,$descripcion){
    $this->idPista = $idPista;//Definición de la variable idPista
    $this->idHorario= $idHorario;//Definición de la variable idHorario
    $this->descripcion = $descripcion;//Definición de la variable descripcion

    include_once '../Models/Access_DB.php';
    $this->mysqli = ConnectDB();
    }
    
    //Funcion ADD: añade una visita
    function ADD(){
        $sql; //Variable para guardar la sentencia SQL
        $resultado; //Variable para guardar el resultado de la sentencia SQL


        $sql = "INSERT INTO `PISTA` (
                 `idHorario`,
                 `descripcion`)
                VALUES(
                '".$this->idHorario."',
                '".$this->descripcion."'
                )";
        if(!$this->mysqli->query($sql)){ //Si la ejecución del insert da error
            return 'error insertando'; //Muesta un mensaje y vuelve al showall
            
        }else{ //Si la ejecución del insert es correcta
            return 'exito insertando'; //Muestra un mensaje y vuelve al showall
        }


    }
   
    //funcion DELETE : comprueba que la tupla a borrar existe y una vez verificado la borra
    function DELETE(){
            $sql = "DELETE FROM PISTA WHERE (`idPista` = '$this->idPista')";

        if (!$this->mysqli->query($sql)) {//Si la ejecución del delete da error
            return 'Error en la eliminación';//Muesta un mensaje y vuelve al showall
        } else { //Si la ejecución del delete es correcta
            return 'Eliminación realizada con éxito';//Muesta un mensaje y vuelve al showall
        }
    }

    //funcion FINFECHA: comprueba las visitas fuera de fecha
    function FINFECHA(){
        $hoy = date("Y-m-d");
        $sql; //variable que alberga la sentencia sql
        $resultado; //almacena la consulta sql
        $result; //almacena el valor de la variable resultado

        // construimos el sql para buscar esa clave en la tabla
        $sql = "SELECT * FROM `PISTA` WHERE `fecha` > '$hoy'";

        $resultado = $this->mysqli->query($sql);

        if (!($resultado)) { // Si la busqueda no da resultados, se devuelve el mensaje de que no existe
            return 'tupla inexistente';
        } else { // si existe se devuelve la tupla resultado
            $result = $resultado;
            return $result;
        }
    }
    
    //Función AllData: devuelve toda la tabla
    function AllData(){
        $sql; //variable que alberga la sentencia sql
        $resultado; //almacena la consulta sql
        $result; //almacena el valor de la variable resultado

        // construimos el sql para buscar esa clave en la tabla
        $sql = "SELECT * FROM `PISTA` ";

        $resultado = $this->mysqli->query($sql);

        if (!($resultado)) { // Si la busqueda no da resultados, se devuelve el mensaje de que no existe
            return 'tupla inexistente';
        } else { // si existe se devuelve la tupla resultado
            $result = $resultado;
            return $result;
        }
    }

 //Funcion RellenaDatos: Recupera todos los atributos de una tupla a partir de su clave
   function RellenaDatos(){
        $sql; //variable que alberga la sentencia sql
        $resultado; //almacena el resultado de la consulta sql 
        $result; //almacena el valor de la variable resultado

        //Sentencia SQL de búsqueda de la tupla
        $sql = "SELECT *
                FROM `PISTA`
                WHERE (`idPista` = '" . $this->idPista . "'
            )";

        $resultado = $this->mysqli->query($sql);
        if (!$resultado){ //Si la busqueda no da resultado (la tupla no está en la BD)
            return 'tupla inexistente';

        }else{ //Si la búsqueda da resultado
            $result = $resultado->fetch_array();
            return $result; //Devuelve la tupla resultado

        }
    }

    // funcion EDIT: verifica que una tupla existe y la edita
    function EDIT(){
        $sql = "UPDATE PISTA SET
                `descripcion` = '$this->descripcion'
        WHERE (`idPista` = '$this->idPista')";

        if (!$this->mysqli->query($sql)) {//Si la ejecución del update da error
            return 'Error en la inserción';//Muesta un mensaje y vuelve al showall
        } else { //Si la ejecución del update es correcta
            return 'Edición realizada con éxito';//Muesta un mensaje y vuelve al showall
        }
    }

}//fin de clase

?> 
