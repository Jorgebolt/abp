<?php
class CAMPEONATO_Model{

    var $idCampeonato;
    var $numParticipantes;
    var $fechaLimiteInscrip;
    var $gruposGenerados;
    var $playoffsGenerados;

    function __construct($idCampeonato, $numParticipantes, $fechaLimiteInscrip, $gruposGenerados, $playoffsGenerados){
        $this->idCampeonato = $idCampeonato;
        $this->numParticipantes = $numParticipantes;
        $this->fechaLimiteInscrip = $fechaLimiteInscrip;
        $this->gruposGenerados= $gruposGenerados;
        $this->playoffsGenerados = $playoffsGenerados;

        include_once '../Models/Access_DB.php';
        $this->mysqli = ConnectDB();
    }

    function ADD(){

        //Primero comprueba si existe en la bd algun campeonato con el mismo idCampeonato
        //$sql = "SELECT * FROM `campeonato` WHERE `idCampeonato` = '" . $this->idCampeonato . "' ";

        //$resultado = $this->mysqli->query($sql);
        //$resultado = true;

        //if(!$resultado){ //si hay error en la ejecucion de la consulta
        //    return 'error conectando con la bd';
        //}else{
            //if($resultado->num_rows == 0){ //si la consulta no devuelve ninguna tupla
                $sql = "INSERT INTO `campeonato` (
                    `idCampeonato`,
                    `numParticipantes`,
                    `fechaLimiteInscrip`,
                    `gruposGenerados`,
                    `playoffsGenerados`
                    )
                    VALUES (
                    NULL,
                    '" .$this->numParticipantes. "',
                    '" .$this->fechaLimiteInscrip. "',
                    'N',
                    'N'
                 )";

                if(!$this->mysqli->query($sql)){ //si la insercion da error
                    return 'error insertando';
                }else{
                    return 'exito insertando';
                }
            //}else{ //si ya existe una tupla con ese idCampeonato
            //    return 'clave existente';
            //}
        //}
    }

    function DELETE(){

        $sql = "DELETE FROM CAMPEONATO WHERE (`idCampeonato` = '$this->idCampeonato')";

        if (!$this->mysqli->query($sql)) { //Si hay un error en la eliminacion
            return 'exito eliminando';
        } else {
            return 'error insertando';
        }
    }

    //Función que devuelve toda la tabla
    function AllData()
    {
        // construimos el sql para buscar esa clave en la tabla
        $sql = "SELECT * FROM `campeonato` ";

        $resultado = $this->mysqli->query($sql);

        if (!($resultado)) { // Si la busqueda no da resultados, se devuelve el mensaje de que no existe
            return 'tupla inexistente';
        } else { // si existe se devuelve la tupla resultado
            $result = $resultado;
            return $result;
        }
    }

    //funcion que devuelve las parejas de un campeonato
    function getParejas(){
        
        $sql = " SELECT *
                FROM `pareja`
                WHERE `idCampeonato` = '" . $this->idCampeonato . "'
        ";

        $resultado = $this->mysqli->query($sql);

        if (!($resultado)) { // Si la busqueda no da resultados, se devuelve el mensaje de que no existe
            return 'tupla inexistente';
        } else { // si existe se devuelve la tupla resultado
            $result = $resultado;
            return $result;
        }
    }

    //funcion que devuelve los grupos de un campeonato
    function getGrupos(){
        $sql = " SELECT *
                FROM `grupo`
                WHERE `idCampeonato` = '" . $this->idCampeonato . "'
        ";

        $resultado = $this->mysqli->query($sql);

        if (!($resultado)) { // Si la busqueda no da resultados, se devuelve el mensaje de que no existe
            return 'tupla inexistente';
        } else { // si existe se devuelve la tupla resultado
            $result = $resultado;
            return $result;
        }
    }

    //funcion que devuelve los ids de los grupos de un campeonato
    function getIdGrupos(){
        $sql = " SELECT `idGrupo`
                FROM `grupo`
                WHERE `idCampeonato` = '" . $this->idCampeonato . "'
        ";

        $resultado = $this->mysqli->query($sql);

        if (!($resultado)) { // Si la busqueda no da resultados, se devuelve el mensaje de que no existe
            echo 'no hubo resultado para la busqueda';
        } else { // si existe se devuelve la tupla resultado
            return $resultado;
        }
    }

    //Recupera todos los atributos de una tupla a partir de su clave
    function RellenaDatos()
    {
       //Sentencia SQL de búsqueda de la tupla
        $sql = "SELECT *
                FROM `campeonato`
                WHERE (`idCampeonato` = '" . $this->idCampeonato . "'
            )";

        $resultado = $this->mysqli->query($sql);

        if (!$resultado) { //Si la busqueda no da resultado (la tupla no está en la BD)
            return 'tupla inexistente';
        } else { //Si la búsqueda da resultado
            $result = $resultado->fetch_array();
            return $result; //Devuelve la tupla resultado
        }
    }

    function RecuperaLigas(){
        //Sentencia SQL de búsqueda de las tuplas
        $sql = "SELECT *
                FROM `ligaRegular`
                WHERE (`idCampeonato` = '" . $this->idCampeonato . "'
            )";

        $resultado = $this->mysqli->query($sql);

        if (!$resultado) { //Si la busqueda no da resultado (la tupla no está en la BD)
            return 'tupla inexistente';
        } else { //Si la búsqueda da resultado
            return $resultado; //Devuelve la tupla resultado
        }
    }

    //funcion que devuelve las parejas participantes de un campeonato
    function getParticipantes(){
        $sql = "SELECT *
                FROM `PAREJA`
                WHERE `idCampeonato` = '" . $this->idCampeonato . "'
                ";

        $resultado = $this->mysqli->query($sql);

        if ($resultado) { //Si la busqueda da resultado
            return $resultado; //Devuelve la tupla resultado

        } else { //Si la búsqueda da error
            return 'false'; //Devuelve la tupla resultado
        }
    }

    //funcion que, dado un usuario, comprueba si esta en las parejas de un campeonato
    //devuelve FALSE en caso de que NO este inscrito
    function estaInscrito($usuario, $parejas){
        $esta = false;
        while($row = $parejas->fetch_array()){
            if($row['login1'] == $usuario OR $row['login2'] == $usuario){
                $esta = true;
            }
        }

        return $esta;
    }

    //funcion para incrementar el numero de participantes tras insertar una pareja en el campeonato
    function incrementarPart(){
        $sql = "SELECT `numParticipantes`
                FROM `CAMPEONATO`
                WHERE `idCampeonato` = '" . $this->idCampeonato . "'
            ";
        
        $resultado = $this->mysqli->query($sql);

        if ($resultado) { //Si la busqueda da resultado
            $res = $resultado->fetch_array();

            //part -> antiguo numero de participantes
            $part = $res[0];
            $newPart = $part + 2;

            $sql = "UPDATE `CAMPEONATO`
                SET `numParticipantes` = " . $newPart . " 
                WHERE `idCampeonato` = '" . $this->idCampeonato . "'
            ";

            $resultado = $this->mysqli->query($sql);

            return $newPart;
        }
    }

    //funcion que distribuye a los jugadores apuntados en un campeonato en grupos
    function generarGrupos($parejas){
        $i = 0;

        while ($arrayParejas = $parejas->fetch_array()){

            //se crea un objeto con los datos de cada pareja
            $PAREJA = new PAREJA_Model($arrayParejas['idCampeonato'], $arrayParejas['idGrupo'], null, $arrayParejas['login1'], $arrayParejas['login2'], $arrayParejas['categoria'], $arrayParejas['nivel'], null, null);
            //echo '// comprobando pareja ' . $i . ': ' . $arrayParejas['login1'] . '/' . $arrayParejas['login2'] . ' (' . $arrayParejas['nivel'] . '+' . $arrayParejas['categoria'] . ') // ';
            
            //busca un grupo de su categoria y nivel
            $idGrupoCorres = $PAREJA->buscaGrupo();

            //si se encuentra un grupo adecuado
            if($idGrupoCorres != null){ 
                //echo 'id buscado = ' . $idGrupoCorres;
                $asignar = $PAREJA->asignarGrupo($idGrupoCorres);
                
                if($asignar){

                    //incrementa en uno el numero de parejas de un grupo
                    $GRUPO = new GRUPO_Model($idGrupoCorres, $arrayParejas['categoria'], $arrayParejas['nivel'], $arrayParejas['idCampeonato'], NULL);
                    $updateNumParejas = $GRUPO->updateNumParejas();
                    //echo $arrayParejas['login1'] . ' y ' . $arrayParejas['login2'] . ' ya tienen grupo // ';
                }
            //si no existe un grupo adecuado
            }else{ 
                //crear un nuevo grupo vacio 
                //con la categoria y el nivel que corresponde
                //se devuelve el id del nuevo grupo
                $nuevoGrupo = $GRUPO->crearNuevo($arrayParejas['categoria'], $arrayParejas['nivel']);

                //si se creo con exito
                if($nuevoGrupo != null){
                    //se lo asigno a la pareja
                    $asignarNuevo = $PAREJA->asignarGrupo($nuevoGrupo);

                    if($asignarNuevo){ //si se le asigno con exito

                        //incrementa en uno el numero de parejas del grupo
                        $GRUPO = new GRUPO_Model($nuevoGrupo, $arrayParejas['categoria'], $arrayParejas['nivel'], $arrayParejas['idCampeonato'], NULL);
                        $updateNumParejas = $GRUPO->updateNumParejas();
                        //echo $arrayParejas['login1'] . ' y ' . $arrayParejas['login2'] . ' ya tienen grupo // ';
                    }

                }else{
                    echo 'no se pudo recupero con exito el id o no se creo el grupo';
                }

                //este nuevo grupo se asignara a parejas sucesivas
                //mientras no este lleno
                //en ese momento, se creara otro nuevo
            }
            $i++;
        }

        //si se actualizo con exito
        if($updateNumParejas){
            return true;
        }
    }

    //funcion que actualiza el atributo gruposGenerados de un campeonato
    function updateGenerarGrupos(){
        $sql = "UPDATE `CAMPEONATO` 
            SET `gruposGenerados` = 'S'
            WHERE `idCampeonato` = '" . $this->idCampeonato . "'
            ";

        $resultado = $this->mysqli->query($sql);

        if ($resultado) {
            return true;
        } else {
            return false;
        } 
    }

    //funcion que devuelve el atributo grupos generados de un campeonato
    function getGruposGenerados(){
        $sql = " SELECT `gruposGenerados`
                FROM `campeonato`
                WHERE `idCampeonato` = '" . $this->idCampeonato . "'
        ";

        $resultado = $this->mysqli->query($sql);

        if (!($resultado)) { // Si la busqueda no da resultados, se devuelve el mensaje de que no existe
            return 'tupla inexistente';

        } else { // si existe se devuelve la tupla resultado
            $arrayRes = $resultado->fetch_array();
            return $arrayRes[0];
        }
    }

    //funcion que devuele los grupos que jugaran un playoff de un campeonato
    function getGruposPlayoff(){
        $sql = "SELECT `idGrupo`
        FROM GRUPO
        WHERE `idCampeonato` = " . $this->idCampeonato . "
            AND `numParejas` > 7
        ";

        $resultado = $this->mysqli->query($sql);

        if (!($resultado)) { // Si la busqueda no da resultados, se devuelve el mensaje de que no existe
            return 'tupla inexistente';

        } else { // si existe se devuelve la tupla resultado
            return $resultado;
        }
    }

    //funcion que actualiza el atributo playoffsGenerados
    function updateGenerarPlayoffs(){
        $sql = "UPDATE `CAMPEONATO` 
            SET `playoffsGenerados` = 'S'
            WHERE `idCampeonato` = " . $this->idCampeonato . "
            ";

        $resultado = $this->mysqli->query($sql);

        if ($resultado) {
            return true;
        } else {
            return false;
        } 
    }

    //funcion que devuelve el atributo playoffsGenerados
    function getPlayoffsGenerados(){
        $sql = " SELECT `playoffsGenerados`
                FROM `campeonato`
                WHERE `idCampeonato` = '" . $this->idCampeonato . "'
        ";

        $resultado = $this->mysqli->query($sql);

        if (!($resultado)) { // Si la busqueda no da resultados, se devuelve el mensaje de que no existe
            return 'tupla inexistente';
            
        } else { // si existe se devuelve la tupla resultado
            $arrayRes = $resultado->fetch_array();
            return $arrayRes[0];
        }
    }

    //funcion que devuelve los playoffs de un campeonato
    function getPlayoffs(){
        $sql = " SELECT *
                FROM `PLAYOFF`
                WHERE `idCampeonato` = '" . $this->idCampeonato . "'
        ";

        $resultado = $this->mysqli->query($sql);

        if (!($resultado)) { // Si la busqueda no da resultados, se devuelve el mensaje de que no existe
            return 'tupla inexistente';

        } else { // si existe se devuelve la tupla resultado
            return $resultado;
        }
    }
}
?>