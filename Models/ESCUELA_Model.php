<?php
class ESCUELA_Model {

    var $idEscuela;//Declaración del atributo idEscuela
    var $nombre;//Declaración del atributo nombre
    var $telefono;//Declaración del atributo telefono
    var $fecha;//Declaración del atributo fecha
    var $hora;//Declaración del atributo hora
    var $idPista;//Declaración del atributo idPista
    var $numPart;//Declaración del atributo numPart
    var $nivel;//Declaración del atributo nivel
    var $loginEnt;//Declaración del atributo loginEnt
    var $login1;//Declaración del atributo login1
    var $login2;//Declaración del atributo login2
    var $login3;//Declaración del atributo login3
    var $login4;//Declaración del atributo login4
    var $login5;//Declaración del atributo login5
    var $login6;//Declaración del atributo login6
    var $login7;//Declaración del atributo login7
    var $login8;//Declaración del atributo login8
    
    var $mysqli;

    //Constructor de la clase
    function __construct($idEscuela,$nombre,$telefono,$fecha,$hora, $idPista, $numPart, $nivel, $loginEnt,$login1,$login2,$login3,$login4,$login5,$login6,$login7,$login8){
    $this->idEscuela = $idEscuela;//Definición de la variable idEscuela
    $this->nombre = $nombre;//Definición de la variable nombre
    $this->telefono = $telefono;//Definición de la variable telefono
    $this->fecha = $fecha;//Definición de la variable fecha
    $this->hora = $hora;//Definición de la variable hora
    $this->idPista = $idPista;//Definición de la variable idPista
    $this->numPart = $numPart;//Definición de la variable numPart
    $this->nivel = $nivel;//Definición de la variable nivel
    $this->loginEnt = $loginEnt;//Definición de la variable loginEnt
    $this->login1 = $login1;//Definición de la variable login1
    $this->login2 = $login2;//Definición de la variable login2
    $this->login3 = $login3;//Definición de la variable login3
    $this->login4 = $login4;//Definición de la variable login4
    $this->login5 = $login5;//Definición de la variable login5
    $this->login6 = $login6;//Definición de la variable login6
    $this->login7 = $login7;//Definición de la variable login7
    $this->login8 = $login8;//Definición de la variable login8

    include_once '../Models/Access_DB.php';
    $this->mysqli = ConnectDB();
    }
    
    //Funcion ADD: añade una visita
    function ADD(){
        $sql; //Variable para guardar la sentencia SQL
        $resultado; //Variable para guardar el resultado de la sentencia SQL


        $sql = "INSERT INTO `ESCUELADEPORTIVA` (
                 `nombre`,
                 `telefono`,
                 `fecha`,
                 `hora`,
                 `idPista`,
                 `numPart`,
                 `nivel`,
                 `loginEnt`)
                VALUES(
                '".$this->nombre."',
                '".$this->telefono."',
                '".$this->fecha."',
                '".$this->hora."',
                '".$this->idPista."',
                '".$this->numPart."',
                '".$this->nivel."',
                '".$this->loginEnt."'
                )";
        if(!$this->mysqli->query($sql)){ //Si la ejecución del insert da error
            return 'error insertando'; //Muesta un mensaje y vuelve al showall
            
        }else{ //Si la ejecución del insert es correcta
            return 'exito insertando'; //Muestra un mensaje y vuelve al showall
        }


    }
   
    //funcion DELETE : comprueba que la tupla a borrar existe y una vez verificado la borra
    function DELETE(){
            $sql = "DELETE FROM ESCUELADEPORTIVA WHERE (`idEscuela` = '$this->idEscuela')";

        if (!$this->mysqli->query($sql)) {//Si la ejecución del delete da error
            return 'Error en la eliminación';//Muesta un mensaje y vuelve al showall
        } else { //Si la ejecución del delete es correcta
            return 'Eliminación realizada con éxito';//Muesta un mensaje y vuelve al showall
        }
    }

    //funcion ADDPLAYER : añade el login del deportista al partido
    function ADDPLAYER(){
        $sql; //Variable para guardar la sentencia SQL
        $resultado; //Variable para guardar el resultado de la sentencia SQ
       if($this->numPart == '0'){
        $sql = "UPDATE `ESCUELADEPORTIVA` SET `login1`='$this->login1', `numPart`='1' WHERE (`idEscuela` = '$this->idEscuela')";
       }else{
            if($this->numPart == '1'){
                $sql = "UPDATE `ESCUELADEPORTIVA` SET `login2`='$this->login1',  `numPart`='2' 
                        WHERE (`idEscuela` = '$this->idEscuela')";
            }else{
                if($this->numPart == '2'){
                    $sql = "UPDATE `ESCUELADEPORTIVA` SET `login3`='$this->login1',  `numPart`='3' 
                            WHERE (`idEscuela` = '$this->idEscuela')";
                }else{
                    if($this->numPart == '3'){
                        $sql = "UPDATE `ESCUELADEPORTIVA` SET `login4`='$this->login1',  `numPart`='4' 
                                WHERE (`idEscuela` = '$this->idEscuela')";
                    }else{
                        if($this->numPart == '4'){
                            $sql = "UPDATE `ESCUELADEPORTIVA` SET `login5`='$this->login1',  `numPart`='5' 
                                    WHERE (`idEscuela` = '$this->idEscuela')";
                        }else{
                            if($this->numPart == '5'){
                            $sql = "UPDATE `ESCUELADEPORTIVA` SET `login6`='$this->login1',  `numPart`='6' 
                                    WHERE (`idEscuela` = '$this->idEscuela')";
                            }else{
                                if($this->numPart == '6'){
                                    $sql = "UPDATE `ESCUELADEPORTIVA` SET `login7`='$this->login1',  `numPart`='7' 
                                            WHERE (`idEscuela` = '$this->idEscuela')";
                                }else{
                                    $sql = "UPDATE `ESCUELADEPORTIVA` SET `login8`='$this->login1',  `numPart`='8' 
                                            WHERE (`idEscuela` = '$this->idEscuela')";
                                }
                            }
                        }
                    }
                }
            }
       }

       if(!$this->mysqli->query($sql)){ //Si la ejecución del insert da error
            return 'error insertando'; //Muesta un mensaje y vuelve al showall
            
        }else{ //Si la ejecución del insert es correcta
            return 'exito insertando'; //Muestra un mensaje y vuelve al showall
        }
    }

    //funcion DELETEPLAYER : elimina el login del deportista del partido
    function DELETEPLAYER($playerlogin, $num){
        $sql; //Variable para guardar la sentencia SQL
        $resultado; //Variable para guardar el resultado de la sentencia SQ
        $this->numPart = $num;

       if($this->login1 == $playerlogin){
        $sql = "UPDATE `ESCUELADEPORTIVA` SET `login1`= '$this->login2', `login2`= '$this->login3', 
                `login3` = '$this->login4', `login4` = '$this->login5', `login5` = '$this->login6',
                `login6` = '$this->login7', `login7` = '$this->login8', `login8` = NULL, `numPart`='$this->numPart' 
                WHERE (`idEscuela` = '$this->idEscuela')";
       }else{
            if($this->login2 == $playerlogin){
                $sql = "UPDATE `ESCUELADEPORTIVA` SET `login2`= '$this->login3', `login3` = '$this->login4', 
                        `login4` = '$this->login5', `login5` = '$this->login6', `login6` = '$this->login7', 
                        `login7` = '$this->login8', `login8` = NULL,  `numPart`='$this->numPart' 
                        WHERE (`idEscuela` = '$this->idEscuela')";
            }else{
                if($this->login3 == $playerlogin){
                    $sql = "UPDATE `ESCUELADEPORTIVA` SET `login3`= '$this->login4', 
                            `login4` = '$this->login5', `login5` = '$this->login6', `login6` = '$this->login7', 
                            `login7` = '$this->login8', `login8` = NULL, `numPart`='$this->numPart' 
                            WHERE (`idEscuela` = '$this->idEscuela')";
                }else{
                    if($this->login4 == $playerlogin){
                        $sql = "UPDATE `ESCUELADEPORTIVA` SET `login4`= '$this->login5', `login5` = '$this->login6',   
                                `login6` = '$this->login7', `login7` = '$this->login8', `login8` = NULL, 
                                `numPart`='$this->numPart' 
                                WHERE (`idEscuela` = '$this->idEscuela')";
                    }else{
                        if($this->login5 == $playerlogin){
                            $sql = "UPDATE `ESCUELADEPORTIVA` SET `login5`= '$this->login6', `login6` = '$this->login7', 
                                    `login7` = '$this->login8', `login8` = NULL, `numPart`='$this->numPart' 
                                    WHERE (`idEscuela` = '$this->idEscuela')";
                        }else{
                            if($this->login6 == $playerlogin){
                                $sql = "UPDATE `ESCUELADEPORTIVA` SET `login6`= '$this->login7', 
                                        `login7` = '$this->login8', `login8` = NULL, `numPart`='$this->numPart' 
                                        WHERE (`idEscuela` = '$this->idEscuela')";
                            }else{
                                if($this->login7 == $playerlogin){
                                    $sql = "UPDATE `ESCUELADEPORTIVA` SET `login7`= '$this->login8', `login8` = NULL, 
                                            `numPart`='$this->numPart' 
                                            WHERE (`idEscuela` = '$this->idEscuela')";
                                }else{
                                    $sql = "UPDATE `ESCUELADEPORTIVA` SET `login8`= NULL, `numPart`='$this->numPart' 
                                            WHERE (`idEscuela` = '$this->idEscuela')";
                                }
                            }
                        }
                    }
                }
            }
       }

       if (!$this->mysqli->query($sql)) {//Si la ejecución del delete da error
            return 'Error en la eliminación';//Muesta un mensaje y vuelve al showall
        } else { //Si la ejecución del delete es correcta
            return 'Eliminación realizada con éxito';//Muesta un mensaje y vuelve al showall
        }

    }

    //funcion FINFECHA: comprueba las visitas fuera de fecha
    function FINFECHA(){
        $hoy = date("Y-m-d");
        $sql; //variable que alberga la sentencia sql
        $resultado; //almacena la consulta sql
        $result; //almacena el valor de la variable resultado

        // construimos el sql para buscar esa clave en la tabla
        $sql = "SELECT * FROM `ESCUELADEPORTIVA` WHERE `fecha` > '$hoy'";

        $resultado = $this->mysqli->query($sql);

        if (!($resultado)) { // Si la busqueda no da resultados, se devuelve el mensaje de que no existe
            return 'tupla inexistente';
        } else { // si existe se devuelve la tupla resultado
            $result = $resultado;
            return $result;
        }
    }
    
    //Función AllData: devuelve toda la tabla
    function AllData(){
        $sql; //variable que alberga la sentencia sql
        $resultado; //almacena la consulta sql
        $result; //almacena el valor de la variable resultado

        // construimos el sql para buscar esa clave en la tabla
        $sql = "SELECT * FROM `ESCUELADEPORTIVA` ";

        $resultado = $this->mysqli->query($sql);

        if (!($resultado)) { // Si la busqueda no da resultados, se devuelve el mensaje de que no existe
            return 'tupla inexistente';
        } else { // si existe se devuelve la tupla resultado
            $result = $resultado;
            return $result;
        }
    }

 //Funcion RellenaDatos: Recupera todos los atributos de una tupla a partir de su clave
   function RellenaDatos(){
        $sql; //variable que alberga la sentencia sql
        $resultado; //almacena el resultado de la consulta sql 
        $result; //almacena el valor de la variable resultado

        //Sentencia SQL de búsqueda de la tupla
        $sql = "SELECT *
                FROM `ESCUELADEPORTIVA`
                WHERE (`idEscuela` = '" . $this->idEscuela . "'
            )";

        $resultado = $this->mysqli->query($sql);
        if (!$resultado){ //Si la busqueda no da resultado (la tupla no está en la BD)
            return 'tupla inexistente';

        }else{ //Si la búsqueda da resultado
            $result = $resultado->fetch_array();
            return $result; //Devuelve la tupla resultado

        }
    }

    // funcion EDIT: verifica que una tupla existe y la edita
    function EDIT(){
        $sql = "UPDATE ESCUELADEPORTIVA SET
                `nombre` = '$this->nombre',
                `telefono` = '$this->telefono',
                `fecha` = '$this->fecha',
                `hora` = '$this->hora',
                `idPista` = '$this->idPista',
                `numPart` = '$this->numPart',
                `nivel` = '$this->nivel',
                `login1` = '$this->login1',
                `login2` = '$this->login2',
                `login3` = '$this->login3',
                `login4` = '$this->login4',
                `login5` = '$this->login5',
                `login6` = '$this->login6',
                `login7` = '$this->login7',
                `login8` = '$this->login8'
        WHERE (`idEscuela` = '$this->idEscuela')";

        if (!$this->mysqli->query($sql)) {//Si la ejecución del update da error
            return 'Error en la inserción';//Muesta un mensaje y vuelve al showall
        } else { //Si la ejecución del update es correcta
            return 'Edición realizada con éxito';//Muesta un mensaje y vuelve al showall
        }
    }

}//fin de clase

?> 
