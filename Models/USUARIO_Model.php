<?php
class USUARIO_Model {

	var $login;
	var $password; 
	var $nombre;
	var $apellidos;
	var $email;
	var $sexo;
	var $telefono;
	var $mysqli;

	function __construct($login,$password,$nombre,$apellidos,$email,$sexo,$telefono){


		$this->login = $login;
		$this->password = $password;
		$this->nombre = $nombre;
		$this->apellidos = $apellidos;
		$this->email = $email;
		$this->sexo = $sexo;
		$this->telefono = $telefono;


		include_once '../Models/Access_DB.php';
		$this->mysqli = ConnectDB();
	}

	public function getLogin() {
		return $this->login;
	}

function NoExisteUsusario(){//usado para comprobar 

	$sql = "select * from USUARIO where login = '" . $this->login . "'";

	$result = $this->mysqli->query($sql);
		if ($result->num_rows == 1){  // existe el usuario
			return 'El usuario ya existe';
		}
		else{
	    		return true; //no existe el usuario
	    	}

	    }

function login(){

	$sql = "SELECT *
	FROM USUARIO
	WHERE login = '" . $this->login . "'
	";

	$resultado = $this->mysqli->query($sql);
	if ($resultado->num_rows == 0){
		return "El login no existe";
	}
	else{
		$tupla = $resultado->fetch_array();
		if ($tupla['password'] == $this->password){
			return true;
		}
		else{
			return "La password para este usuario no es correcta";
		}
	}
}

function AddUser(){


	$sql = "INSERT INTO USUARIO (
	login,
	password, 
	nombre,
	apellidos,
	email,
	sexo,
	telefono
	) 
	VALUES (
	'" . $this->login . "',
	'" . $this->password . "',
	'" . $this->nombre . "',
	'" . $this->apellidos . "',
	'" . $this->email . "',
	'" . $this->sexo . "',
	'" . $this->telefono . "'

	)";

	if($this->mysqli->query($sql)){
		return "exito insertando";
	}
	else{
		return 'error insertando';
		}		
}

function DELETE() {

	$sql = "SELECT * 
	FROM USUARIO
	WHERE (`login`  = '$this->login')";

	$result = $this->mysqli->query( $sql );

	if ( $result->num_rows == 1 ) {
		$sql = "DELETE FROM USUARIO 
		WHERE (`login`  = '$this->login' )";
		$this->mysqli->query( $sql );

		return true;
	} 
	else{
		return false;
	}
} 




function EDIT() {
	
	$sql = "SELECT * 
	FROM USUARIO
	WHERE (`login` = '$this->login')";
		// se ejecuta la query
	$result = $this->mysqli->query( $sql );
		// si el numero de filas es igual a uno es que lo encuentra
	if ( $result->num_rows == 1 ) { 

		$sql = "UPDATE USUARIO SET 
		`login` = '$this->login',
		`password` = '$this->password',
		`nombre` = '$this->nombre',
		`apellidos` = '$this->apellidos',
		`email` = '$this->email',
		`sexo` = '$this->sexo' ,
		`telefono` = '$this->telefono'

		WHERE ( `login`  = '$this->login')";

		$result = $this->mysqli->query( $sql );
		if(!$result){
			return "error insertando";
		}else{
		return "exito insertando";
	}
	}else{
		return "error insertando";
	}
}
	function SHOWALL() {

		$sql = "SELECT *
		FROM USUARIO "
		;


		if ( !( $resultado = $this->mysqli->query( $sql ) ) ) {
			return 'Error en la consulta sobre la base de datos';
		} else { 

			return $resultado;
		}
	}
	function SHOWCURENT() {

		$sql = "SELECT *
		FROM USUARIO
		WHERE (`login` = '$this->login')"
		;


		if ( !( $resultado = $this->mysqli->query( $sql ) ) ) {
			return "Error , No se encuentra el usuario";
		} else { 

			return $resultado;
		}
	}

function ComprobarSexo() {//comprobar si el susuario es administrador
$sql = "SELECT `sexo`
			FROM USUARIO
			WHERE (`login`  = '$this->login')";

			$result = $this->mysqli->query( $sql );

			if ( $result->num_rows == 1 ) {
			$toret=	$result->fetch_array();
				return $toret['sexo'];
			}else{
				return false;
			}
	}


	function ComprobarAdmin() {//comprobar si el susuario es administrador
$sql = "SELECT * 
			FROM ADMINISTRADOR
			WHERE (`login`  = '$this->login')";

			$result = $this->mysqli->query( $sql );

			if ( $result->num_rows == 1 ) {

				return true;
			}else{
				return false;
			}
	}

	
	function ComprobarEntrenador() {//comprobar si el susuario es administrador
$sql = "SELECT * 
			FROM ENTRENADOR
			WHERE (`login`  = '$this->login')";

			$result = $this->mysqli->query( $sql );

			if ( $result->num_rows == 1 ) {

				return true;
			}else{
				return false;
			}
	}

		function ComprobarDeportista() {//comprobar si el susuario es administrador
$sql = "SELECT * 
			FROM DEPORTISTA
			WHERE (`login`  = '$this->login')";

			$result = $this->mysqli->query( $sql );

			if ( $result->num_rows == 1 ) {

				return true;
			}else{
				return false;
			}
	}


	function devolverDeportista() {//comprobar si el susuario es administrador
$sql = "SELECT * 
			FROM DEPORTISTA
			";

			$result = $this->mysqli->query( $sql );

			

				//return $result;

			if ( !( $result = $this->mysqli->query( $sql ) ) ) {
			return 'Error en la consulta sobre la base de datos';
		} else { 

			return $result;
		}
			
	}

	function devolverEntrenador(){

        $sql = "SELECT * 
            FROM ENTRENADOR
            ";

            $result = $this->mysqli->query( $sql );

            

                //return $result;

            if ( !( $result = $this->mysqli->query( $sql ) ) ) {
            return 'Error en la consulta sobre la base de datos';
        } else { 

            return $result;
        }

    }

	function ChangePermisos($A,$E,$D) {//añade el usuario a las tablas deportista administrador y usuario

			if($A){
   
				$this->AddAdmin();
			}else{
				$this->EliminarAdmin();

			}

			if($E){
				$this->AddEntrenador();

			}else{
				$this->EliminarEntrenador();
			}

			if($D){
				$this->AddDeportista();
			}else{
				$this->EliminarDeportista();

			}
			return true;
		}
		

		function AddAdmin(){
			$sql = "SELECT * 
			FROM ADMINISTRADOR
			WHERE (`login`  = '$this->login')";

			$result = $this->mysqli->query( $sql );

			if ( $result->num_rows == 0 ) {
				$sql = "INSERT INTO ADMINISTRADOR(login)
				VALUES ('" . $this->login . "')";
				if (!$this->mysqli->query($sql)) {
					return 'Error en la inserción';
				}
				else{
					return true; 
				}		
			}
			return true;
		}
		function  AddEntrenador(){
			$sql = "SELECT * 
			FROM ENTRENADOR
			WHERE (`login`  = '$this->login')";

			$result = $this->mysqli->query( $sql );

			if ( $result->num_rows == 0 ) {
				$sql = "INSERT INTO ENTRENADOR(login)
				VALUES ('" . $this->login . "')";
				if (!$this->mysqli->query($sql)) {
					return 'Error en la inserción';
				}
				else{
					return true; 
				}		
			}
			return true; 
		}

		function AddDeportista(){
			$sql = "SELECT * 
			FROM DEPORTISTA
			WHERE (`login`  = '$this->login')";

			$result = $this->mysqli->query( $sql );

			if ( $result->num_rows == 0 ) {
				$sql = "INSERT INTO DEPORTISTA(login) 
				VALUES ('" . $this->login . "')";
				if (!$this->mysqli->query($sql)) {
					return 'Error en la inserción';
				}
				else{
					return true; 
				}		
			}
			return true;
		}

		function EliminarEntrenador(){
			$sql = "SELECT * 
			FROM ENTRENADOR
			WHERE (`login`  = '$this->login')";

			$result = $this->mysqli->query( $sql );

			if ( $result->num_rows == 1 ) {
			$sql = "DELETE FROM ENTRENADOR 
			WHERE (`login`  = '$this->login' )";
			if (!$this->mysqli->query($sql)) {
				return 'Error al eliminar';
			}
			else{
				return true; 
			}
		}
		return true;
		}
		function EliminarAdmin(){
			$sql = "SELECT * 
			FROM ADMINISTRADOR
			WHERE (`login`  = '$this->login')";

			$result = $this->mysqli->query( $sql );

			if ( $result->num_rows == 1 ) {
				$sql = "DELETE FROM ADMINISTRADOR 
				WHERE (`login`  = '$this->login' )";
				if (!$this->mysqli->query($sql)) {
					return 'Error al eliminar';
				}
				else{
					return true; 
				}
			}
			return true;
		}

		function EliminarDeportista(){
			$sql = "SELECT * 
			FROM DEPORTISTA
			WHERE (`login`  = '$this->login')";

			$result = $this->mysqli->query( $sql );

			if ( $result->num_rows == 1 ) {
			$sql = "DELETE FROM DEPORTISTA 
			WHERE (`login`  = '$this->login' )";
			if (!$this->mysqli->query($sql)) {
				return 'Error al eliminar';
			}
			else{
				return true; 
			}
		}
		return true; 
		}
	}

		?>