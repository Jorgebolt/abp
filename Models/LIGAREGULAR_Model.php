<?php
class LIGAREGULAR_Model
{
    var $idLiga;
    var $idCampeonato;
    var $numPart;
    var $login1;
    var $login2;
    var $login3;
    var $login4;
    var $login5;
    var $login6;
    var $login7;
    var $login8;
    var $mysqli;

    function __construct($idLiga, $idCampeonato, $numPart, $login1, $login2,  $login3, $login4, $login5, $login6,  $login7, $login8)
    {
        $this->idLiga = $idLiga;
        $this->idCampeonato = $idCampeonato;
        $this->numPart = $numPart;
        $this->login1 = $login1;
        $this->login2 = $login2;
        $this->login3 = $login3;
        $this->login4 = $login4;
        $this->login5 = $login5;
        $this->login6 = $login6;
        $this->login7 = $login7;
        $this->login8 = $login8;

        include_once '../Models/Access_DB.php';
        $this->mysqli = ConnectDB();
    }

    function ADD()
    {
        $sql = "INSERT INTO `ligaRegular`
                VALUES
                (
                    NULL,
                    '" . $this->idCampeonato . "',
                    0,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL
                )";

        if (!$this->mysqli->query($sql)) { //si la insercion da error
            return false;
        } else {
            return true;
        }
    }

    //funcion que devuelve los participantes de una liga
    function getParticipantes()
    {
        $sql = "SELECT login1, login2, login3, login4, login5, login6, login7, login8
                FROM `ligaRegular`
                WHERE `idLiga` = '" . $this->idLiga . "'
                ";

        $resultado = $this->mysqli->query($sql);

        if ($resultado) { //Si la busqueda da resultado
            $result = $resultado->fetch_array();
            return $result; //Devuelve la tupla resultado

        } else { //Si la búsqueda da error
            return 'false'; //Devuelve la tupla resultado
        }
    }

    //funcion que inserta el login del deportista a la liga
    function ADDPLAYER()
    {
        if ($this->numPart == '0') {
            $sql = "UPDATE `ligaRegular` SET `login1`='$this->login1', `numPart`='1' 
            WHERE (`idLiga` = '$this->idLiga')";
        
        } else if ($this->numPart == '1') {
            $sql = "UPDATE `ligaRegular` SET `login2`='$this->login1',  `numPart`='2' 
                    WHERE (`idLiga` = '$this->idLiga')";

        } else if ($this->numPart == '2') {
            $sql = "UPDATE `ligaRegular` SET `login3`='$this->login1',  `numPart`='3' 
                    WHERE (`idLiga` = '$this->idLiga')";

        } else if ($this->numPart == '3') {
            $sql = "UPDATE `ligaRegular` SET `login4`='$this->login1',  `numPart`='4' 
                    WHERE (`idLiga` = '$this->idLiga')";

        } else if ($this->numPart == '4') {
            $sql = "UPDATE `ligaRegular` SET `login5`='$this->login1',  `numPart`='5' 
                    WHERE (`idLiga` = '$this->idLiga')";

        } else if ($this->numPart == '5') {
            $sql = "UPDATE `ligaRegular` SET `login6`='$this->login1',  `numPart`='6' 
                    WHERE (`idLiga` = '$this->idLiga')";

        } else if ($this->numPart == '6') {
            $sql = "UPDATE `ligaRegular` SET `login7`='$this->login1',  `numPart`='7' 
                    WHERE (`idLiga` = '$this->idLiga')";

        }else{
            $sql = "UPDATE `ligaRegular` SET `login8`='$this->login1',  `numPart`='8' 
                    WHERE (`idLiga` = '$this->idLiga')";

        }
        
        if (!$this->mysqli->query($sql)) { //Si la ejecución del upda da error
            return false; //Muesta un mensaje y vuelve al showall

        } else { //Si la ejecución del upda es correcta
            return true; //Muestra un mensaje y vuelve al showall
        }
    }
    

    function DELETE()
    { }

    function AllData()
    {
        // construimos el sql para buscar esa clave en la tabla
        $sql = "SELECT * FROM `ligaRegular` ";

        $resultado = $this->mysqli->query($sql);

        if (!($resultado)) { // Si la busqueda no da resultados, se devuelve el mensaje de que no existe
            return 'tupla inexistente';
        } else { // si existe se devuelve la tupla resultado
            $result = $resultado;
            return $result;
        }
    }

    function RellenaDatos()
    {
        //Sentencia SQL de búsqueda de la tupla
        $sql = "SELECT *
                FROM `ligaRegular`
                WHERE (`idLiga` = '" . $this->idLiga . "'
            )";

        $resultado = $this->mysqli->query($sql);

        if (!$resultado) { //Si la busqueda no da resultado (la tupla no está en la BD)
            return 'tupla inexistente';
        } else { //Si la búsqueda da resultado
            $result = $resultado->fetch_array();
            return $result; //Devuelve la tupla resultado
        }
    }
}