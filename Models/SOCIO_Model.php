<?php
class SOCIO_Model{

    var $idSocio;
    var $loginUsuario;
    var $pago;
    var $numTarjeta;
    var $numSeguridad;
    var $mysqli;

    function __construct($idSocio, $loginUsuario, $pago, $numTarjeta, $numSeguridad){
        $this->idSocio = $idSocio;
        $this->loginUsuario = $loginUsuario;
        $this->pago = $pago;
        $this->numTarjeta = $numTarjeta;
        $this->numSeguridad = $numSeguridad;
        
        include '../Models/Access_DB.php';
        $this->mysqli = ConnectDB();
    }

    function ADD(){
        $sql = "INSERT INTO SOCIO (
            loginUsuario,
            pago,
            numTarjeta, 
            numSeguridad
            ) 
            VALUES (
                '" . $this->loginUsuario . "',
                '" . $this->pago . "',
                '" . $this->numTarjeta . "',
                '" . $this->numSeguridad . "'

                )";
        if($this->mysqli->query($sql)){
            return 'pago realizado con exito';
        }
        else{
            return 'error en el pago';
        }       
    }

    //function SEARCH(){

    //}

    function EDIT(){

    }

    function DELETE(){
        $sql = "DELETE FROM SOCIO WHERE (`idSocio` = '$this->idSocio')";

        if (!$this->mysqli->query($sql)) {//Si la ejecución del delete da error
            return 'Error en la eliminación';//Muesta un mensaje y vuelve al showall
        } else { //Si la ejecución del delete es correcta
            return 'Eliminación realizada con éxito';//Muesta un mensaje y vuelve al showall
        }
    }

    //Función que devuelve toda la tabla
    function AllData()
    {
        $sql; //variable que alberga la sentencia sql
        $result; //almacena el valor de la variable resultado
        // construimos el sql para buscar esa clave en la tabla
        $sql = "SELECT * FROM `socio` ";

        $resultado = $this->mysqli->query($sql);

        if (!($resultado)) { // Si la busqueda no da resultados, se devuelve el mensaje de que no existe
            return 'tupla inexistente';
        } else { // si existe se devuelve la tupla resultado
            $result = $resultado;
            return $result;
        }
    }

    function datosSocio($loginSocio)
    {
        $sql; //variable que alberga la sentencia sql
        $result; //almacena el valor de la variable resultado
        // construimos el sql para buscar esa clave en la tabla
        $login = $loginSocio;
        $sql = "SELECT * FROM `socio` WHERE `loginUsuario` = '" . $login . "'";

        $resultado = $this->mysqli->query($sql);
        $resultado2 = $resultado->fetch_assoc();

        if (!($resultado2)) { // Si la busqueda no da resultados, se devuelve el mensaje de que no existe
            return 'tupla inexistente';
        } else { // si existe se devuelve la tupla resultado
            $result = $resultado2;
            return $result;
        }
    }

    //Recupera todos los atributos de una tupla a partir de su clave
    function RellenaDatos()
    {
       //Sentencia SQL de búsqueda de la tupla
        $sql = "SELECT *
                FROM `socio`
                WHERE (`idSocio` = '" . $this->idSocio . "'
            )";

        $resultado = $this->mysqli->query($sql);

        if (!$resultado) { //Si la busqueda no da resultado (la tupla no está en la BD)
            return 'tupla inexistente';
        } else { //Si la búsqueda da resultado
            $result = $resultado->fetch_array();
            return $result; //Devuelve la tupla resultado
        }
    }
}
?>