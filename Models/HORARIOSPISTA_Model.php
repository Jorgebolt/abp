<?php
class HORARIOSPISTA_Model{

    var $idHorario;
    var $fecha;
    var $hora1;
    var $hora2;
    var $hora3;
    var $hora4;
    var $hora5;
    var $hora6;
    var $hora7;
    var $hora8;
    var $hora9;
    var $mysqli;

    function __construct($idHorario, $fecha, $hora1, $hora2, $hora3, $hora4, $hora5, $hora6, $hora7, $hora8, $hora9){
        $this->idHorario = $idHorario;
        $this->fecha = $fecha;
        $this->hora1 = $hora1;
        $this->hora2 = $hora2;
        $this->hora3 = $hora3;
        $this->hora4 = $hora4;
        $this->hora5 = $hora5;
        $this->hora6 = $hora6;
        $this->hora7 = $hora7;
        $this->hora8 = $hora8;
        $this->hora9 = $hora9;
        
        include_once '../Models/Access_DB.php';
        $this->mysqli = ConnectDB();
    }
    //Función que devuelve toda la tabla
    function AllData()
    {
        $sql; //variable que alberga la sentencia sql
        $result; //almacena el valor de la variable resultado
        // construimos el sql para buscar esa clave en la tabla
        $sql = "SELECT * FROM `horarioPista` ";

        $resultado = $this->mysqli->query($sql);

        if (!($resultado)) { // Si la busqueda no da resultados, se devuelve el mensaje de que no existe
            return 'tupla inexistente';
        } else { // si existe se devuelve la tupla resultado
            $result = $resultado;
            return $result;
        }
    }

    //funcion FINFECHA: comprueba las visitas fuera de fecha
    function FINFECHA(){
        $hoy = date("Y-m-d");
        $sql; //variable que alberga la sentencia sql
        $resultado; //almacena la consulta sql
        $result; //almacena el valor de la variable resultado

        // construimos el sql para buscar esa clave en la tabla
        $sql = "SELECT * FROM `horarioPista` WHERE `fecha` > '$hoy'";

        $resultado = $this->mysqli->query($sql);

        if (!($resultado)) { // Si la busqueda no da resultados, se devuelve el mensaje de que no existe
            return 'tupla inexistente';
        } else { // si existe se devuelve la tupla resultado
            $result = $resultado;
            return $result;
        }
    }

    //Recupera todos los atributos de una tupla a partir de su clave
    function RellenaDatos()
    {
       //Sentencia SQL de búsqueda de la tupla
        $sql = "SELECT *
                FROM `horario`
                WHERE (`idHorarioPista` = '" . $this->idHorario . "'
            )";

        $resultado = $this->mysqli->query($sql);

        if (!$resultado) { //Si la busqueda no da resultado (la tupla no está en la BD)
            return 'tupla inexistente';
        } else { //Si la búsqueda da resultado
            $result = $resultado->fetch_array();
            return $result; //Devuelve la tupla resultado
        }
    }
}
?>