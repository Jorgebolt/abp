<?php
class PARTIDO_Model {

    var $idPartido;//Declaración del atributo idPartido
    var $fecha;//Declaración del atributo fecha
    var $numPart;//Declaración del atributo numPart
    var $hora;//Declaración del atributo hora
    var $login1;//Declaración del atributo login1
    var $login2;//Declaración del atributo login2
    var $login3;//Declaración del atributo login3
    var $login4;//Declaración del atributo login4
    var $idPista;//Declaración del atributo idPista
    
    var $mysqli;

    //Constructor de la clase
    function __construct($idPartido,$fecha,$numPart,$hora,$login1,$login2,$login3,$login4,$idPista){
    $this->idPartido = $idPartido;//Definición de la variable idPartido
    $this->fecha = $fecha;//Definición de la variable fecha
    $this->numPart = $numPart;//Definición de la variable numPart
    $this->hora = $hora;//Definición de la variable hora
    $this->login1 = $login1;//Definición de la variable login1
    $this->login2 = $login2;//Definición de la variable login2
    $this->login3 = $login3;//Definición de la variable login3
    $this->login4 = $login4;//Definición de la variable login4
    $this->idPista = $idPista;//Definición de la variable idPista

    include_once '../Models/Access_DB.php';
    $this->mysqli = ConnectDB();
    }
    
    //Funcion ADD: añade una visita
    function ADD(){
        $sql; //Variable para guardar la sentencia SQL
        $resultado; //Variable para guardar el resultado de la sentencia SQL


        $sql = "INSERT INTO `Partido` (
                 `fecha`,
                 `numPart`,
                 `hora`,
                 `idPista`)
                VALUES(
                 '".$this->fecha."',
                 '".$this->numPart."',
                 '".$this->hora."',
                 '".$this->idPista."'
                )";
        if(!$this->mysqli->query($sql)){ //Si la ejecución del insert da error
            return 'error insertando'; //Muesta un mensaje y vuelve al showall
            
        }else{ //Si la ejecución del insert es correcta
            return 'exito insertando'; //Muestra un mensaje y vuelve al showall
        }


    }
   
    //funcion DELETE : comprueba que la tupla a borrar existe y una vez verificado la borra
    function DELETE(){
            $sql = "DELETE FROM PARTIDO WHERE (`idPartido` = '$this->idPartido')";

        if (!$this->mysqli->query($sql)) {//Si la ejecución del delete da error
            return 'Error en la eliminación';//Muesta un mensaje y vuelve al showall
        } else { //Si la ejecución del delete es correcta
            return 'Eliminación realizada con éxito';//Muesta un mensaje y vuelve al showall
        }
    }

    //funcion ADDPLAYER : añade el login del deportista al partido
    function ADDPLAYER(){
        $sql; //Variable para guardar la sentencia SQL
        $resultado; //Variable para guardar el resultado de la sentencia SQ
       if($this->numPart == '0'){
        $sql = "UPDATE `Partido` SET `login1`='$this->login1', `numPart`='1' WHERE (`idPartido` = '$this->idPartido')";
       }else{
            if($this->numPart == '1'){
                $sql = "UPDATE `Partido` SET `login2`='$this->login1',  `numPart`='2' 
                        WHERE (`idPartido` = '$this->idPartido')";
            }else{
                if($this->numPart == '2'){
                    $sql = "UPDATE `Partido` SET `login3`='$this->login1',  `numPart`='3' 
                            WHERE (`idPartido` = '$this->idPartido')";
                }else{
                    $sql = "UPDATE `Partido` SET `login4`='$this->login1',  `numPart`='4' 
                            WHERE (`idPartido` = '$this->idPartido')";
                }
            }
       }

       if(!$this->mysqli->query($sql)){ //Si la ejecución del insert da error
            return 'error insertando'; //Muesta un mensaje y vuelve al showall
            
        }else{ //Si la ejecución del insert es correcta
            return 'exito insertando'; //Muestra un mensaje y vuelve al showall
        }
    }

    //funcion DELETEPLAYER : elimina el login del deportista del partido
    function DELETEPLAYER($playerlogin, $num){
        $sql; //Variable para guardar la sentencia SQL
        $resultado; //Variable para guardar el resultado de la sentencia SQ
        $this->numPart = $num;

       if($this->login1 == $playerlogin){
        $sql = "UPDATE `Partido` SET `login1`= '$this->login2', `login2`= '$this->login3', 
                `login3` = '$this->login4', `login4` = NULL, `numPart`='$this->numPart' WHERE (`idPartido` = '$this->idPartido')";
       }else{
            if($this->login2 == $playerlogin){
                $sql = "UPDATE `Partido` SET `login2`= '$this->login3', 
                        `login3` = '$this->login4', `login4` = NULL, `numPart`='$this->numPart' 
                        WHERE (`idPartido` = '$this->idPartido')";
            }else{
                if($this->login3 == $playerlogin){
                    $sql = "UPDATE `Partido` SET `login3`= '$this->login4', `login4` = NULL, `numPart`='$this->numPart' 
                            WHERE (`idPartido` = '$this->idPartido')";
                }else{
                    $sql = "UPDATE `Partido` SET `login4`= NULL, `numPart`='$this->numPart' 
                            WHERE (`idPartido` = '$this->idPartido')";
                }
            }
       }

       if (!$this->mysqli->query($sql)) {//Si la ejecución del delete da error
            return 'Error en la eliminación';//Muesta un mensaje y vuelve al showall
        } else { //Si la ejecución del delete es correcta
            return 'Eliminación realizada con éxito';//Muesta un mensaje y vuelve al showall
        }

    }
    
    //Función AllData: devuelve toda la tabla
    function AllData(){
        $sql; //variable que alberga la sentencia sql
        $resultado; //almacena la consulta sql
        $result; //almacena el valor de la variable resultado

        // construimos el sql para buscar esa clave en la tabla
        $sql = "SELECT * FROM `PARTIDO` ";

        $resultado = $this->mysqli->query($sql);

        if (!($resultado)) { // Si la busqueda no da resultados, se devuelve el mensaje de que no existe
            return 'tupla inexistente';
        } else { // si existe se devuelve la tupla resultado
            $result = $resultado;
            return $result;
        }
    }

 //Funcion RellenaDatos: Recupera todos los atributos de una tupla a partir de su clave
   function RellenaDatos(){
        $sql; //variable que alberga la sentencia sql
        $resultado; //almacena el resultado de la consulta sql 
        $result; //almacena el valor de la variable resultado

        //Sentencia SQL de búsqueda de la tupla
        $sql = "SELECT *
                FROM `PARTIDO`
                WHERE (`idPartido` = '" . $this->idPartido . "'
            )";

        $resultado = $this->mysqli->query($sql);
        if (!$resultado){ //Si la busqueda no da resultado (la tupla no está en la BD)
            return 'tupla inexistente';

        }else{ //Si la búsqueda da resultado
            $result = $resultado->fetch_array();
            return $result; //Devuelve la tupla resultado

        }
    }

}//fin de clase

?> 
