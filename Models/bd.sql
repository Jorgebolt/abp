DROP DATABASE IF EXISTS `padelbd`;
DROP USER `admin`@`localhost`;

CREATE DATABASE `padelbd` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;

USE `padelbd`;

-- GRANT USAGE ON * . * TO `padelbd`@`localhost`;
--
-- CREAMOS EL USUARIO Y LE DAMOS PASSWORD,DAMOS PERMISO DE USO Y DAMOS PERMISOS SOBRE LA BASE DE DATOS.
--
CREATE USER IF NOT EXISTS `admin`@`localhost` IDENTIFIED BY 'admin';
-- GRANT USAGE ON *.* TO `admin`@`localhost` REQUIRE NONE WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;
GRANT ALL PRIVILEGES ON `padelbd`.* TO `admin`@`localhost` WITH GRANT OPTION;

CREATE TABLE `contenido` (
	`idcontenido` int(4) AUTO_INCREMENT,
	`titulo` varchar(60),
	`descripcion` varchar(100),
	`login` varchar(15),

	PRIMARY KEY(`idcontenido`),
	FOREIGN KEY(`login`) REFERENCES ADMINISTRADOR(`login`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `jornadaIntensiva` (
	`idJornada` int(4) AUTO_INCREMENT,
	`nivel` enum('amateur','intermedio','profesional') NOT NULL,
	`loginUser` varchar(15),
	`loginEntrenador` varchar(15),
	`fechaJornada` date,

	PRIMARY KEY(`idJornada`),
	FOREIGN KEY(`loginUser`) REFERENCES USUARIO(`login`),
	FOREIGN KEY(`loginEntrenador`) REFERENCES ENTRENADOR(`login`)

) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `administrador` (

	`login` varchar(15),

	PRIMARY KEY(`login`),
	FOREIGN KEY(`login`) REFERENCES USUARIO(`login`)

) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `usuario` (
	
	`login` varchar(15),
	`password` varchar(128) NOT NULL,
	`nombre` varchar(15) NOT NULL,
	`apellidos` varchar(50) NOT NULL,
	`email` varchar(60) NOT NULL,
	`sexo` enum('hombre', 'mujer') NOT NULL,
	`telefono` varchar(11) NOT NULL,

	PRIMARY KEY (`login`),
	UNIQUE KEY `email` (`email`)

) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `entrenador` (
	
	`login` varchar(15),

	PRIMARY KEY(`login`),
	FOREIGN KEY(`login`) REFERENCES USUARIO(`login`)


) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `deportista` (

	`login` varchar(15),

	PRIMARY KEY(`login`),
	FOREIGN KEY(`login`) REFERENCES USUARIO(`login`)

) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `categoria` (
	
	`nivel` varchar(15) NOT NULL,
	`sexo`enum('hombre', 'mujer' , 'mixto') NOT NULL,
	`idCategoria` int(4) AUTO_INCREMENT,

	PRIMARY KEY (`idCategoria`)

) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `campeonato` (
	
	`idCampeonato` int(4) AUTO_INCREMENT,
	`numParticipantes` int(4),
	`fechaLimiteInscrip` date NOT NULL,
	`gruposGenerados` varchar(1) default 'N',
	`playoffsGenerados` varchar(1) default 'N',

	PRIMARY KEY(`idCampeonato`)

) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `pareja` (
	
	`login1` varchar(15),
	`login2` varchar(15),
	`idCampeonato` int(4),
	`categoria` enum('masculina','femenina','mixta') NOT NULL,
	`nivel` enum('amateur','intermedio','profesional') NOT NULL,
	`idGrupo` int(4),
	`idPlayoff` int(4),
	`puntosLiga` int(4),
	`posPlayoff` int(4),
	
	FOREIGN KEY(`idGrupo`) REFERENCES GRUPO(`idGrupo`),
	FOREIGN KEY(`idPlayoff`) REFERENCES PLAYOFF(`idPlayoff`),
	FOREIGN KEY(`idCampeonato`) REFERENCES CAMPEONATO(`idCampeonato`),
	FOREIGN KEY(`login1`) REFERENCES DEPORTISTA(`login`),
	FOREIGN KEY(`login2`) REFERENCES DEPORTISTA(`login`),
	PRIMARY KEY(`login1`,`login2`,`idCampeonato`)

) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `grupo` (
	`idGrupo` int(4) AUTO_INCREMENT,
	`categoria` enum('masculina','femenina','mixta') NOT NULL,
	`nivel` enum('amateur','intermedio','profesional') NOT NULL,
	`idCampeonato` int(4) NOT NULL,
	`numParejas` int(4) NOT NULL,

	PRIMARY KEY(`idGrupo`),
    FOREIGN KEY(`idCampeonato`) REFERENCES CAMPEONATO(`idCampeonato`)
	
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `playOff`(

	`idplayOff` int(4) AUTO_INCREMENT,
	`idGrupo` int(4),
	`idCampeonato` int(4),
	`categoria` enum('masculina','femenina','mixta') NOT NULL,
	`nivel` enum('amateur','intermedio','profesional') NOT NULL,
	`ganador` varchar(50) DEFAULT NULL,

	PRIMARY KEY(`idplayOff`),
    FOREIGN KEY(`idGrupo`) REFERENCES CAMPEONATO(`idGrupo`),
    FOREIGN KEY(`idCampeonato`) REFERENCES CAMPEONATO(`idCampeonato`)

) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `enfrentamiento` (

	`idEnfrentamiento` int(4) AUTO_INCREMENT,
	`fecha` date NOT NULL,
	`hora` varchar(5) NOT NULL,
	`idPista` int(4) NOT NULL,
	`ronda` int(1) NOT NULL,
	`login1` varchar(15) NOT NULL,
	`login2` varchar(15) NOT NULL,
	`login3` varchar(15) NOT NULL,
	`login4` varchar(15) NOT NULL,
	
	PRIMARY KEY(`idEnfrentamiento`),
	FOREIGN KEY(`login1`) REFERENCES DEPORTISTA(`login`),
	FOREIGN KEY(`login2`) REFERENCES DEPORTISTA(`login`),
    FOREIGN KEY(`login3`) REFERENCES DEPORTISTA(`login`),
	FOREIGN KEY(`login4`) REFERENCES DEPORTISTA(`login`)

) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `partido` (

	`idPartido` int(4) AUTO_INCREMENT,
	`fecha` date NOT NULL,
	`numPart` int(1) NOT NULL,
	`hora` varchar(5) NOT NULL,
	`login1` varchar(15),
	`login2` varchar(15),
	`login3` varchar(15),
	`login4` varchar(15),
	`idPista` int(4) NOT NULL,
	
	PRIMARY KEY(`idPartido`),
    FOREIGN KEY(`login1`) REFERENCES DEPORTISTA(`login`),
	FOREIGN KEY(`login2`) REFERENCES DEPORTISTA(`login`),
    FOREIGN KEY(`login3`) REFERENCES DEPORTISTA(`login`),
	FOREIGN KEY(`login4`) REFERENCES DEPORTISTA(`login`),
	FOREIGN KEY(`idPista`) REFERENCES PISTA(`idPista`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- puede que falte descripcion
CREATE TABLE `pista` (

	`idPista` int(4 ) AUTO_INCREMENT,
	`idHorario` int(4),
	`descripcion` varchar(255),

	PRIMARY KEY (`idPista`),
	FOREIGN KEY(`idHorario`) REFERENCES HORARIOPISTA(`idHorario`)

) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `clase` (

	`idClase` int(4) AUTO_INCREMENT,
	`loginParticipante` varchar(15),
	`entrenador` varchar(15),
	`fecha` date NOT NULL,
	`hora` varchar(5) NOT NULL,
	`idPista` int(4) NOT NULL,

	PRIMARY KEY (`idClase`),
	FOREIGN KEY(`loginParticipante`) REFERENCES USUARIO(`login`),
	FOREIGN KEY(`entrenador`) REFERENCES ENTRENADOR(`login`),
	FOREIGN KEY(`idPista`) REFERENCES PISTA(`idPista`)

) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `horarioPista` (
	`idHorario` int(4) AUTO_INCREMENT,
	`hora1` varchar(5) NOT NULL,
	`hora2` varchar(5) NOT NULL,
	`hora3` varchar(5) NOT NULL,
	`hora4` varchar(5) NOT NULL,
	`hora5` varchar(5) NOT NULL,
	`hora6` varchar(5) NOT NULL,
	`hora7` varchar(5) NOT NULL,
	`hora8` varchar(5) NOT NULL,
	`hora9` varchar(5) NOT NULL,
	
	`horaInicio` varchar(5) NOT NULL,
	`horaFin` varchar(5) NOT NULL,
	`fecha` date,

	PRIMARY KEY (`idHorario`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `reserva` (
	`idReserva` int(4 ) AUTO_INCREMENT,
	`fecha` date NOT NULL,
	`login` varchar(15),
	`hora` int(4) NOT NULL,
	`idPista` int(4) NOT NULL,

	
	PRIMARY KEY (`idReserva`),
	FOREIGN KEY(`login`) REFERENCES DEPORTISTA(`login`),
	FOREIGN KEY(`hora`) REFERENCES HORARIO(`idHorario`),
	FOREIGN KEY(`idPista`) REFERENCES PISTA(`idPista`)

) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `escuelaDeportiva` (
	`idEscuela` int(4 ) AUTO_INCREMENT,
	`nombre` varchar(15) NOT NULL,
	`telefono` varchar(11) NOT NULL,
	`fecha` date NOT NULL,
	`hora` varchar(5) NOT NULL,
	`idPista` int(4 ) NOT NULL,
	`numPart` int(4),
	`nivel` enum('amateur','intermedio','profesional') NOT NULL,
	`loginEnt` varchar(15),
	`login1` varchar(15),
	`login2` varchar(15),
	`login3` varchar(15),
	`login4` varchar(15),
	`login5` varchar(15),
	`login6` varchar(15),
	`login7` varchar(15),
	`login8` varchar(15),

	FOREIGN KEY(`loginEnt`) REFERENCES ENTRENADOR(`login`),
	FOREIGN KEY(`login1`) REFERENCES DEPORTISTA(`login`),
	FOREIGN KEY(`login2`) REFERENCES DEPORTISTA(`login`),
    FOREIGN KEY(`login3`) REFERENCES DEPORTISTA(`login`),
	FOREIGN KEY(`login4`) REFERENCES DEPORTISTA(`login`),
	FOREIGN KEY(`login5`) REFERENCES DEPORTISTA(`login`),
	FOREIGN KEY(`login6`) REFERENCES DEPORTISTA(`login`),
    FOREIGN KEY(`login7`) REFERENCES DEPORTISTA(`login`),
	FOREIGN KEY(`login8`) REFERENCES DEPORTISTA(`login`),
	
	PRIMARY KEY (`idEscuela`),
	UNIQUE KEY `nombre` (`nombre`)

) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `reservasRealizadas`(
	`idReserva` int(4 ) AUTO_INCREMENT,
	`login` varchar(15),
	`idPista` int(4) NOT NULL,
	`fecha` date NOT NULL,
	`hora` varchar(5) NOT NULL,
	
	PRIMARY KEY (`idReserva`),
	FOREIGN KEY(`idPista`) REFERENCES PISTA(`idPista`),
	FOREIGN KEY(`login`) REFERENCES DEPORTISTA(`login`)
	
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `socio` (
	`idSocio` int(4) AUTO_INCREMENT,
	`loginUsuario` varchar(15),
	`pago` ENUM('S','N') NOT NULL default 'N',
	`numTarjeta` varchar(16) NOT NULL,
	`numSeguridad` int(3) NOT NULL,
	
	
	PRIMARY KEY (`idSocio`),
	FOREIGN KEY(`loginUsuario`) REFERENCES USUARIO(`login`)
	

) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `estadistica` (
	`idEstadistica` int(4) AUTO_INCREMENT,
	`nombre` varchar(15) NOT NULL,
	`adminLogin` varchar(15) NOT NULL,
	`loginUsuario` varchar(15),
	
	
	PRIMARY KEY (`idEstadistica`),
	FOREIGN KEY(`loginUsuario`) REFERENCES USUARIO(`login`),
	FOREIGN KEY(`adminLogin`) REFERENCES ADMINISTRADOR(`login`)

) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `horarioClases` (
	`idHorarioClases` int(4) AUTO_INCREMENT,
	`horaInicio` varchar(5) NOT NULL,
	`horaFin` varchar(5) NOT NULL,
	`fecha` date,

	PRIMARY KEY (`idHorarioClases`)
	
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `horario` (
	`idHorario` int(4) AUTO_INCREMENT,
	`horaInicio` varchar(5) NOT NULL,
	`horaFin` varchar(5) NOT NULL,
	`fecha` date,

	PRIMARY KEY (`idHorario`)
	

) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- BLOQUE DE INSERT
INSERT INTO `usuario` (`login`, `password`, `nombre`, `apellidos`, `email`, `sexo`, `telefono`) VALUES ('root', 'root', 'mario', 'perez', 'root@root.es', 'HOMBRE', '666666666');
INSERT INTO `usuario` (`login`,`password`, `nombre`, `apellidos`, `email`, `sexo`, `telefono`) VALUES ('user', 'user', 'user', 'portela', 'user@user.es', 'HOMBRE', '999999999');

INSERT INTO `usuario` (`login`,`password`, `nombre`, `apellidos`, `email`, `sexo`, `telefono`) VALUES ('sergio', 'sergio', 'sergio', 'portela', 'sergio@user.es', 'HOMBRE', '999999999');
INSERT INTO `usuario` (`login`,`password`, `nombre`, `apellidos`, `email`, `sexo`, `telefono`) VALUES ('pepe', 'pepe', 'pepe', 'portela', 'pepe@user.es', 'HOMBRE', '999999999');
INSERT INTO `usuario` (`login`,`password`, `nombre`, `apellidos`, `email`, `sexo`, `telefono`) VALUES ('juan', 'juan', 'juan', 'portela', 'juan@user.es', 'HOMBRE', '999999999');
INSERT INTO `usuario` (`login`,`password`, `nombre`, `apellidos`, `email`, `sexo`, `telefono`) VALUES ('jose', 'jose', 'jose', 'portela', 'jose@user.es', 'HOMBRE', '999999999');
INSERT INTO `usuario` (`login`,`password`, `nombre`, `apellidos`, `email`, `sexo`, `telefono`) VALUES ('mario', 'mario', 'mario', 'portela', 'mario@user.es', 'HOMBRE', '999999999');
INSERT INTO `usuario` (`login`,`password`, `nombre`, `apellidos`, `email`, `sexo`, `telefono`) VALUES ('victor', 'victor', 'victor', 'portela', 'victor@user.es', 'HOMBRE', '999999999');
INSERT INTO `usuario` (`login`,`password`, `nombre`, `apellidos`, `email`, `sexo`, `telefono`) VALUES ('rober', 'rober', 'rober', 'portela', 'rober@user.es', 'HOMBRE', '999999999');
INSERT INTO `usuario` (`login`,`password`, `nombre`, `apellidos`, `email`, `sexo`, `telefono`) VALUES ('antonio', 'antonio', 'antonio', 'portela', 'antonio@user.es', 'HOMBRE', '999999999');
INSERT INTO `usuario` (`login`,`password`, `nombre`, `apellidos`, `email`, `sexo`, `telefono`) VALUES ('juanfran', 'juanfran', 'juanfran', 'portela', 'juanfran@user.es', 'HOMBRE', '999999999');
INSERT INTO `usuario` (`login`,`password`, `nombre`, `apellidos`, `email`, `sexo`, `telefono`) VALUES ('alex', 'alex', 'alex', 'portela', 'alex@user.es', 'HOMBRE', '999999999');
INSERT INTO `usuario` (`login`,`password`, `nombre`, `apellidos`, `email`, `sexo`, `telefono`) VALUES ('alfonso', 'alfonso', 'alfonso', 'portela', 'alfonso@user.es', 'HOMBRE', '999999999');
INSERT INTO `usuario` (`login`,`password`, `nombre`, `apellidos`, `email`, `sexo`, `telefono`) VALUES ('alfredo', 'alfredo', 'alfredo', 'portela', 'alfredo@user.es', 'HOMBRE', '999999999');
INSERT INTO `usuario` (`login`,`password`, `nombre`, `apellidos`, `email`, `sexo`, `telefono`) VALUES ('unai', 'unai', 'unai', 'portela', 'unai@user.es', 'HOMBRE', '999999999');
INSERT INTO `usuario` (`login`,`password`, `nombre`, `apellidos`, `email`, `sexo`, `telefono`) VALUES ('andres', 'andres', 'andres', 'portela', 'andres@user.es', 'HOMBRE', '999999999');
INSERT INTO `usuario` (`login`,`password`, `nombre`, `apellidos`, `email`, `sexo`, `telefono`) VALUES ('marcos', 'marcos', 'marcos', 'portela', 'marcos@user.es', 'HOMBRE', '999999999');
INSERT INTO `usuario` (`login`,`password`, `nombre`, `apellidos`, `email`, `sexo`, `telefono`) VALUES ('pep', 'pep', 'pep', 'portela', 'pep@user.es', 'HOMBRE', '999999999');
INSERT INTO `usuario` (`login`,`password`, `nombre`, `apellidos`, `email`, `sexo`, `telefono`) VALUES ('carlos', 'carlos', 'carlos', 'portela', 'carlos@user.es', 'HOMBRE', '999999999');
INSERT INTO `usuario` (`login`,`password`, `nombre`, `apellidos`, `email`, `sexo`, `telefono`) VALUES ('adrian', 'adrian', 'adrian', 'portela', 'adrian@user.es', 'HOMBRE', '999999999');
INSERT INTO `usuario` (`login`,`password`, `nombre`, `apellidos`, `email`, `sexo`, `telefono`) VALUES ('david', 'david', 'david', 'portela', 'david@user.es', 'HOMBRE', '999999999');
INSERT INTO `usuario` (`login`,`password`, `nombre`, `apellidos`, `email`, `sexo`, `telefono`) VALUES ('manu', 'manu', 'manu', 'portela', 'manu@user.es', 'HOMBRE', '999999999');
INSERT INTO `usuario` (`login`,`password`, `nombre`, `apellidos`, `email`, `sexo`, `telefono`) VALUES ('edgar', 'edgar', 'edgar', 'portela', 'edgar@user.es', 'HOMBRE', '999999999');
INSERT INTO `usuario` (`login`,`password`, `nombre`, `apellidos`, `email`, `sexo`, `telefono`) VALUES ('pedro', 'pedro', 'pedro', 'portela', 'pedro@user.es', 'HOMBRE', '999999999');
INSERT INTO `usuario` (`login`,`password`, `nombre`, `apellidos`, `email`, `sexo`, `telefono`) VALUES ('alejandro', 'alejandro', 'alejandro', 'portela', 'alejandro@user.es', 'HOMBRE', '999999999');
INSERT INTO `usuario` (`login`,`password`, `nombre`, `apellidos`, `email`, `sexo`, `telefono`) VALUES ('gero', 'gero', 'gero', 'portela', 'gero@user.es', 'HOMBRE', '999999999');
INSERT INTO `usuario` (`login`,`password`, `nombre`, `apellidos`, `email`, `sexo`, `telefono`) VALUES ('angel', 'angel', 'angel', 'portela', 'angel@user.es', 'HOMBRE', '999999999');
INSERT INTO `usuario` (`login`,`password`, `nombre`, `apellidos`, `email`, `sexo`, `telefono`) VALUES ('iago', 'iago', 'iago', 'portela', 'iago@user.es', 'HOMBRE', '999999999');
INSERT INTO `usuario` (`login`,`password`, `nombre`, `apellidos`, `email`, `sexo`, `telefono`) VALUES ('jorge', 'jorge', 'jorge', 'portela', 'jorge@user.es', 'HOMBRE', '999999999');
INSERT INTO `usuario` (`login`,`password`, `nombre`, `apellidos`, `email`, `sexo`, `telefono`) VALUES ('xose', 'xose', 'xose', 'portela', 'xose@user.es', 'HOMBRE', '999999999');

INSERT INTO `usuario` (`login`,`password`, `nombre`, `apellidos`, `email`, `sexo`, `telefono`) VALUES ('rosa', 'rosa', 'rosa', 'portela', 'rosa@user.es', 'MUJER', '999999999');
INSERT INTO `usuario` (`login`,`password`, `nombre`, `apellidos`, `email`, `sexo`, `telefono`) VALUES ('ana', 'ana', 'ana', 'portela', 'ana@user.es', 'MUJER', '999999999');
INSERT INTO `usuario` (`login`,`password`, `nombre`, `apellidos`, `email`, `sexo`, `telefono`) VALUES ('lara', 'lara', 'lara', 'portela', 'lara@user.es', 'MUJER', '999999999');
INSERT INTO `usuario` (`login`,`password`, `nombre`, `apellidos`, `email`, `sexo`, `telefono`) VALUES ('leti', 'leti', 'leti', 'portela', 'leti@user.es', 'MUJER', '999999999');
INSERT INTO `usuario` (`login`,`password`, `nombre`, `apellidos`, `email`, `sexo`, `telefono`) VALUES ('nati', 'nati', 'nati', 'portela', 'nati@user.es', 'MUJER', '999999999');
INSERT INTO `usuario` (`login`,`password`, `nombre`, `apellidos`, `email`, `sexo`, `telefono`) VALUES ('nerea', 'nerea', 'nerea', 'portela', 'nerea@user.es', 'MUJER', '999999999');
INSERT INTO `usuario` (`login`,`password`, `nombre`, `apellidos`, `email`, `sexo`, `telefono`) VALUES ('andrea', 'andrea', 'andrea', 'portela', 'andrea@user.es', 'MUJER', '999999999');
INSERT INTO `usuario` (`login`,`password`, `nombre`, `apellidos`, `email`, `sexo`, `telefono`) VALUES ('manuela', 'manuela', 'manuela', 'portela', 'manuela@user.es', 'MUJER', '999999999');

-- CAMPEONATOS
INSERT INTO `campeonato` (`idCampeonato`,`numParticipantes`,`fechaLimiteInscrip`, `gruposGenerados`, `playoffsGenerados`) VALUES (NULL, 8, '2021-08-26', 'N', 'N');
INSERT INTO `campeonato` (`idCampeonato`,`numParticipantes`,`fechaLimiteInscrip`, `gruposGenerados`, `playoffsGenerados`) VALUES (NULL, 32, '2020-08-26', 'N', 'N');
INSERT INTO `campeonato` (`idCampeonato`,`numParticipantes`,`fechaLimiteInscrip`, `gruposGenerados`, `playoffsGenerados`) VALUES (NULL, 36, '2020-07-26', 'S', 'N');

-- GRUPOS CAMPEONATO 2
INSERT INTO `grupo` (`idGrupo`, `categoria`, `nivel`, `idCampeonato`, `numParejas`) VALUES (NULL, 'masculina', 'amateur', 2, 0);
INSERT INTO `grupo` (`idGrupo`, `categoria`, `nivel`, `idCampeonato`, `numParejas`) VALUES (NULL, 'femenina', 'intermedio', 2, 0);

-- GRUPOS CAMPEONATO 1
INSERT INTO `grupo` (`idGrupo`, `categoria`, `nivel`, `idCampeonato`, `numParejas`) VALUES (NULL, 'mixta','intermedio', 1, 0);
INSERT INTO `grupo` (`idGrupo`, `categoria`, `nivel`, `idCampeonato`, `numParejas`) VALUES (NULL, 'mixta','amateur', 1, 0);
INSERT INTO `grupo` (`idGrupo`, `categoria`, `nivel`, `idCampeonato`, `numParejas`) VALUES (NULL, 'masculina','amateur', 1, 0);

-- GRUPOS CAMPEONATO 3
INSERT INTO `grupo` (`idGrupo`, `categoria`, `nivel`, `idCampeonato`, `numParejas`) VALUES (NULL, 'mixta','intermedio', 3, 8);
INSERT INTO `grupo` (`idGrupo`, `categoria`, `nivel`, `idCampeonato`, `numParejas`) VALUES (NULL, 'masculina','amateur', 3, 9);
INSERT INTO `grupo` (`idGrupo`, `categoria`, `nivel`, `idCampeonato`, `numParejas`) VALUES (NULL, 'masculina','profesional', 3, 1);

-- PAREJAS CAMPEONATO 1 (sin grupo asignado)
INSERT INTO `pareja` (`idCampeonato`,`idGrupo`, `idPlayoff`, `login1`,`login2`,`categoria`,`nivel`, `puntosLiga`, `posPlayoff`) VALUES (1, NULL, NULL, 'rosa','sergio','mixta','intermedio', 0, NULL);
INSERT INTO `pareja` (`idCampeonato`,`idGrupo`, `idPlayoff`, `login1`,`login2`,`categoria`,`nivel`, `puntosLiga`, `posPlayoff`) VALUES (1, NULL, NULL, 'juan','ana','mixta','amateur', 0, NULL);
INSERT INTO `pareja` (`idCampeonato`,`idGrupo`, `idPlayoff`, `login1`,`login2`,`categoria`,`nivel`, `puntosLiga`, `posPlayoff`) VALUES (1, NULL, NULL, 'marcos','pepe','masculina','amateur', 0, NULL);
INSERT INTO `pareja` (`idCampeonato`,`idGrupo`, `idPlayoff`, `login1`,`login2`,`categoria`,`nivel`, `puntosLiga`, `posPlayoff`) VALUES (1, NULL, NULL, 'andres','rober','masculina','amateur', 0, NULL);

-- PAREJAS CAMPEONATO 2 (sin grupo asignado) masculina - amateur
INSERT INTO `pareja` (`idCampeonato`,`idGrupo`, `idPlayoff`, `login1`,`login2`,`categoria`,`nivel`, `puntosLiga`, `posPlayoff`) VALUES (2, NULL, NULL, 'marcos','pepe','masculina','amateur', 0, NULL);
INSERT INTO `pareja` (`idCampeonato`,`idGrupo`, `idPlayoff`, `login1`,`login2`,`categoria`,`nivel`, `puntosLiga`, `posPlayoff`) VALUES (2, NULL, NULL, 'mario','victor','masculina','amateur', 0, NULL);
INSERT INTO `pareja` (`idCampeonato`,`idGrupo`, `idPlayoff`, `login1`,`login2`,`categoria`,`nivel`, `puntosLiga`, `posPlayoff`) VALUES (2, NULL, NULL, 'sergio','pedro','masculina','amateur', 0, NULL);
INSERT INTO `pareja` (`idCampeonato`,`idGrupo`, `idPlayoff`, `login1`,`login2`,`categoria`,`nivel`, `puntosLiga`, `posPlayoff`) VALUES (2, NULL, NULL, 'andres','rober','masculina','amateur', 0, NULL);
INSERT INTO `pareja` (`idCampeonato`,`idGrupo`, `idPlayoff`, `login1`,`login2`,`categoria`,`nivel`, `puntosLiga`, `posPlayoff`) VALUES (2, NULL, NULL, 'jose','juan','masculina','amateur', 0, NULL);
INSERT INTO `pareja` (`idCampeonato`,`idGrupo`, `idPlayoff`, `login1`,`login2`,`categoria`,`nivel`, `puntosLiga`, `posPlayoff`) VALUES (2, NULL, NULL, 'juanfran','antonio','masculina','amateur', 0, NULL);
INSERT INTO `pareja` (`idCampeonato`,`idGrupo`, `idPlayoff`, `login1`,`login2`,`categoria`,`nivel`, `puntosLiga`, `posPlayoff`) VALUES (2, NULL, NULL, 'alfonso','alex','masculina','amateur', 0, NULL);
INSERT INTO `pareja` (`idCampeonato`,`idGrupo`, `idPlayoff`, `login1`,`login2`,`categoria`,`nivel`, `puntosLiga`, `posPlayoff`) VALUES (2, NULL, NULL, 'alfredo','unai','masculina','amateur', 0, NULL);
INSERT INTO `pareja` (`idCampeonato`,`idGrupo`, `idPlayoff`, `login1`,`login2`,`categoria`,`nivel`, `puntosLiga`, `posPlayoff`) VALUES (2, NULL, NULL, 'pep','carlos','masculina','amateur', 0, NULL);
INSERT INTO `pareja` (`idCampeonato`,`idGrupo`, `idPlayoff`, `login1`,`login2`,`categoria`,`nivel`, `puntosLiga`, `posPlayoff`) VALUES (2, NULL, NULL, 'adrian','david','masculina','amateur', 0, NULL);
INSERT INTO `pareja` (`idCampeonato`,`idGrupo`, `idPlayoff`, `login1`,`login2`,`categoria`,`nivel`, `puntosLiga`, `posPlayoff`) VALUES (2, NULL, NULL, 'manu','edgar','masculina','amateur', 0, NULL);
INSERT INTO `pareja` (`idCampeonato`,`idGrupo`, `idPlayoff`, `login1`,`login2`,`categoria`,`nivel`, `puntosLiga`, `posPlayoff`) VALUES (2, NULL, NULL, 'alejandro','gero','masculina','amateur', 0, NULL);
INSERT INTO `pareja` (`idCampeonato`,`idGrupo`, `idPlayoff`, `login1`,`login2`,`categoria`,`nivel`, `puntosLiga`, `posPlayoff`) VALUES (2, NULL, NULL, 'angel','iago','masculina','amateur', 0, NULL);

-- PAREJAS CAMPEONATO 2 (sin grupo asignado) femenina - intermedio
INSERT INTO `pareja` (`idCampeonato`,`idGrupo`, `idPlayoff`, `login1`,`login2`,`categoria`,`nivel`, `puntosLiga`, `posPlayoff`) VALUES (2, NULL, NULL, 'nerea','nati','femenina','intermedio', 0, NULL);
INSERT INTO `pareja` (`idCampeonato`,`idGrupo`, `idPlayoff`, `login1`,`login2`,`categoria`,`nivel`, `puntosLiga`, `posPlayoff`) VALUES (2, NULL, NULL, 'leti','lara','femenina','intermedio', 0, NULL);
INSERT INTO `pareja` (`idCampeonato`,`idGrupo`, `idPlayoff`, `login1`,`login2`,`categoria`,`nivel`, `puntosLiga`, `posPlayoff`) VALUES (2, NULL, NULL, 'rosa','ana','femenina','intermedio', 0, NULL);

-- PAREJAS CAMPEONATO 3 (con grupo asignado - 6, 7 y 8)
-- mixta - intermedio (8 parejas)
INSERT INTO `pareja` (`idCampeonato`,`idGrupo`, `idPlayoff`, `login1`,`login2`,`categoria`,`nivel`, `puntosLiga`, `posPlayoff`) VALUES (3, 6, NULL, 'nerea','angel','mixta','intermedio', 23, NULL);
INSERT INTO `pareja` (`idCampeonato`,`idGrupo`, `idPlayoff`, `login1`,`login2`,`categoria`,`nivel`, `puntosLiga`, `posPlayoff`) VALUES (3, 6, NULL, 'nati','iago','mixta','intermedio', 17, NULL);
INSERT INTO `pareja` (`idCampeonato`,`idGrupo`, `idPlayoff`, `login1`,`login2`,`categoria`,`nivel`, `puntosLiga`, `posPlayoff`) VALUES (3, 6, NULL, 'leti','alejandro','mixta','intermedio', 30, NULL);
INSERT INTO `pareja` (`idCampeonato`,`idGrupo`, `idPlayoff`, `login1`,`login2`,`categoria`,`nivel`, `puntosLiga`, `posPlayoff`) VALUES (3, 6, NULL, 'lara','gero','mixta','intermedio', 8, NULL);
INSERT INTO `pareja` (`idCampeonato`,`idGrupo`, `idPlayoff`, `login1`,`login2`,`categoria`,`nivel`, `puntosLiga`, `posPlayoff`) VALUES (3, 6, NULL, 'rosa','manu','mixta','intermedio', 4, NULL);
INSERT INTO `pareja` (`idCampeonato`,`idGrupo`, `idPlayoff`, `login1`,`login2`,`categoria`,`nivel`, `puntosLiga`, `posPlayoff`) VALUES (3, 6, NULL, 'ana','edgar','mixta','intermedio', 15, NULL);
INSERT INTO `pareja` (`idCampeonato`,`idGrupo`, `idPlayoff`, `login1`,`login2`,`categoria`,`nivel`, `puntosLiga`, `posPlayoff`) VALUES (3, 6, NULL, 'andrea','david','mixta','intermedio', 22, NULL);
INSERT INTO `pareja` (`idCampeonato`,`idGrupo`, `idPlayoff`, `login1`,`login2`,`categoria`,`nivel`, `puntosLiga`, `posPlayoff`) VALUES (3, 6, NULL, 'manuela','adrian','mixta','intermedio', 20, NULL);

-- masculina - amateur (9 parejas)
INSERT INTO `pareja` (`idCampeonato`,`idGrupo`, `idPlayoff`, `login1`,`login2`,`categoria`,`nivel`, `puntosLiga`, `posPlayoff`) VALUES (3, 7, NULL, 'marcos','pepe','masculina','amateur', 23, NULL);
INSERT INTO `pareja` (`idCampeonato`,`idGrupo`, `idPlayoff`, `login1`,`login2`,`categoria`,`nivel`, `puntosLiga`, `posPlayoff`) VALUES (3, 7, NULL, 'mario','victor','masculina','amateur', 12, NULL);
INSERT INTO `pareja` (`idCampeonato`,`idGrupo`, `idPlayoff`, `login1`,`login2`,`categoria`,`nivel`, `puntosLiga`, `posPlayoff`) VALUES (3, 7, NULL, 'sergio','pedro','masculina','amateur', 4, NULL);
INSERT INTO `pareja` (`idCampeonato`,`idGrupo`, `idPlayoff`, `login1`,`login2`,`categoria`,`nivel`, `puntosLiga`, `posPlayoff`) VALUES (3, 7, NULL, 'andres','rober','masculina','amateur', 17, NULL);
INSERT INTO `pareja` (`idCampeonato`,`idGrupo`, `idPlayoff`, `login1`,`login2`,`categoria`,`nivel`, `puntosLiga`, `posPlayoff`) VALUES (3, 7, NULL, 'jose','juan','masculina','amateur', 8, NULL);
INSERT INTO `pareja` (`idCampeonato`,`idGrupo`, `idPlayoff`, `login1`,`login2`,`categoria`,`nivel`, `puntosLiga`, `posPlayoff`) VALUES (3, 7, NULL, 'juanfran','antonio','masculina','amateur', 40, NULL);
INSERT INTO `pareja` (`idCampeonato`,`idGrupo`, `idPlayoff`, `login1`,`login2`,`categoria`,`nivel`, `puntosLiga`, `posPlayoff`) VALUES (3, 7, NULL, 'alfonso','alex','masculina','amateur', 36, NULL);
INSERT INTO `pareja` (`idCampeonato`,`idGrupo`, `idPlayoff`, `login1`,`login2`,`categoria`,`nivel`, `puntosLiga`, `posPlayoff`) VALUES (3, 7, NULL, 'alfredo','unai','masculina','amateur', 38, NULL);
INSERT INTO `pareja` (`idCampeonato`,`idGrupo`, `idPlayoff`, `login1`,`login2`,`categoria`,`nivel`, `puntosLiga`, `posPlayoff`) VALUES (3, 7, NULL, 'pep','carlos','masculina','amateur', 20, NULL);

-- masculina - profesional (1 pareja)
INSERT INTO `pareja` (`idCampeonato`,`idGrupo`, `idPlayoff`, `login1`,`login2`,`categoria`,`nivel`, `puntosLiga`, `posPlayoff`) VALUES (3, 8, NULL,'jorge','xose','masculina','profesional', 0, 0);

-- JORNADAS INTENSIVAS
INSERT INTO `jornadaIntensiva` (`idJornada`, `nivel`, `loginUser`, `loginEntrenador`, `fechaJornada`) VALUES (NULL, 'amateur', 'mario', 'rober', '2020-05-30');
INSERT INTO `jornadaIntensiva` (`idJornada`, `nivel`, `loginUser`, `loginEntrenador`, `fechaJornada`) VALUES (NULL, 'intermedio','lara', 'gero', '2020-08-15');

INSERT INTO `reservasRealizadas` (`idReserva`,`login`,`idPista`,`fecha`, `hora`) VALUES (1,'mario',1,'2019-11-30','18:00');
INSERT INTO `reservasRealizadas` (`idReserva`,`login`,`idPista`,`fecha`, `hora`) VALUES (2,'sergio',2,'2019-11-28','20:00');
INSERT INTO `reservasRealizadas` (`idReserva`,`login`,`idPista`,`fecha`, `hora`) VALUES (3,'user',1,'2019-12-05','10:00');

INSERT INTO `partido`(`idPartido`, `fecha`, `numPart`, `hora`, `login1`, `login2`, `login3`, `login4`, `idPista`) VALUES ('2','2020-2-5','2','19:00','mario','victor',null,null,'3');
INSERT INTO `partido`(`idPartido`, `fecha`, `numPart`, `hora`, `login1`, `login2`, `login3`, `login4`, `idPista`) VALUES ('3','2020-1-26','0','20:00',null,null,null,null,'2');
INSERT INTO `partido`(`idPartido`, `fecha`, `numPart`, `hora`, `login1`, `login2`, `login3`, `login4`, `idPista`) VALUES ('1','2020-1-30','4','12:00','mario','ana','sergio','victor','4');

INSERT INTO `horario` (`idHorario`,`horaInicio`,`horaFin`,`fecha`) VALUES (1,'09:00','10:29',NULL);
INSERT INTO `horario` (`idHorario`,`horaInicio`,`horaFin`,`fecha`) VALUES (2,'10:30','11:59',NULL);
INSERT INTO `horario` (`idHorario`,`horaInicio`,`horaFin`,`fecha`) VALUES (3,'12:00','13:29',NULL);
INSERT INTO `horario` (`idHorario`,`horaInicio`,`horaFin`,`fecha`) VALUES (4,'13:30','14:59',NULL);
INSERT INTO `horario` (`idHorario`,`horaInicio`,`horaFin`,`fecha`) VALUES (5,'15:00','16:29',NULL);
INSERT INTO `horario` (`idHorario`,`horaInicio`,`horaFin`,`fecha`) VALUES (6,'16:30','17:59',NULL);
INSERT INTO `horario` (`idHorario`,`horaInicio`,`horaFin`,`fecha`) VALUES (7,'18:00','19:29',NULL);
INSERT INTO `horario` (`idHorario`,`horaInicio`,`horaFin`,`fecha`) VALUES (8,'19:30','20:59',NULL);
INSERT INTO `horario` (`idHorario`,`horaInicio`,`horaFin`,`fecha`) VALUES (9,'21:00','22:29',NULL);

INSERT INTO `horarioClases` (`idHorarioClases`,`horaInicio`,`horaFin`,`fecha`) VALUES (1,'09:00','10:29',NULL);
INSERT INTO `horarioClases` (`idHorarioClases`,`horaInicio`,`horaFin`,`fecha`) VALUES (2,'10:30','11:59',NULL);
INSERT INTO `horarioClases` (`idHorarioClases`,`horaInicio`,`horaFin`,`fecha`) VALUES (3,'12:00','13:29',NULL);
INSERT INTO `horarioClases` (`idHorarioClases`,`horaInicio`,`horaFin`,`fecha`) VALUES (4,'13:30','14:59',NULL);
INSERT INTO `horarioClases` (`idHorarioClases`,`horaInicio`,`horaFin`,`fecha`) VALUES (5,'15:00','16:29',NULL);
INSERT INTO `horarioClases` (`idHorarioClases`,`horaInicio`,`horaFin`,`fecha`) VALUES (6,'16:30','17:59',NULL);
INSERT INTO `horarioClases` (`idHorarioClases`,`horaInicio`,`horaFin`,`fecha`) VALUES (7,'18:00','19:29',NULL);
INSERT INTO `horarioClases` (`idHorarioClases`,`horaInicio`,`horaFin`,`fecha`) VALUES (8,'19:30','20:59',NULL);
INSERT INTO `horarioClases` (`idHorarioClases`,`horaInicio`,`horaFin`,`fecha`) VALUES (9,'21:00','22:29',NULL);

INSERT INTO `entrenador` (`login`) VALUES ('rober');
INSERT INTO `entrenador` (`login`) VALUES ('lara');
INSERT INTO `entrenador` (`login`) VALUES ('alejandro');
INSERT INTO `entrenador` (`login`) VALUES ('angel');
INSERT INTO `entrenador` (`login`) VALUES ('nerea');
INSERT INTO `entrenador` (`login`) VALUES ('leti');
INSERT INTO `entrenador` (`login`) VALUES ('gero');
INSERT INTO `entrenador` (`login`) VALUES ('pedro');
INSERT INTO `entrenador` (`login`) VALUES ('edgar');
INSERT INTO `entrenador` (`login`) VALUES ('manu');

INSERT INTO `deportista` (`login`) VALUES ('mario');
INSERT INTO `deportista` (`login`) VALUES ('sergio');
INSERT INTO `deportista` (`login`) VALUES ('pepe');
INSERT INTO `deportista` (`login`) VALUES ('juan');
INSERT INTO `deportista` (`login`) VALUES ('jose');
INSERT INTO `deportista` (`login`) VALUES ('victor');
INSERT INTO `deportista` (`login`) VALUES ('antonio');
INSERT INTO `deportista` (`login`) VALUES ('juanfran');
INSERT INTO `deportista` (`login`) VALUES ('carlos');
INSERT INTO `deportista` (`login`) VALUES ('adrian');

INSERT INTO `escueladeportiva`(`idEscuela`, `nombre`, `telefono`, `fecha`, `hora`, `idPista`, `numPart`, `nivel`, `loginEnt`, `login1`, `login2`, `login3`, `login4`, `login5`, `login6`, `login7`, `login8`) VALUES ('1','Escuela1','637929965','2020-1-25','20:00', '3', '4', 'amateur','lara','ana','pedro','unai','juan',null,null,null,null);
INSERT INTO `escueladeportiva`(`idEscuela`, `nombre`, `telefono`, `fecha`, `hora`, `idPista`, `numPart`, `nivel`, `loginEnt`, `login1`, `login2`, `login3`, `login4`, `login5`, `login6`, `login7`, `login8`) VALUES ('2','Escuela2','637929964','2020-1-28','18:00', '1', '8', 'profesional','rober','ana','pedro','unai','juan','sergio','pepe','jose','alex');
INSERT INTO `escueladeportiva`(`idEscuela`, `nombre`, `telefono`, `fecha`, `hora`, `idPista`, `numPart`, `nivel`, `loginEnt`, `login1`, `login2`, `login3`, `login4`, `login5`, `login6`, `login7`, `login8`) VALUES ('3','Escuela3','637925965','2020-2-5','18:00', '4', '4', 'amateur','lara','nerea','pedro','unai','juan',null,null,null,null);

INSERT INTO `pista`(`idPista`, `idHorario`, `descripcion`) VALUES ('1','1','Pista cubierta de cesped artificial');
INSERT INTO `pista`(`idPista`, `idHorario`, `descripcion`) VALUES ('2','1','Pista al aire libre de resina sintetica');
INSERT INTO `pista`(`idPista`, `idHorario`, `descripcion`) VALUES ('3','1','Pista al aire libre de cesped artificial');
INSERT INTO `pista`(`idPista`, `idHorario`, `descripcion`) VALUES ('4','1','Pista al aire libre de hormigon poroso');
INSERT INTO `pista`(`idPista`, `idHorario`, `descripcion`) VALUES ('5','1','Pista cubierta de resina sintetica');
INSERT INTO `pista`(`idPista`, `idHorario`, `descripcion`) VALUES ('6','1','Pista cubierta de hormigon poroso');
INSERT INTO `pista`(`idPista`, `idHorario`, `descripcion`) VALUES ('7','1','Pista al aire libre de cemento');
INSERT INTO `pista`(`idPista`, `idHorario`, `descripcion`) VALUES ('8','1','Pista cubierta de cemento');
INSERT INTO `pista`(`idPista`, `idHorario`, `descripcion`) VALUES ('9','1','Pista al aire libre de cesped artificial');
INSERT INTO `pista`(`idPista`, `idHorario`, `descripcion`) VALUES ('10','1','Pista al aire libre de cesped artificial');

INSERT INTO `horarioPista`(`idHorario`, `hora1`, `hora2`, `hora3`, `hora4`, `hora5`, `hora6`, `hora7`, `hora8`, `hora9`) VALUES ('1','9:00','10:30','12:00','13:30','15:00','16:30','18:00','19:30','21:00');

INSERT INTO `clase`(`idClase`, `loginParticipante`, `entrenador`, `fecha`, `hora`, `idPista`) VALUES ('1','sergio','lara','2020-1-31','9:00','7');