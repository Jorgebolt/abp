<?php
class PAREJA_Model{

    var $idCampeonato;
    var $idGrupo;
    var $idPlayoff;
    var $login1;
    var $login2;
    var $categoria;
    var $nivel;
    var $puntosLiga;
    var $posPlayoff;
    var $mysqli;

     function __construct($idCampeonato, $idGrupo, $idPlayoff, $login1, $login2, $categoria, $nivel, $puntosLiga, $posPlayoff){
        $this->idCampeonato = $idCampeonato;
        $this->idGrupo = $idGrupo;
        $this->idPlayoff = $idPlayoff;
        $this->login1 = $login1;
        $this->login2 = $login2;
        $this->categoria= $categoria;
        $this->nivel= $nivel;
        $this->puntosLiga = $puntosLiga;
        $this->posPlayoff = $posPlayoff;

        include_once '../Models/Access_DB.php';
        $this->mysqli = ConnectDB();
    }

    function ADD(){

        //Primero comprueba si los miembros ya estan inscritos
       $sql = "SELECT * 
            FROM `pareja`
            WHERE`idCampeonato` = '" . $this->idCampeonato . "' 
            AND ( `login1` = '" . $this->login1 . "' 
                OR `login1` = '" . $this->login2 . "' 
                OR `login2` = '" . $this->login1 . "' 
                Or `login2` = '" . $this->login2 . "')";

        $resultado = $this->mysqli->query($sql);
        $resultado = true;

        if(!$resultado){ //si hay error en la ejecucion de la consulta
            return 'error conectando con la bd';
        }else{
            //if($resultado->num_rows == 0){ //si la consulta no devuelve ninguna tupla
                $sql = "INSERT INTO `pareja` (
                    `login1`,
                    `login2`,
                    `idCampeonato`,
                    `idGrupo`,
                    `categoria`,
                    `nivel`,
                    `puntosLiga`
                    )
                    VALUES ( 
                    '" .$this->login1. "',
                    '" .$this->login2. "',
                    '" .$this->idCampeonato. "',
                    NULL,
                    '" .$this->categoria. "',
                    '" .$this->nivel. "',
                    0
                 )";

                if(!$this->mysqli->query($sql)){ //si la insercion da error
                    //return "Error description: ". $this->nivel  .  $this->mysqli->error;
                    return 'Error insertando, pareja ya existente';
                }else{
                    return 'Exito insertando';
                }
            //}else{ //si ya existe una tupla con ese idCampeonato
            //    return 'clave existente';
            //}
        }
    }

    function DELETE(){

        $sql = "DELETE FROM PAREJA 
                WHERE (`idCampeonato` = '".$this->idCampeonato."' AND `login1`= '" .$this->login1. "')";

        if (!$this->mysqli->query($sql)) { //Si hay un error en la eliminacion
            return 'exito eliminando';
        } else {
            return 'error insertando';
        }
    }

    function sumarVictoria(){

        //primero recupera la puntuacion actual de la pareja
        $sql = "SELECT `puntosLiga`
            FROM `pareja`
            WHERE `login1`= '" . $this->login1 . "'
                AND`login2`= '" . $this->login2 . "'
                AND`idCampeonato`= '" . $this->idCampeonato . "'
        ";

        $resultado = $this->mysqli->query($sql);

        if($resultado){
            $puntArray = $resultado->fetch_array();
            $puntuacionOLD = $puntArray[0];
            //echo 'PUNTUACION ANTIGUA = ' . $puntuacionOLD;

            //despues la actualiza
            $puntuacionNEW = $puntuacionOLD+3;
            //echo ' nueva puntuacion = ' . $puntuacionNEW . ' // ';

            $sql = "UPDATE `pareja`
                SET `puntosLiga`= " . $puntuacionNEW . "
                WHERE `login1`= '" . $this->login1 . "'
                    AND`login2`= '" . $this->login2 . "'
                    AND`idCampeonato`= '" . $this->idCampeonato . "'
            ";

            $actualizacionPunt = $this->mysqli->query($sql);

            if($actualizacionPunt){
                return true;

            }else{
                echo 'error en la actualizacion';
                return false;
            }

        }else {
            echo 'error en la consulta';
        }
    }

    function sumarDerrota(){
        //primero recupera la puntuacion actual de la pareja
        $sql = "SELECT `puntosLiga`
            FROM `pareja`
            WHERE `login1`= '" . $this->login1 . "'
                AND`login2`= '" . $this->login2 . "'
                AND`idCampeonato`= '" . $this->idCampeonato . "'
        ";

        $resultado = $this->mysqli->query($sql);

        if ($resultado) {
            $puntArray = $resultado->fetch_array();
            $puntuacionOLD = $puntArray[0];
            //echo 'PUNTUACION ANTIGUA = ' . $puntuacionOLD;

            //despues la actualiza
            $puntuacionNEW = $puntuacionOLD + 1;
            //echo ' nueva puntuacion = ' . $puntuacionNEW . ' // ';

            $sql = "UPDATE `pareja`
                SET `puntosLiga`= " . $puntuacionNEW . "
                WHERE `login1`= '" . $this->login1 . "'
                    AND`login2`= '" . $this->login2 . "'
                    AND`idCampeonato`= '" . $this->idCampeonato . "'
            ";

            $actualizacionPunt = $this->mysqli->query($sql);

            if ($actualizacionPunt) {
                return true;
            } else {
                echo 'error en la actualizacion';
                return false;
            }
        } else {
            echo 'error en la consulta';
        }
    }

    //funcion que devuelve el id de un grupo que le encaje a la pareja (categoria y nivel)
    function buscaGrupo(){

        $sql = "SELECT `idGrupo`
            FROM `grupo`
            WHERE `idCampeonato` = '" . $this->idCampeonato . "'
                AND `categoria` = '" . $this->categoria . "'
                AND `nivel` = '" . $this->nivel . "'
                AND `numParejas` < 12
        ";

        $busqueda = $this->mysqli->query($sql);

        if($busqueda){ //si la busqueda da resultado
            $busquedaId = $busqueda->fetch_array();
            //devuelve el id del grupo que corresponde
            return $busquedaId[0];

        }else{
            //echo 'NO HAY UN GRUPO CON MENOS DE 12 PAREJAS';
            return null;
        }
    }

    //funcion que, dado un id de grupo, se lo asigna a una pareja
    function asignarGrupo($idGrupoCorres){

        $sql = "UPDATE `pareja`
            SET `idGrupo`= " . $idGrupoCorres . "
            WHERE `idCampeonato` = '" . $this->idCampeonato . "'
                    AND `login1` = '" . $this->login1 . "'
                    AND `login2` = '" . $this->login2 . "'
        ";

        $asignacion = $this->mysqli->query($sql);
        
        if ($asignacion) { //si la asignacion da resultado
            return true;

        } else {
            echo 'error en la busqueda';
            return false;
        }
    }

    //funcion que ajusta el campo idPlayoff, pasado por parametro
    function setIdPlayoff($idPlayoff){
        $sql = "UPDATE `pareja`
        SET `idPlayoff`= " . $idPlayoff . "
        WHERE `login1` = '" . $this->login1 . "'
            AND `login2` = '" . $this->login2 . "'
            AND `idCampeonato` = " . $this->idCampeonato . "
        ";

        $update = $this->mysqli->query($sql);

        if ($update) { //si la asignacion da resultado
            return true;
        } else {
            echo 'error ajustando el idPlayOff';
            return false;
        }
    }

    //funcion que ajusta la posicion de una pareja para el playoff
    function setPosPlayoff($pos){
        $sql = "UPDATE `pareja`
        SET `posPlayoff`= " . $pos . "
        WHERE `login1` = '" . $this->login1 . "'
            AND `login2` = '" . $this->login2 . "'
            AND `idCampeonato` = " . $this->idCampeonato . "
        ";

        $update = $this->mysqli->query($sql);

        if ($update) { //si la asignacion da resultado
            return true;
        } else {
            echo 'error ajustando la posicion de playoff';
            return false;
        }
    }
}
?>