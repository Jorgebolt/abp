<?php
class JORNADA_INTENSIVA_Model{

    var $idJornada;
    var $nivel;
    var $loginUser;
    var $loginEntrenador;
    var $fechaJornada;
    var $mysqli;
    
    function __construct($idJornada, $nivel, $loginUser, $loginEntrenador, $fechaJornada){
        $this->idJornada = $idJornada;
        $this->nivel = $nivel;
        $this->loginUser = $loginUser;
        $this->loginEntrenador= $loginEntrenador;
        $this->fechaJornada = $fechaJornada;

        include_once '../Models/Access_DB.php';
        $this->mysqli = ConnectDB();
    }

    //Función que devuelve toda la tabla
    function AllData()
    {
        //construimos el sql para buscar esa clave en la tabla
        $sql = "SELECT * FROM `jornadaIntensiva`";

        $resultado = $this->mysqli->query($sql);

        if (!($resultado)) { // Si la busqueda no da resultados, se devuelve el mensaje de que no existe
            return 'tupla inexistente';
        } else { // si existe se devuelve la tupla resultado
            $result = $resultado;
            return $result;
        }
    }

    function Add(){
        $sql = "INSERT INTO `jornadaIntensiva` (
            `idJornada`,
            `nivel`,
            `loginUser`,
            `loginEntrenador`,
            `fechaJornada`
            )
            VALUES (
            NULL,
            '" .$this->nivel. "',
            '" .$this->loginUser. "',
            '" . $this->loginEntrenador . "',
            '" . $this->fechaJornada . "'
            )
        ";

        if(!$this->mysqli->query($sql)){ //si la insercion da error
            return false;
        }else{
            return true;
        }
    }

    function Delete(){
        $sql = "DELETE FROM jornadaIntensiva WHERE (`idJornada` = '$this->idJornada')";

        if (!$this->mysqli->query($sql)) { //Si hay un error en la eliminacion
            return false;
        } else {
            return true;
        }
    }

    function RellenaDatos(){
        //Sentencia SQL de búsqueda de la tupla
        $sql = "SELECT *
                FROM `jornadaIntensiva`
                WHERE (`idJornada` = '" . $this->idJornada . "'
            )";

        $resultado = $this->mysqli->query($sql);
        if (!$resultado) { //Si la busqueda no da resultado (la tupla no está en la BD)
            return 'tupla inexistente';
        } else { //Si la búsqueda da resultado
            $result = $resultado->fetch_array();
            return $result; //Devuelve la tupla resultado

        }
    }

    function getUsuarios(){
        $sql = "SELECT `login`
                FROM USUARIO
            ";

        $resultado = $this->mysqli->query($sql);

        if (!$resultado) { //Si la busqueda no da resultado (la tupla no está en la BD)
            return 'tupla inexistente';

        } else { //Si la búsqueda da resultado
            return $resultado; //Devuelve la tupla resultado

        }
    }

    function getEntenadores(){
        $sql = "SELECT `login`
                FROM ENTRENADOR
            ";

        $resultado = $this->mysqli->query($sql);

        if (!$resultado) { //Si la busqueda no da resultado (la tupla no está en la BD)
            return 'tupla inexistente';
        } else { //Si la búsqueda da resultado
            return $resultado; //Devuelve la tupla resultado

        }
    }
}