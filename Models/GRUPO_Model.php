<?php
class GRUPO_Model{

    var $idGrupo;
    var $categoria;
    var $nivel;
    var $idCampeonato;
    var $numParejas;
    var $mysqli;

    function __construct($idGrupo, $categoria, $nivel, $idCampeonato, $numParejas){
        $this->idGrupo = $idGrupo;
        $this->categoria = $categoria;
        $this->nivel = $nivel;
        $this->idCampeonato= $idCampeonato;
        $this->numParejas = $numParejas;

        include_once '../Models/Access_DB.php';
        $this->mysqli = ConnectDB();
    }

    //Función que devuelve toda la tabla
    function AllData()
    {
        // construimos el sql para buscar esa clave en la tabla
        $sql = "SELECT * FROM `grupo`";

        $resultado = $this->mysqli->query($sql);

        if (!($resultado)) { // Si la busqueda no da resultados, se devuelve el mensaje de que no existe
            return 'tupla inexistente';
        } else { // si existe se devuelve la tupla resultado
            $result = $resultado;
            return $result;
        }
    }

    function Add(){

        $sql = "INSERT INTO `grupo` (
                        `idGrupo`,
                        `categoria`,
                        `nivel`,
                        `idCampeonato`,
                        `numParejas`
                    )VALUES (
                        NULL,
                        '" .$this->categoria. "',
                        '" .$this->nivel. "',
                        '" .$this->idCampeonato. "',
                        0
                 )";

        if(!$this->mysqli->query($sql)){ //si la insercion da error
            return 'error insertando';
        }else{
            return 'exito insertando';
        }
    }

    //funcion que devuelve false en caso de que NO exista el grupo buscado
    function existe(){
        $categoria = $this->categoria;
        $nivel = $this->nivel;

        $sql = "SELECT *
            FROM `GRUPO`
            WHERE `categoria` = '" . $categoria . "'
                AND `nivel` = '" . $nivel . "'
        ";

        $resultado = $this->mysqli->query($sql);

        //si la consulta da resultado
        if ($resultado->num_rows != 0) {
            //si existe
            //echo 'EXISTE DICHO GRUPO';
            return true;

        } else {
            //no existe ningun grupo con esta categoria y nivel
            //hay que crearlo
            //echo 'NO EXISTE DICHO GRUPO';
            return false;
        }
    }

    //funcion que devuelve las parejas de un grupo concreto, ordenando por sus puntos
    function getParejas(){
        $idGrupo = $this->idGrupo;
        $idCampeonato = $this->idCampeonato;
        $nivel = $this->nivel;
        $categoria = $this->categoria;

        $sql = "SELECT `login1`,`login2`,`puntosLiga`
            FROM `PAREJA`
            WHERE `idCampeonato` = '" . $idCampeonato . "'
                AND `idGrupo` = '" . $idGrupo . "'
                AND `nivel` = '" . $nivel . "'
                AND `categoria` = '" . $categoria . "'
            ORDER BY `puntosLiga` DESC;
        ";

        $resultado = $this->mysqli->query($sql);

        if(!($resultado)){
            return 'error';
        }else{
            return $resultado;
        }
    }

    //funcion que devuelve las parejas que jugaran el playoff de un grupo
    function getParejasPlayoff(){
        $sql = "SELECT *
            FROM PAREJA
            WHERE `idGrupo` = " . $this->idGrupo . "
            ORDER BY `puntosLiga` DESC
            LIMIT 8
        ";

        $resultado = $this->mysqli->query($sql);

        if (!($resultado)) { // Si la busqueda no da resultados, se devuelve el mensaje de que no existe
            return 'tupla inexistente';

        } else { // si existe se devuelve la tupla resultado
            return $resultado;
        }
    }
    
    //funcion que, dado un usario, devuelve TRUE si pertenece al grupo
    //es decir, si esta en las parejas del grupo
    function estaInscrito($usuario, $parejas){
        $esta = false;
        while ($row = $parejas->fetch_array()) {
            if ($row['login1'] == $usuario or $row['login2'] == $usuario) {
                $esta = true;
            }
        }

        return $esta;
    }

    //funcion que incrementa en uno el numero de parejas de un grupo
    function updateNumParejas(){

        //echo 'se actualizara el numParejas del grupo ' . $this->idGrupo . ' // ';
        
        $sql = "SELECT `numParejas`
            FROM `grupo`
            WHERE `idGrupo`= '" . $this->idGrupo . "'
        ";

        $resultado = $this->mysqli->query($sql);

        if ($resultado) {
            $numParArray = $resultado->fetch_array();
            $numParOLD = $numParArray[0];

            //despues la actualiza
            $numParNEW = $numParOLD + 1;

            $sql = "UPDATE `grupo`
                SET `numParejas`= " . $numParNEW . "
                WHERE `idGrupo`= '" . $this->idGrupo . "'
            ";

            $actualizacion = $this->mysqli->query($sql);

            if ($actualizacion) {
                return true;
            } else {
                echo 'error en la actualizacion';
                return false;
            }
        } else {
            echo 'error en la consulta';
        }
    }

    //funcion que crea un nuevo grupo con la categoria y nivel dados
    function crearNuevo($categoria, $nivel){
        $sql = "INSERT INTO `grupo` (
                        `idGrupo`,
                        `categoria`,
                        `nivel`,
                        `idCampeonato`,
                        `numParejas`
                    )VALUES (
                        NULL,
                        '" . $categoria . "',
                        '" . $nivel . "',
                        '" . $this->idCampeonato . "',
                        0
                 )";

        if(!$this->mysqli->query($sql)){ //si la insercion da error
            echo 'error insertando';
            return null;

        }else{ //si se creo con exito
            //devuelve el id del grupo nuevo
            //al ser id autoincremental
            //es el id mas alto de la bd
            $sql = "SELECT `idGrupo`
                    FROM `grupo`
                    ORDER BY `idGrupo` DESC
            ";

            $recuperaId = $this->mysqli->query($sql);

            if($recuperaId){ //si la busqueda da resultado
                $idGrupoNuevo = $recuperaId->fetch_array();
                echo 'exito recuperando el id = ' . $idGrupoNuevo[0];
                return $idGrupoNuevo[0];
            }
        }
    }

    //funcion que devuelve la categoria de un grupo
    function getCategoria(){
        $sql = "SELECT `categoria`
            FROM GRUPO
            WHERE `idGrupo` = " . $this->idGrupo . "
        ";
        
        $categoria = $this->mysqli->query($sql);

        if($categoria){ //si la busqueda da resultado
            $cat = $categoria->fetch_array();
            return $cat[0];

        }else{
            echo 'error recuperando la categoria';
            return false;
        }
    }

    //funcion que devuelve el nivel de un grupo
    function getNivel(){
        $sql = "SELECT `nivel`
            FROM GRUPO
            WHERE `idGrupo` = " . $this->idGrupo . "
        ";
        
        $nivel = $this->mysqli->query($sql);

        if($nivel){ //si la busqueda da resultado
            $niv = $nivel->fetch_array();
            return $niv[0];
            
        }else{
            echo 'error recuperando el nivel';
            return false;
        }
    }
}