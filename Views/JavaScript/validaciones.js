/*
Documento javascript que contiene el código javascript para las validaciones de los formularios de la página
Pablo Pazos Domínguez, Alias:3w68x9
19/10/2018
*/
var avisado = false; //bandera para que no aparezcan alert al no validarse varios campos
var abc = /^[a-záéíóú]{1}[a-záéíóú ]*$/i;//expresion regular que coincide con una cadena alfabética
var comprobacion = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;//expresión regular que coincide con un email tipo example@example.com
var dni = /^[0-9]{8}[a-z]$/i;//expresion regular que coincide con un deni tipo 17463632P
var cif = /^[0-9][a-z]{9}$/i;//expresion regular que coincide con un deni tipo 17463632P
var telef = /^(\+34|0034|34)?[\s|\-|\.]?[6|7|9][\s|\-|\.]?([0-9][\s|\-|\.]?){8}$/;//expresion regular que coincide con telefonos tipo 0034689664381, +34689664381, 689664381
var texto = /^([^\s]+)+$/;//expresión regular que coincide con texto sin espacios, tabuladores ni saltos de línea
	//función que comprueba si un campo esta vacio.
	function comprobarVacio(campo) {

		var str1 = "2";
		var res = campo.id.concat(str1);

		if (( campo.value == "") || (campo.value.length == 0)) {//comprobamos que el campo no este vacio media la longitud y el texto introducido
			
				document.getElementById(res).innerHTML =  'Ingrese algo en el campo ' + campo.name;
				campo.style.background="#e57162";
				return false;
			
		}
		else {
			document.getElementById(res).innerHTML =  '';
			campo.style.background="#FFF";
		}
		return true;
	}

	//función que comprueba un campo coincida con una expresión regular
	function comprobarExpresionRegular(campo, expr, size){
		if(!comprobarVacio(campo)) return false;
		var str1 = "2";
		var res = campo.id.concat(str1);
		if(expr.test(campo.value) == false || campo.value.length > size){ //comprobamos que coincida con la expresion regular y que el tamaño sea correcto
				
			document.getElementById(res).innerHTML =  'Valor no valido para el campo ' + campo.name ;
			campo.style.background="#e57162";

			return false;
			
		}
		else {
			document.getElementById(res).innerHTML =  '';
			campo.style.background="#FFF";
		}
		return true;
	}

	//funcion que comprueba si se ha seleccionado una pareja
	function comprobarSeleccionPareja(){
		
	}

	//función que comprueba que un numero decimal tengo los decimales indicados y esté dentro de un rango
	function comprobarReal(campo, numDecim, valormenor, valormayor){ç
		var str1 = "2";
		var res = campo.id.concat(str1);
		if(!comprobarVacio(campo)) return false;
		var punto = campo.value.replace(',', '.');//variable que  contiene el numero decimal separado por un punto
		var numero = parseFloat( punto );//variable que tiene en flotante a punto
		var deci = punto.substr(punto.indexOf('.') + 1, punto.length);//deci contiene la parte decimal del número
		if (deci.length > numDecim) {//se comprueban la longitud de la parte decimal
				document.getElementById(res).innerHTML =  'El atributo ' + campo.name + ' tiene que tener ' +numDecim+ ' decimales' ;
				campo.style.background="#e57162";
				return false;
				
		}
		if(numero < valormenor || numero > valormayor){//se comprueba que el número este entre el rango definido

				
				document.getElementById(res).innerHTML = 'El numero debe estar entre ' +valormenor+' y '+valormayor ;
				campo.style.background="#e57162";
				return false;
				
		}
		else {
			document.getElementById(res).innerHTML =  '';
			campo.style.background="#FFF";
		}

		return true;
	}

	//función que comprueba que un campo coincida con los telefonos indicados por la expresion regular
	function comprobarTelf(campo){
		if(!comprobarVacio(campo)) return false;
		if(!comprobarExpresionRegular(campo, telef, 13)) return false;
		return true;
		
	}

	//función que comprueba que un campo coincida con la expresión regular DNI y que la letra este bien introducida
	function comprobarDni(campo){

		if(!comprobarVacio(campo)) return false;
		if(!comprobarExpresionRegular(campo,  dni, 9)) return false;

			var letras = [ "T", "R", "W", "A", "G", "M", "Y", "F", "P", "D", "X", "B", "N", "J", "Z", "S", "Q", "V", "H", "L", "C", "K", "E" ];
			// variable que almacena el resto de la división entera
			var numeros = parseInt( campo.value.substr( 0, 8 ) );
			//variable para almacenar la letra del dni
			var letra = campo.value.substr( 8, 9 );
			//variable que almacena el indice de la letra para comprobar su posión en el array
			var numLetra = numeros % 23;
			//variable que almacena la letra que debería tener el dni
			var letraDni = letras[ numLetra ];
			var str1 = "2";
			var res = campo.id.concat(str1);
			//Compruebo que las letras coincidan
			if (letraDni != letra.toUpperCase( letra )) {

					document.getElementById(res).innerHTML = 'La letra del DNI no es correcta' ;
					campo.style.background="#e57162";
					return false;
				
			}
			else {
				document.getElementById(res).innerHTML =  '';
				campo.style.background="#FFF";
			}
			
			return true;
			
	}
	//función que comprueba que un campo sea un texto sin espacion es blanco, tabuladores ni saltos de líneas
	function comprobarTexto(campo, size) {
		if(!comprobarVacio(campo)) return false;
		if(!comprobarExpresionRegular(campo, texto, 30)) return false;
		var str1 = "2";
		var res = campo.id.concat(str1);
		if (campo.value.length > size) {//comprobamos que la longitud sea la correcta
				
				document.getElementById(res).innerHTML = 'Longitud incorrecta. El atributo ' + campo.name + ' debe tener una logitud máxima ' + size + ' y tiene ' + campo.value.length ;
				campo.style.background="#e57162";
				return false;
			
		}
		else {
			document.getElementById(res).innerHTML =  '';
			campo.style.background="#FFF";
		}
		return true;		
	}	
	//función que compruebe que un campo solo tenga caracteres alfabéticos
	function comprobarAlfabetico(campo, size) {
		if(!comprobarVacio(campo)) return false;
		if(!comprobarExpresionRegular(campo, abc, size)) return false;
		return true;
	}

	//función que comprueba que el numero se encuentra entre el rango indicado
	function comprobarEntero(campo, valormenor, valormayor) {
		if(!comprobarVacio(campo)) return false;
		
		var str1 = "2";
		var res = campo.id.concat(str1);

		var numero = parseInt(campo.value);//variable que contiene el texto como int
		
		if(numero < valormenor || numero > valormayor){//comprobacion de que el número esté en el rango
            	
				document.getElementById(res).innerHTML = 'El numero debe estar entre ' +valormenor+' y '+valormayor;
				campo.style.background="#e57162";
				return false;
			
		}
		else if(isNaN(campo.value)){//comprobamos que la variable campo sea un número
			
				document.getElementById(res).innerHTML = 'Ingrese solo números';
				campo.style.background="#e57162";
				return false;
		}
		else {
			document.getElementById(res).innerHTML =  '';
			campo.style.background="#FFF";
		}
		return true;
	}
	//Ordena la tabla según el campo n
	function sortTable(n) {
	  var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
	  table = document.getElementById("tabla2");
	  switching = true;
	  // Set the sorting direction to ascending:
	  dir = "asc"; 
	  /* Make a loop that will continue until
	  no switching has been done: */
	  while (switching) {
	    // Start by saying: no switching is done:
	    switching = false;
	    rows = table.rows;
	    /* Loop through all table rows (except the
	    first, which contains table headers): */
	    for (i = 1; i < (rows.length - 1); i++) {
	      // Start by saying there should be no switching:
	      shouldSwitch = false;
	      /* Get the two elements you want to compare,
	      one from current row and one from the next: */
	      x = rows[i].getElementsByTagName("TD")[n];
	      y = rows[i + 1].getElementsByTagName("TD")[n];
	      /* Check if the two rows should switch place,
	      based on the direction, asc or desc: */
	      if (dir == "asc") {
	        if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
	          // If so, mark as a switch and break the loop:
	          shouldSwitch = true;
	          break;
	        }
	      } else if (dir == "desc") {
	        if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
	          // If so, mark as a switch and break the loop:
	          shouldSwitch = true;
	          break;
	        }
	      }
	    }
	    if (shouldSwitch) {
	      /* If a switch has been marked, make the switch
	      and mark that a switch has been done: */
	      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
	      switching = true;
	      // Each time a switch is done, increase this count by 1:
	      switchcount ++; 
	    } else {
	      /* If no switching has been done AND the direction is "asc",
	      set the direction to "desc" and run the while loop again. */
	      if (switchcount == 0 && dir == "asc") {
	        dir = "desc";
	        switching = true;
	      }
	    }
	  }
	}
	//convierte la fecha y las compara devolviendo true si fecha fin es mayor que fecha inicio, false en caso contrario
	function convertDateFormat() {
        var string = document.getElementById('fechainicio').value;
        var string2 = document.getElementById('fechafin').value;
        var info = new Date(string.split('/').reverse().join('-'));
        var info2 = new Date(string2.split('/').reverse().join('-'));
        if( info >= info2)
            {   
                document.getElementById('fechainicio2').innerHTML = 'Tiene que ser menor que fecha fin';
				document.getElementById('fechainicio').style.background="#e57162";
				document.getElementById('fechafin2').innerHTML = 'Tiene que ser mayor que fecha fin';
				document.getElementById('fechafin').style.background="#e57162";

                return false;
            }
        else return true;
        
   }
   //función que comprueba las fechas de el formulario de add visita
   function convertDateFormat2() {
        var string = document.getElementById('fechaDesde').value;
        var string2 = document.getElementById('fechaHasta').value;
        var info = new Date(string.split('/').reverse().join('-'));
        var info2 = new Date(string2.split('/').reverse().join('-'));
        if( info >= info2)
            {   
                document.getElementById('fechaDesde2').innerHTML = 'Tiene que ser menor que fecha fin';
				document.getElementById('fechaDesde').style.background="#e57162";
				document.getElementById('fechaHasta2').innerHTML = 'Tiene que ser mayor que fecha fin';
				document.getElementById('fechaHasta').style.background="#e57162";

                return false;
            }
        else return true;
        
   }
   //Funcion que cambia el texto del label para adecuarse al archivo seleccionado
   function getNombreFile(){
			            var number=document.getElementById("file").value;
			            number2=number.substring(12);

			            document.getElementById("juan").textContent = number2;
			        }
	function getNombreFile2(){
			            var number=document.getElementById("file").value;
			            number2=number.substring(12);

			            document.getElementById("informeVis").textContent = number2;
			        }
   //funcion que sirve para cambiar el select de cifCentro en el formulario de edit usuario 
   function myFunction(campo){
		if(document.getElementById('autorizador').value == 'SI'){
			document.getElementById('cifcentro').disabled = true;
			document.getElementById('cifCentro2').innerHTML = 'El autorizador no tiene centro asociado';
		}
		else{
			document.getElementById('cifcentro').disabled = false;
			document.getElementById('cifCentro2').innerHTML = '';
		}
	}
	//función que cambia el campo categoría en el formulario de añadir contrato cuando se cambia la empresa
	function cambiar(){
        var x = document.getElementById("cifempresa").selectedIndex;
        var y = document.getElementById("cifempresa").options;

        z = y[x].title;
        if(z == 'ARREGLOS' || z == 'ARREGLO'){
            document.getElementById("categoria").value = 'ARREGLO';
        }
        else if(z == 'MANTENEDORA' || z == 'MANTENIMIENTO'){
             document.getElementById("categoria").value = 'MANTENIMIENTO';
        }
        else{
            document.getElementById("categoria").value = 'CERTIFICADOR';
        }

    }
	//función que pide la confirmación de borrado del usuario
	function borrado(){
		return confirm('¿Seguro que quiere borrar el usuario?');
	}
	//funcion que modifica el showall de peticiones de registro para asignar un centro
	function añadir(dato){
        document.getElementById("añadir").innerHTML = 'Asigne un centro par alta usuario';
        document.getElementById(dato).style.display = '';
    }
	//función que comprueba que todos los campos son validos del formulario login con las funciones anteriores
	function validarLogin(Formulario){
		return comprobarDni(Formulario.login) && comprobarTexto(Formulario.password, 20);
	}
	//función que comprueba que todos los campos son validos del formulario registrar con las funciones anteriores
	//función que comprueba que todos los campos son validos del formulario añadir con las funciones anteriores
	function validarADD(Formulario){
		if(Formulario.file.value.length == 0){//comprobamos que el tamaño del fichero que sube sea distinto de 0
			document.getElementById('juan2').innerHTML = 'No se puede añadir el archivo vacio';
			document.getElementById('juan').style.background="#e57162";
			return false;
		}

		else{
			document.getElementById('juan2').innerHTML = '';
			document.getElementById('juan').style.background="#FFF";
			if(!comprobarEntero(Formulario.importe, 0, 999999)){//comprobación del importe
				return false;
			}
			else if(!comprobarVacio(Formulario.fechainicio)){//comprobación de la fecha inicio
				return false;
			}
			else if(!comprobarEntero(Formulario.numerovisitas, 0, 99)){//comprobación del numero de visitas
				return false;
			}
			else if(!comprobarVacio(Formulario.fechafin)){//comprobacion de la fecha din
				return false;
			}
			else if(!convertDateFormat()){
				return false;
			}
		}

	}
	//función uqe valida el edit de contratos
	function validarEdit(Formulario){
		if(document.getElementById('juan2').value = ''){
			if(Formulario.file.value.length == 0){//comprobamos que el tamaño del fichero que sube sea distinto de 0
			document.getElementById('juan2').innerHTML = 'No se puede añadir el archivo vacio';
			document.getElementById('juan').style.background="#e57162";
			return false;
			}
		}
		else{
			document.getElementById('juan2').innerHTML = '';
			document.getElementById('juan').style.background="#FFF";
			if(!comprobarEntero(document.getElementById('importe'), 0, 999999)){//comprobación del importe
				return false;
			}
			else if(!convertDateFormat()){
				return false;
			}
			else if(!comprobarEntero(document.getElementById('numerovisitas'), 0, 99)){
				return false;
			}
		}
	}

	//función que valida el formulario edit usuarios
	function validarEdiUs(){
		return comprobarTexto(document.getElementById('login'),15) && comprobarTexto(document.getElementById('password'),20) && comprobarExpresionRegular(document.getElementById('email'), comprobacion, 60) && comprobarTelf(document.getElementById('telefono'));
	}
	//función que valida el formulario registrar usuario
	function validarRegister(Formulario){
		return comprobarTexto(Formulario.login, 15) && comprobarTexto(Formulario.contrasena, 20) && comprobarDni(Formulario.dni) && comprobarAlfabetico(Formulario.nombre, 30) && comprobarAlfabetico(Formulario.apellidos, 50) && comprobarExpresionRegular(Formulario.email, /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i, 60) && comprobarExpresionRegular(Formulario.telefono, /^(\+34|0034|34)?[\s|\-|\.]?[6|7|9][\s|\-|\.]?([0-9][\s|\-|\.]?){8}$/, 13);
	}
	//función que comprueba los campos del formulario Login

	function validarLogin(Formulario){
		return comprobarTexto(Formulario.login, 15) && comprobarTexto(Formulario.password, 20);
	}
	//función que valida el add de visitas
	function validarAddVisita(Formulario){
		return comprobarVacio(document.getElementById("fechaDesde")) && comprobarVacio(document.getElementById("fechaHasta")) && convertDateFormat2();
	}
	//función que valida el edit de visita
	function validarEditVisita(Formulario){
		if(document.getElementById("fechaVis").value == '00/00/0000'){
			document.getElementById('fechaVis2').innerHTML = 'Elige una fecha';
			document.getElementById('fechaVis').style.background="#e57162";
			return false;
		}else{
			document.getElementById('fechaVis2').innerHTML = '';
			document.getElementById('fechaVis').style.background="#FFF";
			if(document.getElementById('informeVis').textContent == ''){

				if(document.getElementById('file').value.length == 0){
					document.getElementById('file2').innerHTML = 'Elige un archivo';
					document.getElementById('informeVis').style.background="#e57162";
					return false;
				}else{
					return true;
				}
			}
			
		}
	}
//funcion que valida el add de centro
	function validarAddCentro(Formulario){
		return comprobarExpresionRegular(document.getElementById('cifCentro'), /^[A-Z0-9-]{1,9}/i, 9) && comprobarVacio(document.getElementById('nombre'), 30) && comprobarVacio(document.getElementById('domicilio'));
	}
	//función que valida el edit de centro
	function validarEditCentro(Formulario){
		return comprobarVacio(document.getElementById('nombre')) && comprobarVacio(document.getElementById('domicilio'));
	}
	//funcion que valida el add de empresa
	function validarAddEmpresa(){
		return comprobarExpresionRegular(document.getElementById('cifEmpresa'), /^[A-Z0-9-]{1,9}/i, 9) && comprobarVacio(document.getElementById('nombre')) && comprobarVacio(document.getElementById('domicilioFiscal'));
	}
	function validarEditEmpresa(){
		return  comprobarVacio(document.getElementById('nombre')) && comprobarVacio(document.getElementById('domicilioFiscal'));
	}
	//función que cambia los campos de el formulario add visita en funcion si la visita es programada  o no
	 function programada(){
            var x = document.getElementById("categoriaVis").value;
            var j = document.getElementById("visitapadre").selectedIndex;
            var y = document.getElementById("visitapadre").options;
            var n = y[0].text;
            if(x == 'PROGRAMADA'){
                 document.getElementById("visitapadre").disabled = true;
                    
                    y[0].value = '';
                    y[0].text = 'No tiene visitas asociadas';
            }else{
                y[0].value = '0';
                y[0].text = 'VP0';
                document.getElementById("visitapadre").disabled = false;
            }
        }
