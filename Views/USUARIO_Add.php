<!--
Vista para el formulario registro de usuarios
-->
<?php

class USUARIO_Add{

  function __construct(){ 
    $this->render();
  }

  function render(){
  
        include '../Views/Header.php'; //Incluye la cabecera
    ?> 

  <div class="formRegister">
  
    <h1 class="titulo"><?php echo "CREAR USUARIO"?></h1>

  <form action='../Controllers/USUARIO_Controller.php' method='post' >

    <div class="form-group row">
      <label class="col-sm-2 col-form-label" for="login"><?php echo $strings['Login'];?>*</label>
        <div class="col-sm-9">
          <input type="text" class="form-control" name="login" placeholder="<?php echo $strings['Login'];?>">
        </div>
    </div>

    <div class="form-group row">
      <label class="col-sm-2 col-form-label" for="password"><?php echo $strings['Contraseña'];?>*</label>
        <div class="col-sm-9">
          <input type="text" class="form-control" name="password" placeholder="<?php echo $strings['Contraseña'];?>">
        </div>
    </div>

    <div class="form-group row">
      <label class="col-sm-2 col-form-label" for="nombre"><?php echo $strings['Nombre'];?>*</label>
        <div class="col-sm-9">
          <input type="text" class="form-control" name="nombre" placeholder="<?php echo $strings['Nombre'];?>">
        </div>
    </div>

    <div class="form-group row">
      <label class="col-sm-2 col-form-label" for="apellidos"><?php echo $strings['Apellidos'];?>*</label>
        <div class="col-sm-9">
          <input type="text" class="form-control" name="apellidos" placeholder="<?php echo $strings['Apellidos'];?>">
        </div>
    </div>

    <div class="form-group row">
      <label class="col-sm-2 col-form-label" for="email"><?php echo $strings['Email'];?>*</label>
        <div class="col-sm-9">
          <input type="text" class="form-control" name="email" placeholder="<?php echo $strings['Email'];?>">
        </div>
    </div>

    <div class="form-group row">
      <label class="col-sm-2 col-form-label" for="sexo"><?php echo $strings['Sexo'];?>*</label>
        <div class="col-sm-9">
          <select name="sexo" class="form-control">
            <option value = 'HOMBRE'><?php echo $strings['Hombre'];?></option>
            <option value = 'MUJER'><?php echo $strings['Mujer'];?></option>
          </select>
        </div>
    </div>

    <div class="form-group row">
      <label class="col-sm-2 col-form-label" for="telefono"><?php echo $strings['Telefono'];?>*</label>
        <div class="col-sm-9">
          <input type="text" class="form-control" name="telefono" placeholder="<?php echo $strings['Telefono'];?>">
        </div>
    </div>

    <div class="form-group row">
      <label class="col-sm-2 col-form-label" for="telefono"><?php echo "Tipo"?>*</label>
        <div class="col-sm-9">
        <input type="checkbox" name="administrador" value="Administrador">Administrador<br>
       <input type="checkbox" name="entrenador" value="Entrenador">Entrenador<br>
         <input type="checkbox" name="deportista" value="Deportista">Deportista<br>
        </div>
    </div>

    <div class="form-group row">
      <label class="col-sm-9 col-form-label"><?php echo $strings['* Indica que los campos son obligatorios'];?></label>
    </div>

      <div class="boton">
        <button type="submit" class="btn btn-outline-primary" name="action" value="Add"><?php echo $strings['Validar'];?></button>
      </div>
  </form>
  </div>
  <?php
   include '../Views/Footer.php';
        ?>
        </html>
        <?php
  } //fin metodo render

} //fin REGISTER

?>
