<!--
    Archivo PHP con la cabecera de la aplicacion
    Fecha de creación: 06/10/2019 
-->

<?php
include_once '../Functions/Authentication.php'; //include con la funcion de comprobar autenticacion

if (!isset($_SESSION['idioma'])) { //Si no hay una variable de idioma la define
    $_SESSION['idioma'] = 'SPANISH';
}

include '../Locales/Strings_' . $_SESSION['idioma'] . '.php'; //Strings de cambio de idioma

?>

<html>
<!-- Abro el HTML -->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>
        <?php echo 'PADEL ABP'; ?>
    </title>

    <script type="text/javascript" src="../Views/calendario/tcal.js"></script>
    <script type="text/javascript" src="../Views/JavaScript/md5.js"></script>

    <link rel="stylesheet" type="text/css" href="../Views/calendario/tcal.css" />
    <script type="text/javascript" src="../Views/calendario/tcal2.js"></script>
    <script type="text/javascript" src="../Views/JavaScript/validaciones.js"></script>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="css/sb-admin-2.min.css" rel="stylesheet">
    <link href="../Views/css/estilo.css" rel="stylesheet">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

</head>

<!-- Meti esto para colocar el menu superior en el header y que aparezca siempre s-->
<div id='main'>

    <body class="bg-gradient-primary">
        <!-- Abro el BODY -->

        <div class="containerHeader">
            <!-- Abro el Contenedor principal -->
        <?php if (IsAuthenticated()) { //si esta autenticado?>
            <div class="headerPadel">
                <h1><?php echo $strings['Padel ABP']; ?></h1><h5><?php echo $strings['(Association of the Best Players)']; ?></h5>
            </div>

            <div class="cajaidiomas">
                <td class="nohay">

                    <form name='idiomaform' action="../Functions/CambioIdioma.php" method="post">
                        <button type="submit" name="idioma" style='background:url("../Views/img/banderaSPAIN.png") center no-repeat; height:30px; width:30px; border-radius: 15px; background-size: 30px 30px;border-color: #072146;' value="SPANISH"><span></span></button>
                        <button type="submit" name="idioma" style='background:url("../Views/img/banderaENGLAND.jpg") center no-repeat; height:30px; width:30px;background-size: 30px 30px; border-radius: 15px; border-color: #072146;' value="ENGLISH"><span></span></button>
                        <button type="submit" name="idioma" style='background:url("../Views/img/banderaGALICIA.jpg") center no-repeat; height:30px; width:30px;background-size: 30px 30px; border-color: #072146;border-radius: 15px;' value="GALLAECIAN"><span></span></button>
                    </form>
                </td><br>
            </div>

            <p>
                <?php 

                    include '../Views/MenuSuperior.php'; //Muestra el menu
                } ?><!--Cierre del if(isAuthenticated())-->

            </p>
            <!-- El <div> container se cierra en el Footer.php -->
            <!-- El <body> se cierra en el Footer.php -->
            <!-- El <html> se cierra en el Footer.php -->