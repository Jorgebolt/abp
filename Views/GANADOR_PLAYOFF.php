<?php

class GANADOR_PLAYOFF
{
    function __construct($ganador)
    {
        $this->render($ganador);
    }

    function render($ganador)
    {        
?>
        <body>
            <div class="container">
<?php
            include '../Views/Header.php'; //Incluye la cabecera
?>
                <br>
                <?php echo '<h3>GANADOR PLAYOFF ' . $_REQUEST['idPlayoff'] . ' (CAMPEONATO ' . $_REQUEST['idCampeonato'] . ' - GRUPO ' . $_REQUEST['idGrupo'] .')</h3>';?>
                <?php echo '<h4>' . $ganador . ', ENHORABUENA!</h4>';?>
            </div>
            <!-- Boton VOLVER -->
        <?php
            echo '<br><a class="btn btn-outline-secondary" href=\'../Controllers/CAMPEONATO_Controller.php?action=Showcurrent&idCampeonato=' . $_REQUEST['idCampeonato'] .  '&gruposGenerados=S' . "'><i class='fas fa-undo-alt'></i></a>";
        ?>

                                
        </body>

        <?php
        include '../Views/Footer.php';
    } //fin metodo render
} //fin clase
?>