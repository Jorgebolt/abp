<?php
class Pista_Add{

    function __construct($datos){ 
        $this->render($datos);
    }

    function render($datos){
 ?>

 <html>
    <?php
        include '../Views/Header.php';//Incluye la cabecera
    ?>
    <body>
       <div class="container">
        <div class="addPartido">
    <form name ='añadir'  action='../Controllers/PISTA_Controller.php?action=Add' method='post'>
        <legend><?php echo $strings['Añadir pista']; ?></legend>

        <div class="form-group row">
            <label class="col-sm-3 col-form-label" for="descripcion"><?php echo $strings['Descripcion'];?></label>
            <div class="col-sm-9">
                <textarea rows="10" cols="50"  name="descripcion" placeholder="Descripcion" ></textarea>
            </div>
        </div>

        <a class="btn btn-outline-secondary" href="../Controllers/PISTA_Controller.php?action=Showall"> <i class="fas fa-undo"> </i></a>
        <button type="submit" class="btn btn-outline-primary"><span class="fas fa-check"></span></button>             
    </form>
    </div>
</div>

<?php
     include '../Views/Footer.php';//Incluye el pie de p�gina
?> 
    </body>
    
</html>
<?php
        } //fin metodo render
    } //fin REGISTER
?>