<?php

  class ESCUELA_Edit{


    function __construct($datos){ 
      $this->render($datos);
    }

    function render($datos){
?>
        <html>
<?php
            include '../Views/Header.php'; //Incluye la cabecera
?>
    
          <div class="formAddPlayer">
              <legend class="titulo"><?php echo $strings['Edita los datos de la escuela']; ?></legend>

              <?php
                        function  cambiar ( $fecha ){ //Función para cambiar el formato de la fecha
                         $mifecha = explode("-", $fecha);
                        $lafecha=$mifecha[2]."/".$mifecha[1]."/".$mifecha[0]; 
                        return $lafecha; 
                    }?>

              <form name ='edit' id="edit" action='../Controllers/ESCUELA_Controller.php?action=Edit' method='post'>

                      <div>
                          <input type="text" class="form-control-plaintext" name="idEscuela" readonly hidden
                            value="<?php echo $datos['idEscuela']; ?>">
                      </div>

                  <div class="form-group row">
                      <label class="col-sm-3 col-form-label" for="nombre"><strong><?php echo $strings['Nombre']; ?>:</strong></label>
                      <div>
                          <input type="text" readonly class="form-control-plaintext" name="nombre" value="<?php echo $datos['nombre']; ?>">
                      </div>
                  </div>

                  <div class="form-group row">
                      <label class="col-sm-3 col-form-label" for="telefono"><strong><?php echo $strings['Telefono']; ?>:</strong></label>
                      <div>
                          <input type="text" class="form-control-plaintext" name="telefono" value="<?php echo $datos['telefono']; ?>">
                      </div>
                  </div>

                  <div class="form-group row">
                      <label class="col-sm-3 col-form-label" for="fecha"><strong><?php echo $strings['Fecha']; ?>:</strong></label>
                      <div>
                          <input type="text" readonly class="tcal tcalInput form-control" name="fecha" value="<?php echo cambiar($datos['fecha']); ?>">
                      </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-sm-3 col-form-label" for="hora"><?php echo $strings['Hora'];?>*</label>
                    <div class="col-sm-9">
                        <select id="hora" name="hora"  >
                            <option value="<?php echo $datos['hora']; ?>" selected><?php echo $datos['hora']; ?></option>
                            <option value="9:00"><?php echo '9:00'; ?></option>
                            <option value="10:00"><?php echo '10:30'; ?></option>
                            <option value="11:00"><?php echo '12:00'; ?></option>
                            <option value="12:00"><?php echo '13:30'; ?></option>
                            <option value="17:00"><?php echo '15:00'; ?></option>
                            <option value="18:00"><?php echo '16:30'; ?></option>
                            <option value="19:00"><?php echo '18:00'; ?></option>
                            <option value="20:00"><?php echo '19:30'; ?></option>
                            <option value="21:00"><?php echo '21:00'; ?></option>
                    </select><br>
                    </div>
                </div>

                  <div class="form-group row">
                      <label class="col-sm-3 col-form-label" for="idPista"><strong><?php echo $strings['Pista']; ?>:</strong></label>
                      <div>
                          <input type="text" class="form-control-plaintext" name="idPista" 
                            value="<?php echo $datos['idPista']; ?>">
                      </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-sm-3 col-form-label" for="nivel"><?php echo $strings['Nivel'];?>*</label>
                    <div class="col-sm-9">
                        <select id="nivel" name="nivel"  >
                            <option value="<?php echo $datos['nivel']; ?>" selected><?php echo $datos['nivel']; ?></option>
                            <option value="amateur"><?php echo 'amateur'; ?></option>
                            <option value="intermedio"><?php echo 'intermedio'; ?></option>
                            <option value="profesional"><?php echo 'profesional'; ?></option>
                    </select><br>
                    </div>
                </div>

                  <div class="form-group row">
                      <label class="col-sm-3 col-form-label" for="numPart"><strong><?php echo $strings['Participantes']; ?>:</strong></label>
                      <div>
                        <input type="text" class="form-control-plaintext" name="numPart" 
                            value="<?php echo $datos['numPart']; ?>">
                      </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-sm-3 col-form-label" for="login1"><strong><?php echo $strings['Alumno']; ?> 1 :</strong></label>
                      <div>
                        <input type="text" class="form-control-plaintext" name="login1" 
                            value="<?php echo $datos['login1']; ?>">
                      </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-sm-3 col-form-label" for="login2"><strong><?php echo $strings['Alumno']; ?> 2 :</strong></label>
                      <div>
                        <input type="text" class="form-control-plaintext" name="login2" 
                            value="<?php echo $datos['login2']; ?>">
                      </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-sm-3 col-form-label" for="login3"><strong><?php echo $strings['Alumno']; ?> 3 :</strong></label>
                      <div>
                        <input type="text" class="form-control-plaintext" name="login3" 
                            value="<?php echo $datos['login3']; ?>">
                      </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-sm-3 col-form-label" for="login4"><strong><?php echo $strings['Alumno']; ?> 4 :</strong></label>
                      <div>
                        <input type="text" class="form-control-plaintext" name="login4"
                            value="<?php echo $datos['login4']; ?>">
                      </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-sm-3 col-form-label" for="login5"><strong><?php echo $strings['Alumno']; ?> 5 :</strong></label>
                      <div>
                        <input type="text" class="form-control-plaintext" name="login5"
                            value="<?php echo $datos['login5']; ?>">
                      </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-sm-3 col-form-label" for="login6"><strong><?php echo $strings['Alumno']; ?> 6 :</strong></label>
                      <div>
                        <input type="text" class="form-control-plaintext" name="login6"
                            value="<?php echo $datos['login6']; ?>">
                      </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-sm-3 col-form-label" for="login7"><strong><?php echo $strings['Alumno']; ?> 7 :</strong></label>
                      <div>
                        <input type="text" class="form-control-plaintext" name="login7"
                            value="<?php echo $datos['login7']; ?>">
                      </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-sm-3 col-form-label" for="login8"><strong><?php echo $strings['Alumno']; ?> 8 :</strong></label>
                      <div>
                        <input type="text" class="form-control-plaintext" name="login8"
                            value="<?php echo $datos['login8']; ?>">
                      </div>
                  </div>


                <div class="boton">
                  <button class="btn btn-outline-secondary" type="submit" name="action" value="Showall"><span class="fas fa-undo-alt"></span></button>
                  <button type="submit" class="btn btn-outline-primary"><?php echo $strings['Añadir'];?></button>
                </div>
              </form>
          </div>
<?php
        include '../Views/Footer.php';//Incluye el pie de página
?>
    </html>
<?php
    }//fin del método render
}//Fin REGISTER

?>