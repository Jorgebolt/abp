<?php

class JORNADA_INTENSIVA_Showall
{
    function __construct($datos)
    {
        $this->render($datos);
    }

    function render($datos)
    {

?>
        <html>
        <?php
        include '../Views/Header.php'; //Incluye la cabecera
        ?>

        <body>
            <div class="container">
                <form action="../Controllers/JORNADA_INTENSIVA_Controller.php" method="">
                    <legend>
                        <?php $numeroJor = $datos->num_rows ?>
                        <?php echo 'JORNADAS INTENSIVAS' . ' (' . $numeroJor . ')'; ?>
                        <?php if ($_SESSION['login'] == 'root') { //si es el root, puede crear un campeonato nuevo
                            echo '<button class="btn btn-outline-primary" type="submit" name="action" value="Add">';
                            echo '<span class="fas fa-plus"></span>';
                            echo '</button>';
                        }; ?>
                    </legend>
                </form>
            </div>
        </body>
        <?php
        function cambiar($fecha)
        { //Función para cambiar el formato de la fecha
            $mifecha = explode("-", $fecha);
            $lafecha = $mifecha[2] . "/" . $mifecha[1] . "/" . $mifecha[0];
            return $lafecha;
        }
        ?>
        <table class="table table-hover table-striped">
            <thead class="thead-light">
                <tr>
                    <th>
                        <!-- Aquí tiene que estar cada visita-->
                        <?php echo '#'; ?>
                    </th>
                    <th scope="col">
                        <?php echo 'Usuario'; ?>
                    </th>
                    <th scope="col">
                        <?php echo 'Entrenador'; ?>
                    </th>
                    <th scope="col">
                        <?php echo 'Nivel'; ?>
                    </th>
                    <th scope="col">
                        <?php echo 'Fecha Jornada'; ?>
                    </th>
                    <?php if($_SESSION['login'] == 'root'){
                        echo '<th scope="col">
                                Opciones
                            </th>';
                    }
                    ?>
                </tr>
            </thead>
<?php
            while ($row = $datos->fetch_array()) {
?>
                <tr>
                    <form action="../Controllers/JORNADA_INTENSIVA_Controller.php" method="">

                        <td>
                            <?php
                            $codigo = '#' . $row['idJornada'];
                            echo $codigo;
                            ?>
                        </td> <!-- Campeonato + idCampeonato -->
                        <td>
                            <?php echo $row['loginUser'] ?>
                        </td>

                        <td>
                            <?php echo $row['loginEntrenador'] ?>
                        </td>

                        <td>
                            <?php echo $row['nivel'] ?>
                        </td>

                        <td>
                            <?php echo cambiar($row['fechaJornada']);?>
                        </td>

                        <?php
                        if ($_SESSION['login'] == 'root') { //si es el root, tiene opcion de gestionar
                            //Boton DELETE
                            echo '<td>
                                    <a class="btn btn-outline-danger" href=\'../Controllers/JORNADA_INTENSIVA_Controller.php?action=Delete&idJornada=' . $row['idJornada'] . "'><i class='far fa-trash-alt'></i></a>
                                </td>";
                        };
                        ?>
                        

                    </form>
                </tr>
            <?php
            } //Fin while
            ?>
        </table>
<?php
        include '../Views/Footer.php';
    } //fin metodo render
} //fin clase
?>