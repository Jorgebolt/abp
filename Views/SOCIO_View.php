<?php
class SOCIO_View
{
function __construct()
{
$this->render();
}

function render()
{

    include '../Views/Header.php';

?>
    <div class="addPartido">
   <form name='consultar' id="consultar" action='../Controllers/SOCIO_Controller.php?action=add' method='post' enctype="multipart/form-data" onsubmit="">
            <fieldset>
                <legend><?php echo $strings['Hacerse socio']; ?></legend>
                <br>
                
                <p><?php echo $strings['Coste de 30€/mes']; ?></p>

                <div class="form-group row">
                    <label for="login" class="col-sm-2 col-form-label"><?php echo $strings['Numero tarjeta']; ?>*</label>
                    <div class="col-sm-9">
                        <input class="form-control" type="text" name="numTarjeta" id="numTarjeta">
                    </div>
                </div>


                <div class="form-group row">
                    <label for="login" class="col-sm-2 col-form-label"><?php echo $strings['Numero seguridad']; ?>*</label>
                    <div class="col-sm-9">
                        <input class="form-control" type="text" name="numSeguridad" id="numSeguridad">
                    </div>
                </div>

                
                <button type="submit" class="btn btn-outline-primary"><span class="fas fa-check"></span></button>
                
            </fieldset>
        </form>
    </div>
<?php
    }
}
?>