<?php

class CAMPEONATO_Delete
{

    function __construct($datos)
    {
        $this->render($datos);
    }

    function render($datos)
    {
        include '../Views/Header.php';
        ?>

        <div class="container">
            <h2><?php echo $strings['Eliminar Campeonato']; ?></h2>
            <form onsubmit="return borrado()" action="../Controllers/CAMPEONATO_Controller.php?action=Delete&idCampeonato=<?php echo $datos[0] ?>&borrado=Si" method="post">

                <div class="form-group row">
                    <label class="col-sm-2 col-form-label"><strong><?php echo $strings['Num. participantes']; ?>:</strong></label>
                    <div class="col-sm-10">
                        <input type="text" readonly class="form-control-plaintext" id="numParticipantes" value="<?php if ($row['numParticipantes'] == 0) {echo 'Vacio';}else{echo $datos[1];} ?>">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-sm-2 col-form-label"><strong><?php echo $strings['Limite Inscripcion']; ?>:</strong></label>
                    <div class="col-sm-10">
                        <input type="text" readonly class="form-control-plaintext" id="fechaLimiteInscrip" value="<?php echo $datos[2]; ?>">
                    </div>
                </div>

                <label class="">
                    <?php echo $strings['¿Estás seguro?']; ?>
                </label> <!-- ¿Confirmar borrado? -->
                <!-- Botón para CONFIRMAR el BORRADO de un usuario -->
                <button type="submit" class="btn btn-outline-danger"><i class='fas fa-trash-alt'></i></button>
                <a href="../Controllers/CAMPEONATO_Controller.php?action=Showall" class="btn btn-outline-secondary"><span class="fas fa-undo-alt"></a>
            </form>

    <?php
        }
    }
    ?>