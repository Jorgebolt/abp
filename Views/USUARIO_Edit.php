<?php

class USUARIO_Edit
{
    function __construct($datos)
    {
        $this->render($datos);
    }

    function render($datos){
  
        include '../Views/Header.php'; //Incluye la cabecera
     $row = $datos->fetch_array()
     ?>

  <div class="formRegister">
  
    <h1 class="titulo"><?php echo "EDITAR USUARIO"?></h1>

  <form action='../Controllers/USUARIO_Controller.php' method='post' >

    <div class="form-group row">
      <label class="col-sm-2 col-form-label" for="login"><?php echo $strings['Login'];?>*</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" name="login" readonly  value=  <?php echo $row['login']; ?>>
        </div>
    </div>

    <div class="form-group row">
      <label class="col-sm-2 col-form-label" for="password"><?php echo $strings['Contraseña'];?>*</label>
        <div class="col-sm-9">
          <input type="text" class="form-control" name="password"value=  <?php echo $row['password']; ?>>
        </div>
    </div>

    <div class="form-group row">
      <label class="col-sm-2 col-form-label" for="nombre"><?php echo $strings['Nombre'];?>*</label>
        <div class="col-sm-9">
          <input type="text" class="form-control" name="nombre"value=  <?php echo $row['nombre'];?>>
        </div>
    </div>

    <div class="form-group row">
      <label class="col-sm-2 col-form-label" for="apellidos"><?php echo $strings['Apellidos'];?>*</label>
        <div class="col-sm-9">
          <input type="text" class="form-control" name="apellidos" value=  <?php echo $row['apellidos'];?>>
        </div>
    </div>

    <div class="form-group row">
      <label class="col-sm-2 col-form-label" for="email"><?php echo $strings['Email'];?>*</label>
        <div class="col-sm-9">
          <input type="text" class="form-control" name="email" value=  <?php echo $row['email'];?>>
        </div>
    </div>

    <div class="form-group row">
      <label class="col-sm-2 col-form-label" for="sexo"><?php echo $strings['Sexo'];?>*</label>
        <div class="col-sm-9">
          <select name="sexo" class="form-control">
            <option value = 'HOMBRE'<?php if($row['sexo']=="hombre")  echo "selected";?> ><?php echo $strings['Hombre'];?></option>
            <option value = 'MUJER'<?php if($row['sexo']=="mujer")  echo "selected";?>><?php echo $strings['Mujer'];?></option>
          </select>
        </div>
    </div>

    <div class="form-group row">
      <label class="col-sm-2 col-form-label" for="telefono"><?php echo $strings['Telefono'];?>*</label>
        <div class="col-sm-9">
          <input type="text" class="form-control" name="telefono" value=  <?php echo $row['telefono'];?>>
        </div>
    </div>

    <div class="form-group row">
      <label class="col-sm-2 col-form-label" for="telefono"><?php echo "Tipo"?>*</label>
        <div class="col-sm-9">
        <input type="checkbox" name="administrador" value="Administrador">Administrador<br>
       <input type="checkbox" name="entrenador" value="Entrenador">Entrenador<br>
         <input type="checkbox" name="deportista" value="Deportista">Deportista<br>
        </div>
    </div>

    <div class="form-group row">
      <label class="col-sm-9 col-form-label"><?php echo $strings['* Indica que los campos son obligatorios'];?></label>
    </div>

      <div class="boton">
        <button type="submit" class="btn btn-outline-primary" name="action" value="Edit"><?php echo $strings['Validar'];?></button>
      </div>
  </form>
  </div>
  <?php
   include '../Views/Footer.php';
        ?>
        </html>
        <?php
  } //fin metodo render

} //fin REGISTER

?>
