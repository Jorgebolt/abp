<?php
/*  Archivo PHP con la vista de la Mensaje realizada con una clase
*/
class MESSAGE{

	private $string; 
	private $volver;

	function __construct($string, $volver){
		$this->string = $string;
		$this->volver = $volver;	
		$this->render();
	}

	function render(){
		?>
		<html>
<?php
		include '../Views/Header.php';
?>
		<div class="container">
			<h3>
			<br>
			<br>
			<br>
			<br>
<?php		
				echo $strings[$this->string]; //Contiene la frase para el usuario
?>
			<br>
			<br>
<?php
			echo '<a class="btn btn-outline-secondary" href=\'' . $this->volver . "'>" . '<span class="fas fa-undo"></span>' . " </a>";
?>
			</h3>
		</div>
		<br>
<?php
		include '../Views/Footer.php';
	} //fin metodo render

}
