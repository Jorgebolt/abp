<?php

class LIGAREGULAR_Showcurrent
{
    function __construct($datos)
    {
        $this->render($datos);
    }

    function render($datos)
    {
        ?>
        <html>
        <?php
                include '../Views/Header.php'; //Incluye la cabecera
                ?>
        <div class="formAddPlayer">
            <legend class="titulo"><?php echo $strings['Vista detallada de la liga']; ?> <?php echo $datos['idLiga']; ?></legend>

            <form name='añadir' id="añadir" action='../Controllers/CAMPEONATO_Controller.php?action=Showcurrent&idCampeonato=<?php echo $datos['idCampeonato']; ?>&ligasGeneradas=S' method='post'>

                <div class="form-group row">
                    <label class="col-sm-3 col-form-label"><strong><?php echo 'Enfrentamiento 1:'; ?></strong></label>
                    <div class="col-sm-9">
                        <input type="text" readonly class="form-control-plaintext" id="login1" value="<?php echo $datos['login1'] . '-' . $datos['login2'] . ' VS ' . $datos['login3'] . '-' . $datos['login4']; ?>">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-sm-3 col-form-label"><strong><?php echo 'Enfrentamiento 2:'; ?></strong></label>
                    <div class="col-sm-9">
                        <input type="text" readonly class="form-control-plaintext" id="login1" value="<?php echo $datos['login5'] . '-' . $datos['login6'] . ' VS ' . $datos['login7'] . '-' . $datos['login8']; ?>">
                    </div>
                </div>

                <div class="boton">
                    <button class="btn btn-outline-secondary" type="submit" name="action" value="Showcurrent"><span class="fas fa-undo-alt"></span></button>
                </div>
            </form>
        </div>
<?php
        include '../Views/Footer.php';
    } //fin metodo render
} //fin clase
?>