<?php

class Estadisticas_View
{
    function __construct($balance,$popularidadCampeonatos,$Entrenadores,$partidos)
    {
        $this->render($balance,$popularidadCampeonatos,$Entrenadores,$partidos);
    }

    function render($balance,$popularidadCampeonatos,$Entrenadores,$partidos)
    {

        ?>
        <html>
<?php
    include '../Views/Header.php'; //Incluye la cabecera
  
?>

<?php
///***************************
       


    //graficos importados de canvasjs
?>



<!DOCTYPE HTML>
<html>
<head>  
<script>
window.onload = function () {
 
var chart = new CanvasJS.Chart("chartContainer", {
    animationEnabled: true,
    exportEnabled: true,
    title:{
        text: "Balance Generos"
    },
    subtitles: [{
        text: ""
    }],
    data: [{
        type: "pie",
        showInLegend: "true",
        legendText: "{label}",
        indexLabelFontSize: 16,
        indexLabel: "{label} - #percent%",
        yValueFormatString: ",##0",
        dataPoints: <?php echo json_encode($balance, JSON_NUMERIC_CHECK); ?>
    }]
});
chart.render();

var chart2 = new CanvasJS.Chart("chartContainer2", {
    animationEnabled: true,
    exportEnabled: true,
    title:{
        text: "Popularidad Categoria+nivel"
    },
    subtitles: [{
        text: ""
    }],
    data: [{
        type: "pie",
        showInLegend: "true",
        legendText: "{label}",
        indexLabelFontSize: 16,
        indexLabel: "{label} - #percent%",
        yValueFormatString: "parejas:,##0",
        dataPoints: <?php echo json_encode($popularidadCampeonatos, JSON_NUMERIC_CHECK); ?>
    }]
});

chart2.render();

var chart3 = new CanvasJS.Chart("chartContainer3", {
    animationEnabled: true,
    exportEnabled: true,
    title:{
        text: "Entrenadores Por Deportista"
    },
    subtitles: [{
        text: ""
    }],
    data: [{
        type: "pie",
        showInLegend: "true",
        legendText: "{label}",
        indexLabelFontSize: 16,
        indexLabel: "{label} - #percent%",
        yValueFormatString: ",##0",
        dataPoints: <?php echo json_encode($Entrenadores, JSON_NUMERIC_CHECK); ?>
    }]
});
chart3.render();



}
</script>
</head>
<body>
<div id="chartContainer" style="height: 370px; width: 50%;float:left;"></div>
<div id="chartContainer2" style="height: 370px; width: 50%;float:left;"></div>
<div id="chartContainer3" style="height: 370px; width: 50%;float:left;"></div>
<h1 id="Container4" style="height: 370px; width: 50%;float:left;text-align: center;
vertical-align: middle;"><?php echo "Media Partidos:" . $partidos ?></h1>
<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
</body>
</html>                             

<?php
        include '../Views/Footer.php';
    } //fin metodo render
} //fin clase
?>