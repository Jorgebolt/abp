<?php

class PLAYOFF_Enfrentamientos_Final
{
    function __construct($parejas, $cruce1_ganadora, $cruce2_ganadora)
    {
        $this->render($parejas, $cruce1_ganadora, $cruce2_ganadora);
    }

    function render($parejas, $cruce1_ganadora, $cruce2_ganadora)
    {
        include '../Views/Header.php'; //Incluye la cabecera
?>
    <body>
        <div class="container">
            <form action="../Controllers/CAMPEONATO_Controller.php" method="">
                <legend>
                    <?php echo 'PLAYOFF' . ' (CAMPEONATO ' . $_REQUEST['idCampeonato'] . ' - GRUPO ' . $_REQUEST['idGrupo'] . ')'; ?>
                </legend>

                <table class="table table-hover table-striped">
                    <thead class="thead-light">
                        <tr>
                            <th>
                                <!-- Aquí tiene que estar cada visita-->
                                <?php echo 'Posicion Liga'; ?>
                            </th>
                            <th>
                                <!-- Aquí tiene que estar cada visita-->
                                <?php echo 'Parejas'; ?>
                            </th>
                        </tr>
                    </thead>
<?php
                    while ($row = $parejas->fetch_array()) {
?>
                        <tr>
                            <td>
                                <?php
                                    if($_SESSION['login'] == $row['login1'] || $_SESSION['login'] == $row['login2']){
                                        echo '<b>#' . $row['posPlayoff'] . '</b>';
                                    }else{ 
                                        echo '#' . $row['posPlayoff'];
                                    }
                                ?>
                            </td>
                            <td>
                                <?php
                                    if($_SESSION['login'] == $row['login1'] || $_SESSION['login'] == $row['login2'])
                                        echo '<b>' . $row['login1'] . ' - ' . $row['login2'] . ' (' . $row['puntosLiga'] . ' ptos)</b>';
                                    else
                                        echo $row['login1'] . ' - ' . $row['login2'] . ' (' . $row['puntosLiga'] . ' ptos)';
                                ?>
                            </td>
                        </tr>
<?php
                    }//Fin while
?>
                </table>
            </form>

            <form action="../Controllers/PLAYOFF_Controller.php?action=RegistrarResultadoJornada1&idCampeonato=<?php echo $_REQUEST['idCampeonato']?>&idGrupo=<?php echo $_REQUEST['idGrupo'];?>&idPlayoff=<?php echo $_REQUEST['idPlayoff'];?>" method="post">
                <legend>
                    <?php echo 'CRUCES - FINAL PLAYOFF'; ?>
                </legend>
<?php
                echo '<h3>' . $cruce1_ganadora . ' VS ';
                echo $cruce2_ganadora  . '</h3>';
?>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="ganador_playoff" value="<?php echo $cruce1_ganadora;?>"><?php echo $cruce1_ganadora;?><br>
                    <input class="form-check-input" type="radio" name="ganador_playoff" value="<?php echo $cruce2_ganadora;?>"><?php echo $cruce2_ganadora;?><br><br>
<?php               
                    if($_SESSION['login'] == 'root'){
                        //Boton para REGISTRAR RESULTADO
                        //echo '<button class="btn btn-outline-primary" name="action" value="RegistrarResultadoFinal"><span class="far fa-edit"></span></button>';
                        echo '<button class="btn btn-outline-primary" type="submit" name="action" value="RegistrarResultadoFinal">Registrar Final</span></button>';
                    }
?>
                </div>
            </form>
        </div>
<?php
        include '../Views/Footer.php';
    } //fin metodo render
} //fin clase
?>
