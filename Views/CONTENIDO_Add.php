<?php

class CONTENIDO_Add{

  function __construct(){ 
    $this->render();
  }

  function render(){
  
        include '../Views/Header.php'; //Incluye la cabecera
    ?> 

  <div class="formRegister">
  
    <h1 class="titulo"><?php echo "CREAR CONTENIDO"?></h1>

  <form action='../Controllers/CONTENIDO_Controller.php' method='post' >

     <div class="form-group row">
      <label class="col-sm-2 col-form-label" for="login"><?php echo $strings['Login'];?>*</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" name="login" readonly  value=  <?php echo $_SESSION['login']; ?>>
        </div>
    </div>
    <div class="form-group row">
      <label class="col-sm-2 col-form-label" for="titulo"><?php echo "Titulo";?>*</label>
        <div class="col-sm-9">
          <input type="text" class="form-control" name="titulo" placeholder="Titulo">
        </div>
    </div>

    <div class="form-group row">
      <label class="col-sm-2 col-form-label" for="titulo"><?php echo "Descripcion";?>*</label>
        <div class="col-sm-9">
          <textarea rows="10" cols="50"  name="descripcion" placeholder="Titulo" >
         
           </textarea>
        </div>
    </div>

    <div class="form-group row">
      <label class="col-sm-9 col-form-label"><?php echo $strings['* Indica que los campos son obligatorios'];?></label>
    </div>

      <div class="boton">
        <button type="submit" class="btn btn-outline-primary" name="action" value="Add"><?php echo $strings['Validar'];?></button>
      </div>
  </form>
  </div>
  <?php
   include '../Views/Footer.php';
        ?>
        </html>
        <?php
  } //fin metodo render

} //fin REGISTER

?>
