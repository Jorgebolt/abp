<?php

class Inicio
{
	function __construct()
	{
		$this->render();
	}

	function render()
	{
		include '../Views/Header.php';

		?>
		<?php if (!IsAuthenticated()) { //si esta autenticado?>
			<div  class="Bienvenida">
				<div>
				<h1><?php echo $strings['Padel ABP']; ?></h1><h5><?php echo $strings['(Association of the Best Players)']; ?></h5>
				<p><?php echo $strings['Forma parte del mejor club de pádel a nivel nacional ahora. Hazlo,'];?> </p><a href="../Controllers/Registro_Controller.php"><?php echo $strings['REGÍSTRATE AQUÍ'];?></a>
				<p><?php echo $strings['y si ya eres miembro']; ?></p> 
				<a href="../Controllers/Login_Controller.php"><?php echo $strings['LOGUEATE'];?></a>.
				</div>
			</div>

		<?php 
				include '../Views/MenuSuperior.php'; //Muestra el menu
		?>
		<?php } ?><!--Cierre del if(!isAuthenticated())-->

			<div class="contPartidos">
				<img class="imgPartidos" src="../Views/imagenes/padel.jpg">
				<div class="textPartidos"><h3><?php echo $strings['Promociones']; ?></h3>
					<p><?php echo $strings['Disfruta de tu tiempo libre echando un partido con otros 3 miembros del club en una de nuestras pistas. Todos los días habrán promociones nuevas de partidos']; ?>.</p>
				</div>

			</div>

			<div class="contPartidos">
				<img class="imgPartidos" src="../Views/imagenes/trofeo.jpg">
				<div class="textPartidos"><h3><?php echo $strings['Campeonatos']; ?></h3>
					<p><?php echo $strings['Disfruta de tu tiempo libre echando un partido con otros 3 miembros del club en una de nuestras pistas. Todos los días habrán promociones nuevas de partidos']; ?>.</p>
				</div>

			</div>
<?php
		include '../Views/Footer.php';
		}
}
?>