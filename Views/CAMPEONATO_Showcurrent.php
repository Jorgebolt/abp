<?php

class CAMPEONATO_Showcurrent
{
    function __construct($datos, $participantes, $estaInscrito)
    {
        $this->render($datos, $participantes, $estaInscrito);
    }

    function render($datos, $participantes, $estaInscrito)
    {

        ?>
        <html>
<?php
            include '../Views/Header.php'; //Incluye la cabecera

            function cambiar($fecha)
            { //Función para cambiar el formato de la fecha
                $mifecha = explode("-", $fecha);
                $lafecha = $mifecha[2] . "/" . $mifecha[1] . "/" . $mifecha[0];
                return $lafecha;
            }

            //funcion que controla si se alcanzo la fecha limite de inscripcion de un campeonato
            function limiteAgotado($fechaLimite)
            {
                $hoy = date("d/m/Y", time());
                $parseLimite = cambiar($fechaLimite);

                if ($hoy < $parseLimite) {
                    return false;
                } else {
                    return true;
                }
            }
?>
        <div class="container">
            <h2><?php echo $strings['Detalles Campeonato'] . ' ' . $datos[0]; if($_SESSION['login'] != 'root' && !$estaInscrito && !limiteAgotado($datos[2])){echo ' - ' . $strings['Inscribirse'];} ?></h2>
            <form action="../Controllers/CAMPEONATO_Controller.php?action=Showcurrent&numParticipantes=<?php echo $datos[1];?>&idCampeonato=<?php echo $datos[0];?>" method="post">

                <div class="form-group row">
                    <label class="col-sm-2 col-form-label"><strong><?php echo $strings['Limite Inscripcion']; ?>:</strong></label>
                    <div class="col-sm-10">
                        <input type="text" readonly class="form-control-plaintext" id="fechaLimiteInscrip" value="<?php if(!limiteAgotado($datos[2])){echo cambiar($datos[2]);}else{echo 'INSCRIPCION CERRADA';} ?>">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-sm-2 col-form-label"><strong><?php echo 'Participantes:';?></strong></label>
                    <div class="col-sm-10">
                        <input type="text" readonly class="form-control-plaintext" id="numParticipantes" value="<?php 
                                                                                                                    if($datos[1] == 0){ //imprime "vacio"
                                                                                                                        echo 'Vacio';
                                                                                                                    }else{ //imprime los nombres de los participantes
                                                                                                                        echo $datos[1];
                                                                                                                    }
                                                                                                                ?>">
                    </div>
                </div>

                <!-- Si es el root, aun no se rellenaron los grupos y hay participantes inscritos, puede hacerlo -->
                <?php
                    if ($_SESSION['login'] == 'root' && $_REQUEST['gruposGenerados'] == 'N' && $datos[1] > 0) {
                    //Boton RELLENAR GRUPOS
                    echo '<button class="btn btn-outline-primary" type="submit" name="action" value="RellenarGrupos">GENERAR GRUPOS</span></button><br>';
                } ?>

                <!-- Si es el root, ya se rellenaron los grupos y aun no se generaron los playoffs, puede generarlos -->
                <?php
                    if ($_SESSION['login'] == 'root' && $_REQUEST['gruposGenerados'] == 'S' && $datos[4] == 'N') {
                    //Boton GENERAR PLAYOFF
                    echo '<button class="btn btn-outline-primary" type="submit" name="action" value="GenerarPlayoff">PLAY-OFFS</span></button>';
                } ?>

                <!-- Si es un usuario, puede inscribirse en el campeonato y tiene el boton de volver porque NUNCA visualiza el showall de grupos-->
                <?php if ($_SESSION['login'] != 'root'){
                    //Si la inscripcion esta ABIERTA, el usuario NO esta inscrito y no estan ya generados los grupos
                    if (!limiteAgotado($datos[2]) && !$estaInscrito && $_REQUEST['gruposGenerados'] == 'N'){
                        //Boton INSCRIBIRSE
                        echo '<button class="btn btn-outline-primary" type="submit" name="action" value="Inscribirse"><span class="fas fa-pencil-alt"></span></button>';
                    }
                }?>

                <!-- Boton VOLVER -->
                <?php echo '<button class="btn btn-outline-secondary" name="action" value="Showall"><span class="fas fa-undo-alt"></span></button>';?>
            </form>
<?php
    } //fin metodo render
} //fin clase
?>