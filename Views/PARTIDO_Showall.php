<?php

class Partido_Showall{
    function __construct($lista, $datos, $message, $fuerafecha){
        $this->render($datos, $message, $fuerafecha);
    }

    function render($datos, $message, $fuerafecha){  
?>
        <html>
<?php
        include '../Views/Header.php'; //Incluye la cabecera

                        function  cambiar ( $fecha ){ //Función para cambiar el formato de la fecha
                         $mifecha = explode("-", $fecha);
                        $lafecha=$mifecha[2]."/".$mifecha[1]."/".$mifecha[0]; 
                        return $lafecha; 
                        
                        }?>

                <div class="container">
                    <form action="../Controllers/PARTIDO_Controller.php" method="">
                        <legend>
                            
                            <?php echo $strings['PARTIDOS']; ?>
                            <?php if($_SESSION['login'] == 'root'){ ?>

                                <a class="btn btn-outline-primary" href='../Controllers/PARTIDO_Controller.php?action=Add'>
                                <span class='far fa-plus-square'></a>
                                <?php } ?>
                        </legend>
                    </form>

                    <table class="table table-hover table-striped">
                         <thead class="thead-light">
                            <tr>
                                <th> <!-- Aquí tiene que estar cada partido-->
                                   <?php echo $strings['Partido']; ?>
                                </th>
                                <th scope="col">
                                    <?php echo $strings['Fecha']; ?>
                                </th>
                                <th scope="col">
                                    <?php echo $strings['Hora']; ?>
                                </th>
                                <th scope="col">
                                    <?php echo $strings['Pista']; ?>
                                </th>
                                <th scope="col">
                                    <?php echo $strings['Nº']; ?>
                                </th>
                                <th scope="col">
                                    <?php echo $strings['Participantes']; ?>
                                </th>
                                <th scope="col">
                                    <?php echo $strings['Opciones']; ?>
                                </th>
                             </tr>
                        </thead> 
<?php
            while($row = $datos->fetch_array()){ 
                if($fuerafecha < $row['fecha']){
?>
                 <tr>
                    <form action="../Controllers/PARTIDO_Controller.php" method="">
                                        
                        <td>
                            <?php 
                            $idPartido = $row['idPartido'];

                            $codigo = "Partido ". $row['idPartido'];
                            
                            echo $codigo;?>
                        </td> <!-- Partido + idPartido -->
                        <td>
                            <?php echo cambiar($row['fecha']); ?>
                        </td> 

                         <td>
                            <?php echo $row['hora']; ?>
                        </td> 

                        <td>
                            <?php echo $row['idPista']; ?>
                        </td> 

                        <td>
                            <?php echo $row['numPart'] . '/4'; ?>
                        </td>

                        <td>
                            <?php echo $row['login1'] . ' ' . $row['login2'] . ' ' . $row['login3'] . ' ' . $row['login4']; ?>
                        </td> 

                        <td>
                            <?php if($_SESSION['login'] == 'root'){
                                 echo '<a class="btn btn-outline-danger" href=\'../Controllers/PARTIDO_Controller.php?action=Delete&idPartido=' . $row['idPartido'] . "'>
                                    <i class='far fa-trash-alt'></i></a>";
            
                                }else if ($row['numPart'] != '4'){
                                        if($row['login1'] != $_SESSION['login']){
                                            if($row['login2'] != $_SESSION['login']){
                                                if($row['login3'] != $_SESSION['login']){
                                                    if($row['login4'] != $_SESSION['login']){
                                    echo '<a class="btn btn-outline-primary" href=\'../Controllers/PARTIDO_Controller.php?action=AddPlayer&idPartido=' . $row['idPartido'] . '&login1=' . $_SESSION['login'] . '&numPart=' . $row['numPart'] ."'>
                                        <i class='far fa-plus-square'></i></a>"; 
                                                    } 
                                                }
                                            }
                                        }
                                      }
                                    if ($row['login1'] == $_SESSION['login'] || 
                                        $row['login2'] == $_SESSION['login'] || $row['login3'] == $_SESSION['login'] || 
                                        $row['login4'] == $_SESSION['login']){
                                    echo '<a class="btn btn-outline-danger" href=\'../Controllers/PARTIDO_Controller.php?action=DeletePlayer&idPartido=' . $row['idPartido'] . '&login1=' . $row['login1'] . '&login2=' . $row['login2'] . '&login3=' . $row['login3'] . '&login4=' . $row['login4'] . '&numPart=' . $row['numPart'] ."'><i class='far fa-trash-alt'></i></a>";
                                };?>
                        </td>
                    </form>
                </tr>
<?php
                }else{
                if($_SESSION['login'] == 'root'){
?>
         <tr>
                    <form action="../Controllers/ESCUELA_Controller.php" method="">
                                        
                        <td>
                            <?php 
                            $idPartido = $row['idPartido'];

                            $codigo = "Partido ". $row['idPartido'];
                            
                            echo $codigo;?>
                        </td> <!-- Partido + idPartido -->
                        <td>
                            <?php echo cambiar($row['fecha']); ?>
                        </td> 

                         <td>
                            <?php echo $row['hora']; ?>
                        </td> 

                        <td>
                            <?php echo $row['idPista']; ?>
                        </td> 

                        <td>
                            <?php echo $row['numPart'] . '/4'; ?>
                        </td>

                        <td>
                            <?php echo $row['login1'] . ' ' . $row['login2'] . ' ' . $row['login3'] . ' ' . $row['login4']; ?>
                        </td> 

                        <td>
                            <?php if($_SESSION['login'] == 'root'){
                                 echo '<a class="btn btn-outline-danger" href=\'../Controllers/PARTIDO_Controller.php?action=Delete&idPartido=' . $row['idPartido'] . "'>
                                    <i class='far fa-trash-alt'></i></a>";
            
                                }else if ($row['numPart'] != '4'){
                                        if($row['login1'] != $_SESSION['login']){
                                            if($row['login2'] != $_SESSION['login']){
                                                if($row['login3'] != $_SESSION['login']){
                                                    if($row['login4'] != $_SESSION['login']){
                                    echo '<a class="btn btn-outline-primary" href=\'../Controllers/PARTIDO_Controller.php?action=AddPlayer&idPartido=' . $row['idPartido'] . '&login1=' . $_SESSION['login'] . '&numPart=' . $row['numPart'] ."'>
                                        <i class='far fa-plus-square'></i></a>"; 
                                                    } 
                                                }
                                            }
                                        }
                                      }
                                    if ($row['login1'] == $_SESSION['login'] || 
                                        $row['login2'] == $_SESSION['login'] || $row['login3'] == $_SESSION['login'] || 
                                        $row['login4'] == $_SESSION['login']){
                                    echo '<a class="btn btn-outline-danger" href=\'../Controllers/PARTIDO_Controller.php?action=DeletePlayer&idPartido=' . $row['idPartido'] . '&login1=' . $row['login1'] . '&login2=' . $row['login2'] . '&login3=' . $row['login3'] . '&login4=' . $row['login4'] . '&numPart=' . $row['numPart'] ."'><i class='far fa-trash-alt'></i></a>";
                                };?>
                        </td>
                    </form>
                </tr>





<?php
            }

            }//Fin while
        }
?>                               
            </table>
        </div>
<?php            
            include '../Views/Footer.php'; //Incluye el pie de página
?>
        </html>
<?php

    }//fin del método render
}//Fin REGISTER

?>