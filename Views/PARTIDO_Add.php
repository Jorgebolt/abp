<?php
class Partido_Add{

    function __construct($datos){ 
        $this->render($datos);
    }

    function render($datos){
 ?>

 <html>
    <?php
        include '../Views/Header.php';//Incluye la cabecera
    ?>
    <body>
       <div class="container">
        <div class="addPartido">
    <form name ='añadir'  action='../Controllers/PARTIDO_Controller.php?action=Add' method='post'>
        <legend><?php echo $strings['Añadir partido']; ?></legend>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label" for="fecha"><?php echo $strings['Fecha del partido'];?></label>
            <div class="col-sm-9">
                <input class="tcal tcalInput form-control" type="text" name="fecha" readonly 
                    placeholder="<?php echo $strings['haz click para cambiar la fecha']; ?>">
            </div>
        </div>

        <div class="form-group row">
                    <label class="col-sm-2 col-form-label" for="hora"><?php echo $strings['Hora'];?>*</label>
                    <div class="col-sm-9">
                        <select id="hora" name="hora"  >
                            <option value="" selected>-<?php echo $strings['Hora']; ?>-</option>
                            <option value="9:00"><?php echo '9:00'; ?></option>
                            <option value="10:30"><?php echo '10:30'; ?></option>
                            <option value="12:00"><?php echo '12:00'; ?></option>
                            <option value="13:30"><?php echo '13:30'; ?></option>
                            <option value="15:00"><?php echo '15:00'; ?></option>
                            <option value="16:30"><?php echo '16:30'; ?></option>
                            <option value="18:00"><?php echo '18:00'; ?></option>
                            <option value="19:30"><?php echo '19:30'; ?></option>
                            <option value="21:00"><?php echo '21:00'; ?></option>
                    </select><br>
                    </div>
                </div>

        <a class="btn btn-outline-secondary" href="../Controllers/PARTIDO_Controller.php?action=Showall"> <i class="fas fa-undo"> </i></a>
        <button type="submit" class="btn btn-outline-primary"><span class="fas fa-check"></span></button>             
    </form>
    </div>
</div>

<?php
     include '../Views/Footer.php';//Incluye el pie de p�gina
?> 
    </body>
    
</html>
<?php
        } //fin metodo render
    } //fin REGISTER
?>