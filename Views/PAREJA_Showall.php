<?php

class PAREJA_Showall
{
    function __construct($parejas)
    {
        $this->render($parejas);
    }

    function render($parejas)
    {
        ?>
        <html>
        <div class="container">
            <form action="../Controllers/PAREJA_Controller.php" method="">
                <legend>
                    <?php $numeroParejas = $parejas->num_rows ?>
                    <?php echo 'PAREJAS' . ' (' . $numeroParejas . ')'; ?>
                </legend>
            </form>

            <table class="table table-hover table-striped">
                <thead class="thead-light">
                    <tr>
                        <th>
                            <?php echo 'Deportistas'; ?>
                        </th>
                        <th scope="col">
                            <?php echo 'Categoria'; ?>
                        </th>
                        <th scope="col">
                            <?php echo 'Nivel'; ?>
                        </th>
                        <th scope="col">
                            <?php echo 'Grupo'; ?>
                        </th>
                    </tr>
                </thead>
<?php
    while ($row = $parejas->fetch_array()) {
?>
                    <tr>
                        <form action="../Controllers/PAREJA_Controller.php" method="">
                            <td>
                                <?php 
                                    if($_SESSION['login'] == $row['login1'] || $_SESSION['login'] == $row['login2']){
                                        echo '<b>' . $row['login1'] . '  - ' . $row['login2'] . '</b>';

                                    }else{
                                        echo $row['login1'] . '  - ' . $row['login2'];

                                    }
                                ?>
                            </td>

                            <td>
                                <?php 
                                    if($_SESSION['login'] == $row['login1'] || $_SESSION['login'] == $row['login2']){
                                        echo '<b>' . $row['categoria'] . '</b>';

                                    }else{
                                        echo $row['categoria'];

                                    }
                                ?>
                            </td>

                            <td>
                                <?php 
                                    if($_SESSION['login'] == $row['login1'] || $_SESSION['login'] == $row['login2']){
                                        echo '<b>' . $row['nivel'] . '</b>';

                                    }else{
                                        echo $row['nivel'];
                                    }
                                ?>
                            </td>

                            <td>
                                <?php
                                    if($_SESSION['login'] == $row['login1'] || $_SESSION['login'] == $row['login2']){
                                        if($row['idGrupo'] == null){
                                            echo '<b>SIN GRUPO</b>';
                                        }else{
                                            echo '<b>'. $row['idGrupo'] . '</b>';
                                        }

                                    }else{
                                        if($row['idGrupo'] == null){
                                            echo 'SIN GRUPO';
                                        }else{
                                            echo $row['idGrupo'];
                                        }
                                    }
                                ?>
                            </td>

<?php                           //en principio, no habrá opciones sobre las parejas
                                if ($_SESSION['login'] == 'root' && false) {
                                    echo '<td>
                                            <a class="btn btn-outline-danger" href=\'../Controllers/PARTIDO_Controller.php?action=Delete&idPartido=' . $row['idPartido'] . "'>
                                            <i class='far fa-trash-alt'></i>
                                        </a>";

                                    echo '<a class="btn btn-outline-primary" href=\'../Controllers/PARTIDO_Controller.php?action=AddPlayer&idPartido=' . $row['idPartido'] . '&login1=' . $_SESSION['login'] . '&numPart=' . $row['numPart'] . "'>
                                            <i class='far fa-plus-square'></i>
                                        </a>
                                        </td>
                                    ";
                                }
?>
                        </form>
                    </tr>
<?php
                } //Fin while
?>
            </table>
        </div>
<?php
    //include '../Views/Footer.php'; //Incluye el pie de página
?>
    </html>
<?php
    } //fin del método render
} //Fin REGISTER
?>