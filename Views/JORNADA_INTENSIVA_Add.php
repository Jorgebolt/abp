<?php
class JORNADA_INTENSIVA_Add
{

    function __construct($usuarios, $entenadores)
    {
        $this->render($usuarios, $entenadores);
    }

    function render($usuarios, $entenadores)
    {
?>

        <html>
        <?php
        include '../Views/Header.php'; //Incluye la cabecera
        ?>

        <body>
            <div class="container">
                <div class="addPartido">
                    <form name='añadir' action='../Controllers/JORNADA_INTENSIVA_Controller.php?action=Add' method='post'>
                        <legend>
                            <?php echo 'CREAR JORNADA INTENSIVA'; ?>
                        </legend>

                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label" for="nivel"><?php echo $strings['Nivel']; ?>:</label>
                            <div class="col-sm-9">
                                <select id="nivel" name="nivel">
                                    <option value="amateur"><?php echo 'Amateur'; ?></option>
                                    <option value="intermedio"><?php echo 'Intermedio'; ?></option>
                                    <option value="profesional"><?php echo 'Profesional'; ?></option>
                                </select><br>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label" for="fecha"><?php echo $strings['Fecha']; ?>:</label>
                            <div class="col-sm-9">
                                <input class="tcal tcalInput form-control" type="text" name="fecha" readonly placeholder="<?php echo $strings['haz click para cambiar la fecha']; ?>">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label" for="loginUser"><?php echo 'Usuario'; ?>:</label>
                            <div class="col-sm-9">
                                <select id="loginUser" name="loginUser">
                                    <?php
                                    while ($user = $usuarios->fetch_array()) {
                                        if ($user['login'] == $_SESSION['login']) {
                                            continue;
                                        } else {
                                            echo '<option value="' . $user['login'] . '">' . $user['login'] . '</option>';
                                        }
                                    }
                                    ?>
                                </select><br>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label" for="loginEntrenador"><?php echo 'Entrenador'; ?>:</label>
                            <div class="col-sm-9">
                                <select id="loginEntrenador" name="loginEntrenador">
                                    <?php
                                    while ($mister = $entenadores->fetch_array()) {
                                        if ($mister['login'] == $_SESSION['login']) {
                                            continue;
                                        } else {
                                            echo '<option value="' . $mister['login'] . '">' . $mister['login'] . '</option>';
                                        }
                                    }
                                    ?>
                                </select><br>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-outline-primary"><span class="fas fa-check"></span></button>
                        <a class="btn btn-outline-secondary" href="../Controllers/JORNADA_INTENSIVA_Controller.php?action=Showall"> <i class="fas fa-undo"> </i></a>
                    </form>
                </div>
            </div>

            <?php
            include '../Views/Footer.php'; //Incluye el pie de p�gina
            ?>
        </body>

        </html>
<?php
    } //fin metodo render
} //fin REGISTER
?>