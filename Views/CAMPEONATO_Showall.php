<?php

class CAMPEONATO_Showall
{
    function __construct($datos)
    {
        $this->render($datos);
    }

    function render($datos)
    {

        ?>
        <html>
<?php
    include '../Views/Header.php'; //Incluye la cabecera
?>
        <body>
            <div class="container">
                <form action="../Controllers/CAMPEONATO_Controller.php" method="">
                    <legend>
                        <?php $numeroCamp = $datos->num_rows ?>
                        <?php echo $strings['CAMPEONATOS'] . ' (' . $numeroCamp . ')'; ?>
                        <?php if ($_SESSION['login'] == 'root') { //si es el root, puede crear un campeonato nuevo
                            echo '<button class="btn btn-outline-primary" type="submit" name="action" value="Add">';
                            echo '<span class="fas fa-plus"></span>';
                            echo '</button>';
                        }; ?>
                    </legend>
                </form>
            </div>
        </body>
        <?php
            function cambiar($fecha)
            { //Función para cambiar el formato de la fecha
                $mifecha = explode("-", $fecha);
                $lafecha = $mifecha[2] . "/" . $mifecha[1] . "/" . $mifecha[0];
                return $lafecha;
            }
            //funcion que controla si se alcanzo la fecha limite de inscripcion de un campeonato
            function limiteAgotado($fechaLimite)
            {
                $hoy = date("d/m/Y", time());
                $parseLimite = cambiar($fechaLimite);

                if ($hoy < $parseLimite) {
                    return false;
                } else {
                    return true;
                }
            }
        ?>

        <table class="table table-hover table-striped">
            <thead class="thead-light">
                <tr>
                    <th>
                        <!-- Aquí tiene que estar cada visita-->
                        <?php echo $strings['Campeonato']; ?>
                    </th>
                    <th scope="col">
                        <?php echo 'Participantes'; ?>
                    </th>
                    <th scope="col">
                        <?php echo 'Fecha Inscripcion'; ?>
                    </th>
                    <th scope="col">
                        <?php echo 'Grupos';?>
                    </th>
                    <th scope="col">
                        <?php echo 'PlayOffs';?>
                    </th>
                    <th scope="col">
                        <?php echo $strings['Opciones']; ?>
                    </th>
                </tr>
            </thead>
            <?php
                    while ($row = $datos->fetch_array()) {
                        ?>
                <tr>
                    <form action="../Controllers/CAMPEONATO_Controller.php" method="">

                        <td>
                            <?php
                                $codigo = "Campeonato " . $row['idCampeonato'];
                                echo $codigo;
                            ?>
                        </td> <!-- Campeonato + idCampeonato -->

                        <td>
                            <?php if($row['numParticipantes'] == 0){ echo 'Vacio';}else{ echo $row['numParticipantes'];} ?>
                        </td>

                        <td>
                            <?php if(!limiteAgotado($row['fechaLimiteInscrip'])){ echo cambiar($row['fechaLimiteInscrip']);}else{echo 'INSCRIPCION CERRADA';} ?>
                        </td>

                        <td>
                            <?php if($row['gruposGenerados'] == 'S') echo $row['gruposGenerados'] . 'I';else echo $row['gruposGenerados'] . 'O'?>
                        </td>
                        <td>
                            <?php if($row['playoffsGenerados'] == 'S') echo $row['playoffsGenerados'] . 'I';else echo $row['playoffsGenerados'] . 'O'?>
                        </td>

                        <td>
                            <?php 
                            if ($_SESSION['login'] == 'root') { //si es el root, tiene opcion de gestionar
                                //Boton SHOWCURRENT
                                echo '<a class="btn btn-outline-primary" href=\'../Controllers/CAMPEONATO_Controller.php?action=Showcurrent&idCampeonato=' . $row['idCampeonato'] .  '&gruposGenerados=' . $row['gruposGenerados'] . "'><i class='far fa-eye'></i></a>";
                                //Boton DELETE
                                echo '<a class="btn btn-outline-danger" href=\'../Controllers/CAMPEONATO_Controller.php?action=Delete&idCampeonato=' . $row['idCampeonato'] . "'><i class='far fa-trash-alt'></i></a>";
                            
                            } else{ //si es un usuario normal, se puede inscribir entrando al showcurrent
                                //Boton INSCRIBIRSE
                                echo '<a class="btn btn-outline-primary" href=\'../Controllers/CAMPEONATO_Controller.php?action=Showcurrent&idCampeonato=' . $row['idCampeonato'] .  '&gruposGenerados=' . $row['gruposGenerados'] . "'><i class='far fa-eye'></i></a>";
                                
                            };?>
                        </td>
                        
                    </form>
                </tr>
            <?php
                    } //Fin while
                    ?>
        </table>

<?php
        include '../Views/Footer.php';
    } //fin metodo render
} //fin clase
?>