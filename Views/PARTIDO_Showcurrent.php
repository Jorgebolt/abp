<?php

  class PARTIDO_Showcurrent{


    function __construct($datos){ 
      $this->render($datos);
    }

    function render($datos){
?>
        <html>
<?php
            include '../Views/Header.php'; //Incluye la cabecera
?>
    
          <div class="formAddPlayer">
              <legend class="titulo"><?php echo $strings['Vista detallada del partido']; ?> <?php echo 
              $strings['Partido']. $datos['idPartido']; ?></legend>

              <form name ='añadir' id="añadir" action='../Controllers/PARTIDO_Controller.php?action=AddPlayer' method='post'>

                  <?php
                        function  cambiar ( $fecha ){ //Función para cambiar el formato de la fecha
                         $mifecha = explode("-", $fecha);
                        $lafecha=$mifecha[2]."/".$mifecha[1]."/".$mifecha[0]; 
                        return $lafecha; 
                    }?>

                  <div class="form-group row">
                      <label class="col-sm-3 col-form-label"><strong><?php echo $strings['Fecha']; ?>:</strong></label>
                      <div>
                          <input type="text" readonly class="form-control-plaintext" id="fecha" value="<?php echo cambiar($datos['fecha']); ?>">
                      </div>
                  </div>

                  <div class="form-group row">
                      <label class="col-sm-3 col-form-label"><strong><?php echo $strings['Pista']; ?>:</strong></label>
                      <div>
                          <input type="text" readonly class="form-control-plaintext" id="idPista" 
                            value="<?php echo $datos['idPista']; ?>">
                      </div>
                  </div>

                  <?php if ($datos['login1'] == NULL){ ?>

                    <div class="form-group row">
                      <label class="col-sm-2 col-form-label" for="login1"><?php echo $strings['Jugador']; ?> 1 :</label>
                      <div class="col-sm-9">
                          <input type="text"  class="form-control" name="login1" 
                              placeholder="<?php echo $strings['Introduce el login'];?>">
                      </div>
                    </div>

                  <?php  }else{  ?>

                    <div class="form-group row">
                      <label class="col-sm-3 col-form-label"><strong><?php echo $strings['Jugador']; ?> 1 :</strong></label>
                      <div>
                          <input type="text" readonly class="form-control-plaintext" id="login1" 
                            value="<?php echo $datos['login1']; ?>">
                      </div>
                    </div>

                   <?php } ?>
                  

                  <?php if ($datos['login2'] == NULL){ ?>

                    <div class="form-group row">
                      <label class="col-sm-2 col-form-label" for="login2"><?php echo $strings['Jugador']; ?> 2 :</label>
                      <div class="col-sm-9">
                          <input type="text"  class="form-control" name="login2" 
                              placeholder="<?php echo $strings['Introduce el login'];?>">
                      </div>
                    </div>

                  <?php  }else{  ?>

                    <div class="form-group row">
                      <label class="col-sm-3 col-form-label"><strong><?php echo $strings['Jugador']; ?> 2 :</strong></label>
                      <div>
                          <input type="text" readonly class="form-control-plaintext" id="login2" 
                            value="<?php echo $datos['login2']; ?>">
                      </div>
                    </div>

                   <?php } ?>

                  <?php if ($datos['login3'] == NULL){ ?>

                    <div class="form-group row">
                      <label class="col-sm-2 col-form-label" for="login3"><?php echo $strings['Jugador']; ?> 3 :</label>
                      <div class="col-sm-9">
                          <input type="text"  class="form-control" name="login3" 
                              placeholder="<?php echo $strings['Introduce el login'];?>">
                      </div>
                    </div>

                  <?php  }else{  ?>

                    <div class="form-group row">
                      <label class="col-sm-3 col-form-label"><strong><?php echo $strings['Jugador']; ?> 3 :</strong></label>
                      <div>
                          <input type="text" readonly class="form-control-plaintext" id="login3" 
                            value="<?php echo $datos['login3']; ?>">
                      </div>
                    </div>

                   <?php } ?>

                  <?php if ($datos['login4'] == NULL){ ?>

                    <div class="form-group row">
                      <label class="col-sm-2 col-form-label" for="login4"><?php echo $strings['Jugador']; ?> 4 :</label>
                      <div class="col-sm-9">
                          <input type="text"  class="form-control" name="login4" 
                              placeholder="<?php echo $strings['Introduce el login'];?>">
                      </div>
                    </div>

                  <?php  }else{  ?>

                    <div class="form-group row">
                      <label class="col-sm-3 col-form-label"><strong><?php echo $strings['Jugador']; ?> 4 :</strong></label>
                      <div>
                          <input type="text" readonly class="form-control-plaintext" id="login4"
                            value="<?php echo $datos['login4']; ?>">
                      </div>
                    </div>

                   <?php } ?>

                <div class="boton">
                  <button class="btn btn-outline-secondary" type="submit" name="action" value="Showall"><span class="fas fa-undo-alt"></span></button>
                  <button type="submit" class="btn btn-outline-primary"><?php echo $strings['Añadir'];?></button>
                </div>
              </form>
          </div>
<?php
        include '../Views/Footer.php';//Incluye el pie de página
?>
    </html>
<?php
    }//fin del método render
}//Fin REGISTER

?>