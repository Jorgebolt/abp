<?php
//Clase login que contine el formulario para acceder a la aplicacion

class Login{
    function __construct()
    {
        $this->render();
    }

    function render(){

    ?>
    
    <?php
         include '../Views/Header.php'; //Incluye la cabecera
    ?>

        <div class="formLogin">
  
        <h1 class="tituloLogin"><?php echo 'Login';?></h1>

        <form name='form' action='../Controllers/Login_Controller.php' method='post'>

            <div class="form-group row">
                <label class="col-sm-2 col-form-label" for="login"><?php echo 'Login';?></label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="login" placeholder="<?php echo 'Login';?>">
                </div>
            </div>

            <div class="form-group row">
                <label class="col-sm-2 col-form-label" for="password"><?php echo 'Password';?></label>
                <div class="col-sm-9">
                    <input type="password" class="form-control" name="password" placeholder="<?php echo 'Password';?>">
                </div>
            </div>

            <div class="boton">
                <a class="btn btn-outline-secondary" href="../Controllers/USUARIO_Controller.php"> <i class="fas fa-undo"> </i></a>
                <button type="submit" class="btn btn-outline-primary"><span class="fas fa-check"></span></button>
            </div>
        </form>
        </div>
<?php

        include '../Views/Footer.php';
    }//fin metodo render
}//fin Login

?>