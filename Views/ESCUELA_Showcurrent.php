<?php

  class ESCUELA_Showcurrent{


    function __construct($datos){ 
      $this->render($datos);
    }

    function render($datos){
?>
        <html>
<?php
            include '../Views/Header.php'; //Incluye la cabecera
?>
    
          <div class="formAddPlayer">
              <legend class="titulo"><?php echo $strings['Vista detallada de la escuela']; ?></legend>

              <?php
                        function  cambiar ( $fecha ){ //Función para cambiar el formato de la fecha
                         $mifecha = explode("-", $fecha);
                        $lafecha=$mifecha[2]."/".$mifecha[1]."/".$mifecha[0]; 
                        return $lafecha; 
                    }?>

                  <div class="form-group row">
                      <label class="col-sm-3 col-form-label"><strong><?php echo $strings['Nombre']; ?>:</strong></label>
                      <div>
                          <input type="text" readonly class="form-control-plaintext" id="nombre" value="<?php echo $datos['nombre']; ?>">
                      </div>
                  </div>

                  <div class="form-group row">
                      <label class="col-sm-3 col-form-label"><strong><?php echo $strings['Telefono']; ?>:</strong></label>
                      <div>
                          <input type="text" readonly class="form-control-plaintext" id="telefono" value="<?php echo $datos['telefono']; ?>">
                      </div>
                  </div>

                  <div class="form-group row">
                      <label class="col-sm-3 col-form-label"><strong><?php echo $strings['Fecha']; ?>:</strong></label>
                      <div>
                          <input type="text" readonly class="form-control-plaintext" id="fecha" value="<?php echo cambiar($datos['fecha']); ?>">
                      </div>
                  </div>

                  <div class="form-group row">
                      <label class="col-sm-3 col-form-label"><strong><?php echo $strings['Hora']; ?>:</strong></label>
                      <div>
                          <input type="text" readonly class="form-control-plaintext" id="hora" value="<?php echo $datos['hora']; ?>">
                      </div>
                  </div>

                  <div class="form-group row">
                      <label class="col-sm-3 col-form-label"><strong><?php echo $strings['Pista']; ?>:</strong></label>
                      <div>
                          <input type="text" readonly class="form-control-plaintext" id="idPista" 
                            value="<?php echo $datos['idPista']; ?>">
                      </div>
                  </div>

                  <div class="form-group row">
                      <label class="col-sm-3 col-form-label"><strong><?php echo $strings['Nivel']; ?>:</strong></label>
                      <div>
                          <input type="text" readonly class="form-control-plaintext" id="nivel" 
                            value="<?php echo $datos['nivel']; ?>">
                      </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-sm-3 col-form-label"><strong><?php echo $strings['Participantes']; ?>:</strong></label>
                      <div>
                          <input type="text" readonly class="form-control-plaintext" id="numPart" 
                            value="<?php echo $datos['numPart']; ?>">
                      </div>
                  </div>
                  

                    <div class="form-group row">
                      <label class="col-sm-3 col-form-label"><strong><?php echo $strings['Alumno']; ?> 1 :</strong></label>
                      <div>
                          <input type="text" readonly class="form-control-plaintext" id="login1" 
                            value="<?php echo $datos['login1']; ?>">
                      </div>
                    </div>

                    <div class="form-group row">
                      <label class="col-sm-3 col-form-label"><strong><?php echo $strings['Alumno']; ?> 2 :</strong></label>
                      <div>
                          <input type="text" readonly class="form-control-plaintext" id="login2" 
                            value="<?php echo $datos['login2']; ?>">
                      </div>
                    </div>

                    <div class="form-group row">
                      <label class="col-sm-3 col-form-label"><strong><?php echo $strings['Alumno']; ?> 3 :</strong></label>
                      <div>
                          <input type="text" readonly class="form-control-plaintext" id="login3" 
                            value="<?php echo $datos['login3']; ?>">
                      </div>
                    </div>

                  <div class="form-group row">
                    <label class="col-sm-3 col-form-label"><strong><?php echo $strings['Alumno']; ?> 4 :</strong></label>
                      <div>
                        <input type="text" readonly class="form-control-plaintext" id="login4"
                            value="<?php echo $datos['login4']; ?>">
                      </div>
                    </div>

                  <div class="form-group row">
                    <label class="col-sm-3 col-form-label"><strong><?php echo $strings['Alumno']; ?> 5 :</strong></label>
                      <div>
                        <input type="text" readonly class="form-control-plaintext" id="login5"
                            value="<?php echo $datos['login5']; ?>">
                      </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-sm-3 col-form-label"><strong><?php echo $strings['Alumno']; ?> 6 :</strong></label>
                      <div>
                        <input type="text" readonly class="form-control-plaintext" id="login6"
                            value="<?php echo $datos['login6']; ?>">
                      </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-sm-3 col-form-label"><strong><?php echo $strings['Alumno']; ?> 7 :</strong></label>
                      <div>
                        <input type="text" readonly class="form-control-plaintext" id="login7"
                            value="<?php echo $datos['login7']; ?>">
                      </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-sm-3 col-form-label"><strong><?php echo $strings['Alumno']; ?> 8 :</strong></label>
                      <div>
                        <input type="text" readonly class="form-control-plaintext" id="login8"
                            value="<?php echo $datos['login8']; ?>">
                      </div>
                  </div>

                <div class="boton">
                  <a class="btn btn-outline-secondary" href="../Controllers/ESCUELA_Controller.php?action=Showall"> 
                    <i class="fas fa-undo"> </i></a>
                </div>
        </div>
<?php
        include '../Views/Footer.php';//Incluye el pie de página
?>
    </html>
<?php
    }//fin del método render
}//Fin REGISTER

?>