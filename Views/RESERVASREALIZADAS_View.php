<?php

class RESERVASREALIZADAS_View
{
    function __construct($datos)
    {
        $this->render($datos);
    }

    function render($datos)
    {
        ?>
        <html>
        <?php
                include '../Views/Header.php'; //Incluye la cabecera

                ?>
        <div class="container">
            <form action="../Controllers/RESERVAPISTA_Controller.php" method="">
                <legend>

                    <?php echo $strings['RESERVAS']; ?>
                    <?php 
                                echo '<a class="btn btn-outline-primary" href=\'../Controllers/RESERVAPISTA_Controller.php?action=empezar&fecha=' .  "'>
                                    <i class='far fa-plus-square'></i></a>";
                            



                                ?>
                        </legend>
                    </form>

                    <table class="table table-hover table-striped">
                         <thead class="thead-light">
                            <tr>

                                
                                <th> 
                                    <?php if ($_SESSION['login'] == 'root'){
                                    echo $strings['IDReserva']; 
                                    };
                                    ?>
                                </th>

                                <th> 
                                    <?php if ($_SESSION['login'] == 'root'){
                                    echo $strings['Login']; 
                                    };
                                    ?>
                                </th>
                                
                                <th> 
                                   <?php echo $strings['Pista']; ?>
                                </th>
                                <th> 
                                   <?php echo $strings['Fecha']; ?>
                                </th>
                                <th>
                                    <?php echo $strings['Hora']; ?>
                                </th>
                                <th scope="col">
                                    <?php echo $strings['Opciones']; ?>
                                </th>
                                
                             </tr>
                        </thead> 
<?php
            while($row = $datos->fetch_array()){ 
?>
                 <tr>
                    <form action="../Controllers/RESERVAPISTA_Controller.php" method="">
                         

                         <td> 
                            <?php if ($_SESSION['login'] == 'root'){
                            echo $row['idReserva'];; 
                            };
                            ?>
                        </td>

                        <td> 
                            <?php if ($_SESSION['login'] == 'root'){
                            echo $row['login'];; 
                            };
                            ?>
                        </td>
                        <td>
                            <?php echo $row['idPista']; ?>
                        </td>

                        <td>
                            <?php 
                            
                            
                            echo $row['fecha'];?>
                        </td> <!-- Partido + idPartido -->

                        <td>
                            <?php echo $row['hora']; ?>
                        </td>

                        <td>
                            <?php 
                                    echo '<a class="btn btn-outline-danger" href=\'../Controllers/RESERVAPISTA_Controller.php?action=delete&idReserva=' . $row['idReserva'] . "'>

                                    <i class='far fa-trash-alt'></i></a>";
                                            


                                            ?>
                            </td>

                        </form>
                    </tr>
                <?php
                        } //Fin while
                        ?>
            </table>
        </div>
        <?php
                include '../Views/Footer.php'; //Incluye el pie de página
                ?>

        </html>
<?php

    } //fin del método render
} //Fin REGISTER

?>