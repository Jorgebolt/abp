<?php

class PLAYOFF_Showall
{
    function __construct($playoffs, $playoffsGenerados)
    {
        $this->render($playoffs, $playoffsGenerados);
    }

    function render($playoffs, $playoffsGenerados)
    {

?>
<html>
    <body>
        <div class="container">
            <form action="../Controllers/PLAYOFF_Controller.php" method="">
                <legend>
                    <?php $numeroPlayoffs = $playoffs->num_rows ?>
                    <?php echo 'GRUPOS - FASE PLAYOFF' . ' (' . $numeroPlayoffs . ')'; ?>
                </legend>
            </form>
        </div>
    </body>
    <table class="table table-hover table-striped">
        <thead class="thead-light">
            <tr>
                <th>
                    <?php echo 'Grupo';?>
                </th>
                <th scope="col">
                    <?php echo 'Categoria';?>
                </th>
                <th scope="col">
                    <?php echo 'Nivel';?>
                </th>
                <th scope="col">
                    <?php echo 'Cruces';?>
                </th>
            </tr>
        </thead>
<?php
        while ($row = $playoffs->fetch_array()) {
?>
            <tr>
                <form action="../Controllers/PLAYOFF_Controller.php" method="">

                    <td>
                        <?php echo 'Playoff ' . $row[0]. ' (Grupo ' . $row['idGrupo'] . ')';?>
                    </td>

                    <td>
                        <?php echo $row['categoria'];?>
                    </td>

                    <td>
                        <?php echo $row['nivel'];?>
                    </td>

                    <td>
                        <?php if($row['ganador'] != null){
                            //Boton VER GANADOR
                            echo '<a class="btn btn-outline-primary" href=\'../Controllers/PLAYOFF_Controller.php?action=ShowGanador&idPlayoff=' . $row[0] . '&idCampeonato=' . $row['idCampeonato'] . '&idGrupo=' . $row['idGrupo'] . '&ganador=' . $row['ganador'] . "'><i class='far fa-star'></i></a>";
                        }else if($playoffsGenerados == 'S'){ //si ya se generaron los playoffs
                            //visualizara las opciones
                            //Boton APUNTAR RESULTADO
                            echo '<a class="btn btn-outline-primary" href=\'../Controllers/PLAYOFF_Controller.php?action=ShowCruces&idPlayoff=' . $row[0] . '&idCampeonato=' . $row['idCampeonato'] . '&idGrupo=' . $row['idGrupo'] . '&categoria=' . $row['categoria'] . '&nivel=' . $row['nivel'] . "'><i class='far fa-eye'></i></a>";

                        };?>
                    </td>
                </form>
            </tr>
<?php
        } //Fin while
?>
    </table>
<?php
        include '../Views/Footer.php';
    } //fin metodo render
} //fin clase
?>