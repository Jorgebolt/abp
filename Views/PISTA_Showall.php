<?php

class Pista_Showall{
    function __construct($lista, $datos, $lista2, $datos2, $message){
        $this->render($datos, $datos2, $message);
    }

    function render($datos, $datos2, $message){  
?>
        <html>
<?php
        include '../Views/Header.php'; //Incluye la cabecera

?>    
                <div class="container">
                    <form action="../Controllers/PISTA_Controller.php" method="">
                        <legend class="leyendaEscuelas">
                            <?php $numeroPistas = $datos->num_rows;
                             echo $strings['PISTAS'] . ' (' . $numeroPistas . ')'; 
                             if($_SESSION['login'] == 'root'){ ?>

                                <a class="btn btn-outline-primary" href='../Controllers/PISTA_Controller.php?action=Add'>
                                <span class='far fa-plus-square'></a>
                            <?php } ?>
                        </legend>
                    </form>

                    <table class="table table-hover table-striped">
                         <thead class="thead-light">
                            <tr>
                                <th> <!-- Aquí tiene que estar cada escuela-->
                                   <?php echo $strings['Pista']; ?>
                                </th>
                                <th scope="col">
                                    <?php echo $strings['Horarios']; ?>
                                </th>
                                <th scope="col">
                                    <?php echo $strings['Descripcion']; ?>
                                </th>
                                <th scope="col">
                                    <?php echo $strings['Opciones']; ?>
                                </th>
                             </tr>
                        </thead> 
    <?php while($row2 = $datos2->fetch_array()){ 
        $hora1 = $row2['hora1']; $hora2 = $row2['hora2']; $hora3 = $row2['hora3']; $hora4 = $row2['hora4'];
        $hora5 = $row2['hora5']; $hora6 = $row2['hora6']; $hora7 = $row2['hora7']; $hora8 = $row2['hora8'];
        $hora9 = $row2['hora9'];
    }?>

<?php
            while($row = $datos->fetch_array()){                 
?>
                 <tr>
                    <form action="../Controllers/PISTA_Controller.php" method="">
                                        
                        <td>
                            <?php 
                            $idEscuela = $row['idPista'];

                            $codigo = "Pista ". $row['idPista'];
                            
                            echo $codigo;?>
                        </td> <!-- Pista + idPista -->

                        <td>
                            <?php echo $hora1 . ' ' . $hora2 . ' ' . $hora3 . ' ' . $hora4 . ' ' . $hora5 . ' ' .
                                       $hora6 . ' ' . $hora7 . ' ' . $hora8 . ' ' . $hora9 ; ?>
                        </td> 

                         <td>
                            <?php echo $row['descripcion']; ?>
                        </td> 


                        <td>
                            <?php
                             if($_SESSION['login'] == 'root'){

                                 echo '<a class="btn btn-outline-primary" href=\'../Controllers/PISTA_Controller.php?action=Edit&idPista='.$row['idPista']."'><i class='fas fa-pencil-alt'></i></a>";

                                 echo '<a class="btn btn-outline-danger" href=\'../Controllers/PISTA_Controller.php?action=Delete&idPista=' . $row['idPista'] . "'>
                                    <i class='far fa-trash-alt'></i></a>";
            
                                }?>
                        </td>
                    </form>
                </tr>
                             
<?php
        }//Fin while
?>                               
            </table>
        </div>
<?php            
            include '../Views/Footer.php'; //Incluye el pie de página
?>
        </html>
<?php

    }//fin del método render
}//Fin REGISTER

?>