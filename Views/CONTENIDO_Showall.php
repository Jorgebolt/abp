<?php

class CONTENIDO_Showall
{
    function __construct($datos)
    {
        $this->render($datos);
    }

    function render($datos)
    {

        ?>
        <html>
<?php
    include '../Views/Header.php'; //Incluye la cabecera
?>
        <body>
            <div class="container">
                <form action="../Controllers/CONTENIDO_Controller.php" method="">
                    <legend>
                        <?php $numeroCamp = $datos->num_rows ?>
                        <?php if ($_SESSION['login'] == 'root') { //si es el root, puede crear un campeonato nuevo
                            echo '<button class="btn btn-outline-primary" type="submit" name="action" value="Add">';
                            echo '<span class="fas fa-plus"></span>';
                            echo '</button>';
                        }; ?>
                    </legend>
                </form>
            </div>
        </body>
       

        <table class="table table-hover table-striped">
            <thead class="thead-light">
                <tr>
         
                    <th scope="col">
                        <?php echo 'titulo'; ?>
                    </th>
                   <th scope="col">
                        <?php echo 'Autor'; ?>
                    </th>
                    <th scope="col">
                       
                    </th>
               
                </tr>
            </thead>
 
                   <?php while ($row = $datos->fetch_array()) {
                        ?>
                <tr>
                    <form action="../Controllers/CONTENIDO_Controller.php" method="">

                        <td>
                            <?php
                                
                                     
                                        echo $row['titulo'];
                                        ?>
                        </td> 

                        <td>
                            <?php
                                
                                     
                                        echo  $row['login'];
                                        ?>
                        </td> 

                       

                        <td>
                            <?php if ($_SESSION['login'] == 'root')  echo '<a class="btn btn-outline-danger" href=\'../Controllers/CONTENIDO_Controller.php?action=Delete&idcontenido=' . $row['idcontenido'] . "'><i class='far fa-trash-alt'></i></a>"; ?>
                            <?php echo '<a class="btn btn-outline-primary" href=\'../Controllers/CONTENIDO_Controller.php?action=Showcurrent&idcontenido=' . $row['idcontenido'] . "'><i class='far fa-eye'></i></a>"; ?>
                            <?php if ($_SESSION['login'] == 'root') echo '<a class="btn btn-outline-primary" href=\'../Controllers/CONTENIDO_Controller.php?action=Edit&idcontenido=' .$row['idcontenido'] . "'><i class='far fa-edit'></i></a>"; ?>
                        </td>
                    </form>
                </tr>

<?php
                    } //Fin while
                    ?>
        </table>

<?php
        include '../Views/Footer.php';
    } //fin metodo render
} //fin clase
?>