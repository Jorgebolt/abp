<?php

class APUNTAR_RESULTADO
{
    function __construct($login1_parejaUser, $login2_parejaUser, $login1_parejaRival, $login2_parejaRival)
    {
        $this->render($login1_parejaUser, $login2_parejaUser, $login1_parejaRival, $login2_parejaRival);
    }

    function render($login1_parejaUser, $login2_parejaUser, $login1_parejaRival, $login2_parejaRival)
    {
        $parejaUser = $login1_parejaUser . ' ' . $login2_parejaUser;
        $parejaRival = $login1_parejaRival . ' ' . $login2_parejaRival;
?>
        <html>
<?php
            include '../Views/Header.php'; //Incluye la cabecera
?>           
  <div class="container">
        <h2><?php echo 'Registrar Resultado Liga Regular';?></h2>
        <div class="form-check">
        <br><h5>GANADOR:</h5>
        <form action="../Controllers/GRUPO_Controller.php?idCampeonato=<?php echo $_REQUEST['idCampeonato']?>&idGrupo=<?php echo $_REQUEST['idGrupo'];?>&pareja1=<?php echo $parejaUser;?>&pareja2=<?php echo $parejaRival;?>" method="post">
            <input class="form-check-input" type="radio" name="pareja_ganadora" value="<?php echo $parejaUser;?>"><?php echo $login1_parejaUser . ' - ' . $login2_parejaUser;?><br>
            <input class="form-check-input" type="radio" name="pareja_ganadora" value="<?php echo $parejaRival;?>"><?php echo $login1_parejaRival . ' - ' . $login2_parejaRival;?><br><br>
<?php
        //Boton para REGISTRAR RESULTADO
        echo '<button class="btn btn-outline-primary" name="action" value="RegistrarResultado"><span class="far fa-edit"></span></button>';
        
        //Boton para VOLVER al showcurrent del campeonato
        echo '<a class="btn btn-outline-secondary" href=\'../Controllers/CAMPEONATO_Controller.php?action=Showcurrent&idCampeonato=' . $_REQUEST['idCampeonato'] . "'><i class='fas fa-undo-alt'></i></a>";
?>
        </form>
    </div>
<?php
        include '../Views/Footer.php';
    } //fin metodo render
} //fin clase
?>