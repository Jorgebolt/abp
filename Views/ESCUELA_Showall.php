<?php

class Escuela_Showall{
    function __construct($lista, $datos, $message, $fechafin){
        $this->render($datos, $message, $fechafin);
    }

    function render($datos, $message, $fechafin){  
?>
        <html>
<?php
        include '../Views/Header.php'; //Incluye la cabecera

                        function  cambiar ( $fecha ){ //Función para cambiar el formato de la fecha
                         $mifecha = explode("-", $fecha);
                        $lafecha=$mifecha[2]."/".$mifecha[1]."/".$mifecha[0]; 
                        return $lafecha; 
                        }?>
   
                <div class="container">
                    <form action="../Controllers/ESCUELA_Controller.php" method="">
                        <legend class="leyendaEscuelas">
                            <?php
                             $cont = 0; $entrenador = false;
                             echo $strings['ESCUELAS']; 
                             ?>
                        </legend>
                    </form>

                    <table class="table table-hover table-striped">
                         <thead class="thead-light">
                            <tr>
                                <th> <!-- Aquí tiene que estar cada escuela-->
                                   <?php echo $strings['Escuela']; ?>
                                </th>
                                <th scope="col">
                                    <?php echo $strings['Fecha']; ?>
                                </th>
                                <th scope="col">
                                    <?php echo $strings['Hora']; ?>
                                </th>
                                <th scope="col">
                                    <?php echo $strings['Participantes']; ?>
                                </th>
                                <th scope="col">
                                    <?php echo $strings['Opciones']; ?>
                                </th>
                             </tr>
                        </thead> 
<?php
            while($row = $datos->fetch_array()){ 
                if($fechafin < $row['fecha']){

?>
                 <tr>
                    <form action="../Controllers/ESCUELA_Controller.php" method="">
                                        
                        <td>
                            <?php 
                            $idEscuela = $row['idEscuela'];

                            $codigo = "Escuela ". $row['idEscuela'];
                            
                            echo $codigo;?>
                        </td> <!-- Escuela + idEscuela -->

                        <td>
                            <?php echo cambiar($row['fecha']); ?>
                        </td> 
                         <td>
                            <?php echo $row['hora']; ?>
                        </td> 
                        <td>
                            <?php echo $row['numPart']; ?>
                        </td> 

                        <td>
                            <?php
                            echo '<a class="btn btn-outline-primary" href=\'../Controllers/ESCUELA_Controller.php?action=Showcurrent&idEscuela=' . $row['idEscuela'] . "'><i class='far fa-eye'></i></a>";

                             if($_SESSION['login'] == $row['loginEnt']){

                                 echo '<a class="btn btn-outline-primary" href=\'../Controllers/ESCUELA_Controller.php?action=Edit&idEscuela='.$row['idEscuela']."'><i class='fas fa-pencil-alt'></i></a>";

                                 echo '<a class="btn btn-outline-danger" href=\'../Controllers/ESCUELA_Controller.php?action=Delete&idEscuela=' . $row['idEscuela'] . "'>
                                    <i class='far fa-trash-alt'></i></a>";
            
                                }else if ($row['numPart'] != '8'){
                                        if($row['login1'] != $_SESSION['login']){
                                            if($row['login2'] != $_SESSION['login']){
                                                if($row['login3'] != $_SESSION['login']){
                                                    if($row['login4'] != $_SESSION['login']){
                                                        if($row['login5'] != $_SESSION['login']){
                                                            if($row['login6'] != $_SESSION['login']){
                                                                if($row['login7'] != $_SESSION['login']){
                                                                    if($row['login8'] != $_SESSION['login']){
                                                                        
                                    echo '<a class="btn btn-outline-primary" href=\'../Controllers/ESCUELA_Controller.php?action=AddPlayer&idEscuela=' . $row['idEscuela'] . '&login1=' . $_SESSION['login'] . '&numPart=' . $row['numPart'] ."'>
                                        <i class='far fa-plus-square'></i></a>";
                                                }   }   }   }   }   }   }   }
                                            }
                                    if ($row['login1'] == $_SESSION['login'] || 
                                        $row['login2'] == $_SESSION['login'] || $row['login3'] == $_SESSION['login'] || 
                                        $row['login4'] == $_SESSION['login'] || $row['login5'] == $_SESSION['login']|| 
                                        $row['login6'] == $_SESSION['login']|| $row['login7'] == $_SESSION['login'] || 
                                        $row['login8'] == $_SESSION['login']){
                                    echo '<a class="btn btn-outline-danger" href=\'../Controllers/ESCUELA_Controller.php?action=DeletePlayer&idEscuela=' . $row['idEscuela'] . '&login1=' . $row['login1'] . '&login2=' . $row['login2'] . '&login3=' . $row['login3'] . '&login4=' . $row['login4'] . '&numPart=' . $row['numPart'] . '&login5=' . $row['login5']  . '&login6=' . $row['login6']  . '&login7=' . $row['login7']  . '&login8=' . $row['login8'] ."'><i class='far fa-trash-alt'></i></a>";
                                };?>
                        </td>
                    </form>
                </tr>
                    <?php if($_SESSION['login'] == $row['loginEnt'])
                                $entrenador = true;
                            else
                                $entrenador = false;
                            if($entrenador && $cont==0){ ?>
                                <div class="botonAddEscuela">
                                    <a class="btn btn-outline-primary" href='../Controllers/ESCUELA_Controller.php?action=Add'>
                                    <span class='far fa-plus-square'></a>
                                </div>
                            <?php   $cont=1;
                                    } ?>            
<?php
            }else{
                if($_SESSION['login'] == $row['loginEnt']){
?>
         <tr>
                    <form action="../Controllers/ESCUELA_Controller.php" method="">
                                        
                        <td>
                            <?php 
                            $idEscuela = $row['idEscuela'];

                            $codigo = "Escuela ". $row['idEscuela'];
                            
                            echo $codigo;?>
                        </td> <!-- Escuela + idEscuela -->

                        <td>
                            <?php echo cambiar($row['fecha']); ?>
                        </td> 
                         <td>
                            <?php echo $row['hora']; ?>
                        </td> 
                        <td>
                            <?php echo $row['numPart']; ?>
                        </td> 

                        <td>
                            <?php
                            echo '<a class="btn btn-outline-primary" href=\'../Controllers/ESCUELA_Controller.php?action=Showcurrent&idEscuela=' . $row['idEscuela'] . "'><i class='far fa-eye'></i></a>";

                             if($_SESSION['login'] == $row['loginEnt']){

                                 echo '<a class="btn btn-outline-primary" href=\'../Controllers/ESCUELA_Controller.php?action=Edit&idEscuela='.$row['idEscuela']."'><i class='fas fa-pencil-alt'></i></a>";

                                 echo '<a class="btn btn-outline-danger" href=\'../Controllers/ESCUELA_Controller.php?action=Delete&idEscuela=' . $row['idEscuela'] . "'>
                                    <i class='far fa-trash-alt'></i></a>";
            
                                }else if ($row['numPart'] != '8'){
                                        if($row['login1'] != $_SESSION['login']){
                                            if($row['login2'] != $_SESSION['login']){
                                                if($row['login3'] != $_SESSION['login']){
                                                    if($row['login4'] != $_SESSION['login']){
                                                        if($row['login5'] != $_SESSION['login']){
                                                            if($row['login6'] != $_SESSION['login']){
                                                                if($row['login7'] != $_SESSION['login']){
                                                                    if($row['login8'] != $_SESSION['login']){
                                                                        
                                    echo '<a class="btn btn-outline-primary" href=\'../Controllers/ESCUELA_Controller.php?action=AddPlayer&idEscuela=' . $row['idEscuela'] . '&login1=' . $_SESSION['login'] . '&numPart=' . $row['numPart'] ."'>
                                        <i class='far fa-plus-square'></i></a>";
                                                }   }   }   }   }   }   }   }
                                            }
                                    if ($row['login1'] == $_SESSION['login'] || 
                                        $row['login2'] == $_SESSION['login'] || $row['login3'] == $_SESSION['login'] || 
                                        $row['login4'] == $_SESSION['login'] || $row['login5'] == $_SESSION['login']|| 
                                        $row['login6'] == $_SESSION['login']|| $row['login7'] == $_SESSION['login'] || 
                                        $row['login8'] == $_SESSION['login']){
                                    echo '<a class="btn btn-outline-danger" href=\'../Controllers/ESCUELA_Controller.php?action=DeletePlayer&idEscuela=' . $row['idEscuela'] . '&login1=' . $row['login1'] . '&login2=' . $row['login2'] . '&login3=' . $row['login3'] . '&login4=' . $row['login4'] . '&numPart=' . $row['numPart'] . '&login5=' . $row['login5']  . '&login6=' . $row['login6']  . '&login7=' . $row['login7']  . '&login8=' . $row['login8'] ."'><i class='far fa-trash-alt'></i></a>";
                                };?>
                        </td>
                    </form>
                </tr>





<?php
            }
            }
        }//Fin while
?>                               
            </table>
        </div>
<?php            
            include '../Views/Footer.php'; //Incluye el pie de página
?>
        </html>
<?php

    }//fin del método render
}//Fin REGISTER

?>