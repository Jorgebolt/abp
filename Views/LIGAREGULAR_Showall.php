<?php

class LIGAREGULAR_Showall
{
    function __construct($datos)
    {
        $this->render($datos);
    }

    function render($datos)
    {

    include '../Locales/Strings_' . $_SESSION['idioma'] . '.php'; //Strings de cambio de idioma

    ?>

        <body>
            <div class="container">
                <form action="../Controllers/LIGAREGULAR_Controller.php" method="">
                    <legend>
                        <?php $numeroCamp = $datos->num_rows ?>
                        <?php echo $strings['LIGAS REGULARES'] . ' (' . $numeroCamp . ')'; ?>
                        
                    </legend>
                </form>
            </div>
        </body>

        <table class="table table-hover table-striped">
            <thead class="thead-light">
                <tr>
                    <th>
                        <!-- Aquí tiene que estar cada visita-->
                        <?php echo $strings['Liga']; ?>
                    </th>
                    <th scope="col">
                        <?php echo $strings['Campeonato']; ?>
                    </th>
                    <th scope="col">
                        <?php echo $strings['Opciones']; ?>
                    </th>
                </tr>
            </thead>
            <?php
                    while ($row = $datos->fetch_array()) {
                        ?>
                <tr>
                    <form action="../Controllers/LIGAREGULAR_Controller.php" method="">

                        <td>
                            <?php
                                        $codigoLiga = "Liga " . $row['idLiga'];

                                        echo $codigoLiga;
                                        ?>
                        </td> <!-- Liga + idLiga -->

                        <td>
                            <?php
                                        $codigoCampeonato = "Campeonato " . $row['idCampeonato'];

                                        echo $codigoCampeonato;
                                        ?>
                        </td> <!-- Campeonato + idCampeonato -->

                        <td>
                            <?php echo '<a class="btn btn-outline-primary" href=\'../Controllers/LIGAREGULAR_Controller.php?action=Showcurrent&idLiga=' . $row['idLiga'] . "'><i class='far fa-eye'></i></a>";?>
                        </td>
                    </form>
                </tr>
            <?php
                    } //Fin while
                    ?>
        </table>

        <a href="../Controllers/CAMPEONATO_Controller.php?action=Showall" class="btn btn-outline-secondary"><span class="fas fa-undo-alt"></a>

        </html>
<?php
        include '../Views/Footer.php';
    } //fin metodo render
} //fin clase
?>