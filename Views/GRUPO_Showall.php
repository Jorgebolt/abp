<?php

class GRUPO_Showall
{
    function __construct($grupos, $gruposGenerados, $playoffsGenerados)
    {
        $this->render($grupos, $gruposGenerados, $playoffsGenerados);
    }

    function render($grupos, $gruposGenerados, $playoffsGenerados)
    {
?>
        <html>
        <body>
            <div class="container">
                <form action="../Controllers/GRUPO_Controller.php" method="">
                    <legend>
                        <?php $numeroGrupos = $grupos->num_rows ?>
                        <?php echo 'GRUPOS - FASE LIGA REGULAR' . ' (' . $numeroGrupos . ')'; ?>
                    </legend>
                </form>
            </div>
        </body>
        <table class="table table-hover table-striped">
            <thead class="thead-light">
                <tr>
                    <th>
                        <?php echo 'Grupo';?>
                    </th>
                    <th scope="col">
                        <?php echo 'Categoria';?>
                    </th>
                    <th scope="col">
                        <?php echo 'Nivel';?>
                    </th>
                    <th scope="col">
                        <?php echo 'Opciones';?>
                    </th>
                </tr>
            </thead>
<?php
            while ($row = $grupos->fetch_array()) {
?>
                <tr>
                    <form action="../Controllers/GRUPO_Controller.php" method="">

                        <td>
                            <?php echo 'Grupo ' . $row['idGrupo'];?>
                        </td>

                        <td>
                            <?php echo $row['categoria'];?>
                        </td>

                        <td>
                            <?php echo $row['nivel'];?>
                        </td>

                        <td>
                            <?php 
                                //si hay menos de 8 parejas y ya se generaron los grupos, NO se juega el grupo
                                if($row['numParejas'] < 8 && $gruposGenerados == 'S'){
                                    echo 'NO HAY 8 PAREJAS';

                                }else{ //si hay 8 o mas parejas, se juega

                                    if($gruposGenerados == 'S'){ //si ya se generaron los grupos
                                        //solo puede apuntar un resultado mientras no hayan llegado los playoffs
                                        if($playoffsGenerados == 'N'){
                                            //Boton APUNTAR RESULTADO
                                            echo '<a class="btn btn-outline-primary" href=\'../Controllers/GRUPO_Controller.php?action=Showcurrent&idGrupo=' . $row['idGrupo'] . '&idCampeonato=' . $row['idCampeonato'] . '&categoria=' . $row['categoria'] . '&nivel=' . $row['nivel'] . "'><i class='far fa-edit'></i></a>";
                                        }
                                        
                                        //Boton CLASIFICACION
                                        echo '<a class="btn btn-outline-primary" href=\'../Controllers/GRUPO_Controller.php?action=Clasificacion&idGrupo=' . $row['idGrupo'] . '&idCampeonato=' . $row['idCampeonato'] . '&categoria=' . $row['categoria'] . '&nivel=' . $row['nivel'] . "'><i class='fas fa-sort-amount-down'></i></a>";
                                    }
                                }
                            ;?>
                        </td>
                    </form>
                </tr>
<?php
            } //Fin while
?>
        </table>
<?php
    } //fin metodo render
} //fin clase
?>