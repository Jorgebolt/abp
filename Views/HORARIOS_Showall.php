<?php

class HORARIOS_Showall
{
    function __construct($datos)
    {
        $this->render($datos);
    }

    function render($datos)
    {
        ?>
        <html>
        <?php
                include '../Views/Header.php'; //Incluye la cabecera

                ?>
        <div class="container">
            <form action="../Controllers/CLASESPARTICULARES_Controller.php" method="">
                <legend>

                    <?php echo $strings['HORARIOS']; ?>
                    
                        </legend>
                    </form>

                    <table class="table table-hover table-striped">
                         <thead class="thead-light">
                            <tr>
                                
                                <th> 
                                   <?php echo $strings['Hora inicio']; ?>
                                </th>
                                <th> 
                                   <?php echo $strings['Hora fin']; ?>
                                </th>
                                <th scope="col">
                                    <?php echo $strings['Opcion de reservar']; ?>
                                </th>
                                
                             </tr>
                        </thead> 
<?php
            while($row = $datos->fetch_array()){ 
?>
                 <tr>
                    <form action="../Controllers/CLASESPARTICULARES_Controller.php" method="">
                         
                        <td>
                            <?php echo $row['horaInicio']; ?>
                        </td>

                        <td>
                            <?php 
                            
                            
                            echo $row['horaFin'];?>
                        </td> <!-- Partido + idPartido -->

                        <td>
                            <?php 
                                    echo '<a class="btn btn-outline-primary" href=\'../Controllers/CLASESPARTICULARES_Controller.php?action=add&horaInicio=' . $row['horaInicio'] . '&fecha=' . $row['fecha'] . "'>
                                    <i class='far fa-plus-square'></i></a>";
                                            


                                            ?>
                            </td>

                        </form>
                    </tr>
                <?php
                        } //Fin while
                        ?>
            </table>
        </div>
        <?php
                include '../Views/Footer.php'; //Incluye el pie de página
                ?>

        </html>
<?php

    } //fin del método render
} //Fin REGISTER

?>