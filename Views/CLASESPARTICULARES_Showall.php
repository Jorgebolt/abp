<?php

class CLASESPARTICULARES_Showall
{
    function __construct($datos,$datos2)
    {
        $this->render($datos,$datos2);
    }

    function render($datos,$datos2)
    {
        ?>
        <html>
        <?php
                include '../Views/Header.php'; //Incluye la cabecera
                

                ?>
        <div class="container">
            <form action="../Controllers/CLASESPARTICULARES_Controller.php" method="">
                <legend>

                    <?php
                    $cont = 0;
                    $deportista = false;
                     echo $strings['CLASES PROGRAMADAS'];  ?>
                    
                        </legend>
                    </form>

                    <table class="table table-hover table-striped">
                         <thead class="thead-light">
                            <tr>
                                
                                <th> 
                                   <?php echo $strings['Entrenador']; ?>
                                </th>
                                <th> 
                                   <?php echo $strings['Alumno']; ?>
                                </th>
                                <th> 
                                   <?php echo $strings['Fecha']; ?>
                                </th>
                                <th> 
                                   <?php echo $strings['Hora']; ?>
                                </th>
                                <th> 
                                   <?php echo $strings['Pista']; ?>
                                </th>
                                <th scope="col">
                                    <?php echo $strings['Opciones']; ?>
                                </th>
                                
                             </tr>
                        </thead> 
<?php
            while($row = $datos->fetch_array()){ 
                if(($_SESSION['login'] == $row['entrenador']) || ($_SESSION['login'] == $row['loginParticipante']) || ($_SESSION['login'] == 'root') ){
?>
                 <tr>
                    <form action="../Controllers/CLASESPARTICULARES_Controller.php" method="">
                         
                        <td>
                            <?php echo $row['entrenador']; ?>
                        </td>

                        <td>
                            <?php echo $row['loginParticipante']; ?>
                        </td>

                        <td>
                            <?php 
                            
                            
                            echo $row['fecha'];?>
                        </td> <!-- Partido + idPartido -->

                        <td>
                            <?php echo $row['hora']; ?>
                        </td>

                        <td>
                            <?php echo $row['idPista']; ?>
                        </td>

                        <td>
                            <?php 


                            

                            
                                
                                    echo '<a class="btn btn-outline-danger" href=\'../Controllers/CLASESPARTICULARES_Controller.php?action=delete&idClase=' . $row['idClase'] . "'>

                                    <i class='far fa-trash-alt'></i></a>";
                                    
                                            
                                
                                
                            
                                            ?>

                            <?php

                                    
                                            


                                            ?>

                            </td>

                        </form>
                    </tr>
                <?php

                


            }

            while($row2 = $datos2->fetch_array()){

                            if($_SESSION['login'] == $row2['login'])
                                $deportista = true;
                            else
                                $deportista = false;
                                if($deportista && $cont == 0){ ?>
                                    <div class="botonAddEscuela"><a class="btn btn-outline-primary" href='../Controllers/CLASESPARTICULARES_Controller.php?action=empezar'>
                                    <i class='far fa-plus-square'></i></a></div>
                                    <?php
                                    $cont = 1;
                                            
                                }
                                
                                
                            }
            
                        } //Fin while
                        ?>
            </table>
        </div>
        <?php
                include '../Views/Footer.php'; //Incluye el pie de página
                ?>

        </html>
<?php

    } //fin del método render
} //Fin REGISTER

?>