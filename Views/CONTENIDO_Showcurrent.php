<?php

class CONTENIDO_Showcurrent{

 function __construct($datos)
    {
        $this->render($datos);
    }

    function render($datos){
  
  
        include '../Views/Header.php'; //Incluye la cabecera
        $row = $datos->fetch_array()
    ?> 

  <div class="formRegister">
  
    <h1 class="titulo"><?php echo "CREAR CONTENIDO"?></h1>

  <form action='../Controllers/CONTENIDO_Controller.php' method='post' >

       <div class="form-group row">
      <label class="col-sm-2 col-form-label" for="login"><?php echo $strings['Login'];?>*</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" name="login" readonly  value=  <?php echo $_SESSION['login']; ?>>
        </div>
    </div>
    <div class="form-group row">
      <label class="col-sm-2 col-form-label" for="titulo"><?php echo "Titulo";?>*</label>
        <div class="col-sm-9">
          <input type="text" class="form-control" readonly name="titulo" value=<?php echo $row['titulo']; ?>>
        </div>
    </div>

    <div class="form-group row">
      <label class="col-sm-2 col-form-label" for="titulo"><?php echo "Descripcion";?>*</label>
        <div class="col-sm-9">
          <textarea rows="10" cols="50"  readonly name="descripcion"><?php echo $row['descripcion']; ?></textarea>
        </div>
    </div>

           <input  type="hidden" class="form-control" name="idcontenido" value=<?php echo $row['idcontenido']; ?>>

    <div class="form-group row">
      <label class="col-sm-9 col-form-label"><?php echo $strings['* Indica que los campos son obligatorios'];?></label>
    </div>

      
  </form>
  </div>
  <?php
   include '../Views/Footer.php';
        ?>
        </html>
        <?php
  } //fin metodo render

} //fin REGISTER

?>
