<?php

class SOCIO_Showall
{
    function __construct($datos)
    {
        $this->render($datos);
    }

    function render($datos)
    {
        ?>
        <html>
        <?php
                include '../Views/Header.php'; //Incluye la cabecera

                ?>
        <div class="container">
            <form action="../Controllers/SOCIO_Controller.php" method="">
                <legend>

                    <?php echo $strings['SOCIOS']; ?>
                    
                        </legend>
                    </form>

                    <table class="table table-hover table-striped">
                         <thead class="thead-light">
                            <tr>
                                
                                <th> 
                                   <?php echo $strings['IDSocio']; ?>
                                </th>
                                <th> 
                                   <?php echo $strings['LoginUsuario']; ?>
                                </th>
                                
                             </tr>
                        </thead> 
<?php
            while($row = $datos->fetch_array()){ 
?>
                 <tr>
                    <form action="../Controllers/SOCIO_Controller.php" method="">
                         

                    
                        <td>
                            <?php echo $row['idSocio']; ?>
                        </td>

                        <td>
                            <?php 
                            
                            
                            echo $row['loginUsuario'];?>
                        </td> <!-- Partido + idPartido -->

                        <td>
                            <?php 
                                    echo '<a class="btn btn-outline-danger" href=\'../Controllers/SOCIO_Controller.php?action=delete&idSocio=' . $row['idSocio'] . "'>

                                    <i class='far fa-trash-alt'></i></a>";
                                            


                                            ?>
                            </td>

                        </form>
                    </tr>
                <?php
                        } //Fin while
                        ?>
            </table>
        </div>
        <?php
                include '../Views/Footer.php'; //Incluye el pie de página
                ?>

        </html>
<?php

    } //fin del método render
} //Fin REGISTER

?>