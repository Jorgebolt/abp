<?php

class GRUPO_Clasificacion
{
    function __construct($parejas)
    {
        $this->render($parejas);
    }

    function render($parejas)
    {
?>
        <html>
<?php
        include '../Views/Header.php'; //Incluye la cabecera
?>
        <html>
        <body>
            <div class="container">
                <form action="../Controllers/GRUPO_Controller.php" method="">
                    <legend>
                        <h3><?php echo 'Grupo ' . $_REQUEST['idGrupo'] . ' (Campeonato ' . $_REQUEST['idCampeonato'] . ')';?></h3>
                        <?php echo 'Clasificacion - Fase Liga Regular'; ?>
                    </legend>
                </form>
            </div>
        </body>
        <table class="table table-hover table-striped">
            <thead class="thead-light">
                <tr>
                    <th>
                        <?php echo 'Posicion';?>
                    </th>
                    <th scope="col">
                        <?php echo 'Pareja';?>
                    </th>
                    <th scope="col">
                        <?php echo 'Puntos';?>
                    </th>
                </tr>
            </thead>
<?php
            $i=1;
            while ($row = $parejas->fetch_array()) {
?>
                <tr>
                    <td>
                        <?php if($row['login1'] == $_SESSION['login'] || $row['login2'] == $_SESSION['login']){
                                echo '<b>#' . $i . '</b>';
                                $i = $i + 1;
                            }else{
                                echo '#' . $i;
                                $i = $i + 1;
                            }
                        ?>
                    </td>
                    <td>
                        <?php if($row['login1'] == $_SESSION['login'] || $row['login2'] == $_SESSION['login']){
                                echo '<b>' . $row['login1'] . ' - ' . $row['login2'] . '</b>';
                            }else{
                                echo $row['login1'] . ' - ' . $row['login2'];
                            }
                        ?>
                    </td>

                    <td>
                        <?php if($row['login1'] == $_SESSION['login'] || $row['login2'] == $_SESSION['login']){
                                echo '<b>' . $row['puntosLiga'] . '</b>';
                            }else{
                                echo $row['puntosLiga'];
                            }
                        ?>
                    </td>
                </tr>
<?php
            } //Fin while
?>
        </table>
<?php
                //Boton CLASIFICACION
                echo '<a class="btn btn-outline-secondary" href=\'../Controllers/CAMPEONATO_Controller.php?action=Showcurrent&idCampeonato=' . $_REQUEST['idCampeonato'] . "'><i class='fas fa-undo-alt'></i></a>";

        include '../Views/Footer.php';
    } //fin metodo render
} //fin clase
    ?>