<?php

class PLAYOFF_Enfrentamientos
{
    function __construct($parejas, $estaInscrito, $parejasCruce1, $parejasCruce2, $parejasCruce3, $parejasCruce4)
    {
        $this->render($parejas, $estaInscrito, $parejasCruce1, $parejasCruce2, $parejasCruce3, $parejasCruce4);
    }

    function render($parejas, $estaInscrito, $parejasCruce1, $parejasCruce2, $parejasCruce3, $parejasCruce4)
    {

        ?>
        <html>
<?php
    include '../Views/Header.php'; //Incluye la cabecera
?>
    <body>
        <div class="container">
            <form action="../Controllers/CAMPEONATO_Controller.php" method="">
                <legend>
                    <?php echo 'PLAYOFF' . ' (CAMPEONATO ' . $_REQUEST['idCampeonato'] . ' - GRUPO ' . $_REQUEST['idGrupo'] . ')'; ?>
                </legend>

                <table class="table table-hover table-striped">
                    <thead class="thead-light">
                        <tr>
                            <th>
                                <!-- Aquí tiene que estar cada visita-->
                                <?php echo 'Posicion Liga'; ?>
                            </th>
                            <th>
                                <!-- Aquí tiene que estar cada visita-->
                                <?php echo 'Parejas'; ?>
                            </th>
                        </tr>
                    </thead>
<?php
                    while ($row = $parejas->fetch_array()) {
?>
                        <tr>
                            <td>
                                <?php
                                    if($_SESSION['login'] == $row['login1'] || $_SESSION['login'] == $row['login2']){
                                        echo '<b>#' . $row['posPlayoff'] . '</b>';
                                    }else{ 
                                        echo '#' . $row['posPlayoff'];
                                    }
                                ?>
                            </td>
                            <td>
                                <?php
                                    if($_SESSION['login'] == $row['login1'] || $_SESSION['login'] == $row['login2'])
                                        echo '<b>' . $row['login1'] . ' - ' . $row['login2'] . ' (' . $row['puntosLiga'] . ' ptos)</b>';
                                    else
                                        echo $row['login1'] . ' - ' . $row['login2'] . ' (' . $row['puntosLiga'] . ' ptos)';
                                ?>
                            </td>
                        </tr>
<?php
                    }//Fin while
?>
                </table>
<?php 
                //Boton VOLVER
                echo '<a class="btn btn-outline-secondary" href=\'../Controllers/CAMPEONATO_Controller.php?action=Showcurrent&idCampeonato=' . $_REQUEST['idCampeonato'] . "'><i class='fas fa-undo-alt'></i></a>";
?>
            </form>

            <form action="../Controllers/PLAYOFF_Controller.php?action=RegistrarResultadoJornada1&idCampeonato=<?php echo $_REQUEST['idCampeonato']?>&idGrupo=<?php echo $_REQUEST['idGrupo'];?>&idPlayoff=<?php echo $_REQUEST['idPlayoff'];?>" method="post">
                <legend>
                    <?php echo 'CRUCES - FASE 1 PLAYOFF'; ?>
                </legend>
<?php
                while ($row = $parejasCruce1->fetch_array()) {

                    //if ($_SESSION['login'] == $row['login1'] || $_SESSION['login'] == $row['login2']) {
                    if($row['posPlayoff'] == 1) {
                        echo '<h4>' . $row['login1'] . ' - ' . $row['login2'] . ' VS ';
                        $cruce1_pareja1 = $row['login1'] . ' - ' . $row['login2'];
                        
                    } else if ($row['posPlayoff'] == 8) {
                        echo $row['login1'] . ' - ' . $row['login2'] . '</h4>';
                        $cruce1_pareja2 = $row['login1'] . ' - ' . $row['login2'];

                    }else{
                        continue;
                    }
                } //Fin while
?>
                <div class="form-check">
                        <input class="form-check-input" type="radio" name="cruce1_ganadora" value="<?php echo $cruce1_pareja1;?>"><?php echo $cruce1_pareja1;?><br>
                        <input class="form-check-input" type="radio" name="cruce1_ganadora" value="<?php echo $cruce1_pareja2;?>"><?php echo $cruce1_pareja2;?><br><br>
<?php
                        while ($row = $parejasCruce2->fetch_array()) {
                            //if ($_SESSION['login'] == $row['login1'] || $_SESSION['login'] == $row['login2']) {
                            if($row['posPlayoff'] == 2) {
                                echo '<h4>' . $row['login1'] . ' - ' . $row['login2'] . ' VS ';
                                $cruce2_pareja1 = $row['login1'] . ' - ' . $row['login2'];
                                
                            } else if ($row['posPlayoff'] == 7) {
                                echo $row['login1'] . ' - ' . $row['login2'] . '</h4>';
                                $cruce2_pareja2 = $row['login1'] . ' - ' . $row['login2'];

                            }else{
                                continue;
                            }
                        } //Fin while
?>
                        <input class="form-check-input" type="radio" name="cruce2_ganadora" value="<?php echo $cruce2_pareja1;?>"><?php echo $cruce2_pareja1;?><br>
                        <input class="form-check-input" type="radio" name="cruce2_ganadora" value="<?php echo $cruce2_pareja2;?>"><?php echo $cruce2_pareja2;?><br><br>
<?php
                        while ($row = $parejasCruce3->fetch_array()) {
                            //if ($_SESSION['login'] == $row['login1'] || $_SESSION['login'] == $row['login2']) {
                            if($row['posPlayoff'] == 3) {
                                echo '<h4>' . $row['login1'] . ' - ' . $row['login2'] . ' VS ';
                                $cruce3_pareja1 = $row['login1'] . ' - ' . $row['login2'];
                                
                            } else if ($row['posPlayoff'] == 6) {
                                echo $row['login1'] . ' - ' . $row['login2'] . '</h4>';
                                $cruce3_pareja2 = $row['login1'] . ' - ' . $row['login2'];

                            }else{
                                continue;
                            }
                        } //Fin while
?>
                        <input class="form-check-input" type="radio" name="cruce3_ganadora" value="<?php echo $cruce3_pareja1;?>"><?php echo $cruce3_pareja1;?><br>
                        <input class="form-check-input" type="radio" name="cruce3_ganadora" value="<?php echo $cruce3_pareja2;?>"><?php echo $cruce3_pareja2;?><br><br>
<?php
                        while ($row = $parejasCruce4->fetch_array()) {
                            //if ($_SESSION['login'] == $row['login1'] || $_SESSION['login'] == $row['login2']) {
                            if($row['posPlayoff'] == 4) {
                                echo '<h4>' . $row['login1'] . ' - ' . $row['login2'] . ' VS ';
                                $cruce4_pareja1 = $row['login1'] . ' - ' . $row['login2'];
                                
                            } else if ($row['posPlayoff'] == 5) {
                                echo $row['login1'] . ' - ' . $row['login2'] . '</h4>';
                                $cruce4_pareja2 = $row['login1'] . ' - ' . $row['login2'];

                            }else{
                                continue;
                            }
                        } //Fin while
?>
                        <input class="form-check-input" type="radio" name="cruce4_ganadora" value="<?php echo $cruce4_pareja1;?>"><?php echo $cruce4_pareja1;?><br>
                        <input class="form-check-input" type="radio" name="cruce4_ganadora" value="<?php echo $cruce4_pareja2;?>"><?php echo $cruce4_pareja2;?><br><br>
<?php               
                        if($_SESSION['login'] == 'root'){
                            //Boton para REGISTRAR RESULTADO
                            //echo '<button class="btn btn-outline-primary" name="action" value="RegistrarResultadoJornada1"><span class="far fa-edit"></span></button>';
                            echo '<button class="btn btn-outline-primary" type="submit" name="action" value="RegistrarResultadoJornada1">Registrar Jornada 1</span></button>';
                        }
?>
                </div>
            </form>
        </div>
    </body>
<?php
        include '../Views/Footer.php';
    } //fin metodo render
} //fin clase
?>