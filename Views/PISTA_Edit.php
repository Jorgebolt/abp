<?php

  class PISTA_Edit{


    function __construct($valores){ 
      $this->render($valores);
    }

    function render($valores){

            include '../Views/Header.php'; //Incluye la cabecera
            
?>
    
          <div class="formAddPlayer">
              <legend class="titulo"><?php echo $strings['Edita los datos de la pista']; ?></legend>

              <form name ='edit' id="edit" action='../Controllers/PISTA_Controller.php?action=Edit' method='post'>

                <div class="form-group row">
                      <label class="col-sm-3 col-form-label" for="idPista"><strong><?php echo $strings['Pista']; ?>:</strong></label>
                      <div>
                          <input type="text" class="form-control-plaintext" name="idPista" readonly
                            value="<?php echo $valores['idPista']; ?>">
                      </div>
                  </div>

                 <div class="form-group row">
                    <label class="col-sm-2 col-form-label" for="descripcion"><?php echo "Descripcion";?>:</label>
                    <div class="col-sm-9">
                      <textarea rows="10" cols="50" name="descripcion" value= "<?php echo $valores['descripcion']; ?>" >
                        <?php echo $valores['descripcion']; ?>
                      </textarea>
                    </div>
                  </div>

                <div class="boton">
                  <button class="btn btn-outline-secondary" type="submit" name="action" value="Showall"><span class="fas fa-undo-alt"></span></button>
                  <button type="submit" class="btn btn-outline-primary"><?php echo $strings['Añadir'];?></button>
                </div>
              </form>
          </div>
<?php
        include '../Views/Footer.php';//Incluye el pie de página
?>
    </html>
<?php
    }//fin del método render
}//Fin REGISTER

?>