<?php

class HORARIOS_View{
    function __construct($datos)
{
$this->render($datos);
}

function render($datos){  
?>
        <html>
<?php
        include '../Views/Header.php'; //Incluye la cabecera

?>    
                <div class="container">
                    <form action="../Controllers/RESERVAPISTA_Controller.php" method="">
                        <legend>
                            <?php $numeroPistas = $datos->num_rows ?>
                            <?php echo $strings['PISTAS'] . ' (' . $numeroPistas . ')' ; ?>
                            <a class="btn btn-outline-primary" href='../Controllers/RESERVAPISTA_Controller.php?action=ver'><span class='far fa-plus-square'></a>
                        </legend>
                    </form>

                    <table class="table table-hover table-striped">
                         <thead class="thead-light">
                            <tr>
                                <th> 
                                   <?php echo $strings['Pista']; ?>
                                </th>
                                <th scope="col">
                                    <?php echo $strings['Hora de mañana']; ?>
                                </th>
                                <th scope="col">
                                    <?php echo $strings['Hora de tarde']; ?>
                                </th>
                                <th scope="col">
                                    <?php echo $strings['Hora de noche']; ?>
                                </th>
                                <th scope="col">
                                    <?php echo $strings['Opciones']; ?>
                                </th>
                             </tr>
                        </thead> 
<?php
            while($row = $datos->fetch_array()){ 
?>
                 <tr>
                    <form action="../Controllers/RESERVAPISTA_Controller.php" method="">
                                        
                        <td>
                            <?php 
                            $idPartido = $row['idPista'];

                            $codigo = "Pista ". $row['idPista'];
                            
                            echo $codigo;?>
                        </td> <!-- Partido + idPartido -->

                        <td>
                            <?php echo $row['hora1']; ?>
                        </td> 

                        <td>
                            <?php echo $row['hora2']; ?>
                        </td> 
                        <td>
                            <?php echo $row['hora3']; ?>
                        </td> 
                        <td>
                            <?php 
                            //Aqui hay que poner un boton para mañana,tarde o noche
                            ?>
                                
                        </td>

                        
                    </form>
                </tr>
<?php
            }//Fin while
?>                               
            </table>
        </div>
<?php            
            include '../Views/Footer.php'; //Incluye el pie de página
?>
        </html>
<?php

    }//fin del método render
}//Fin REGISTER

?>