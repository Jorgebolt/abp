<?php

class GRUPO_Showcurrent
{
    function __construct($parejasFila, $parejasColumna, $idCampeonato, $idGrupo, $estaInscrito)
    {
        $this->render($parejasFila, $parejasColumna, $idCampeonato, $idGrupo, $estaInscrito);
    }

    function render($parejasFila, $parejasColumna, $idCampeonato, $idGrupo, $estaInscrito)
    {
?>
        <html>
        <?php
        include '../Views/Header.php'; //Incluye la cabecera

        function cambiar($fecha)
        { //Función para cambiar el formato de la fecha
            $mifecha = explode("-", $fecha);
            $lafecha = $mifecha[2] . "/" . $mifecha[1] . "/" . $mifecha[0];
            return $lafecha;
        }

        //funcion que controla si se alcanzo la fecha limite de inscripcion de un campeonato
        function limiteAgotado($fechaLimite)
        {
            $hoy = date("d/m/Y", time());
            $parseLimite = cambiar($fechaLimite);

            if ($hoy < $parseLimite) {
                return false;
            } else {
                return true;
            }
        }
        ?>
        <div class="container">
            <h2><?php echo 'Grupo' . ' ' . $idGrupo . ' - Registrar Enfrentamiento Liga Regular'; ?></h2>
            <form action="../Controllers/GRUPO_Controller.php?action=ApuntarGanador&idCampeonato=<?php echo $idCampeonato;?>&idGrupo=<?php echo $idGrupo;?>" method="post">
            <br>    
            <h5>TU PAREJA:</h5>
                <select name="parejaUser">
<?php
                while ($row = $parejasFila->fetch_array()) {
?>
                    <?php if ($row['login1'] == $_SESSION['login'] or $row['login2'] == $_SESSION['login']) {
                        $parejaUser = $row['login1'] . '-' . $row['login2'];
                        echo '<option value=' . $parejaUser . '>' . $row['login1'] . ' - ' . $row['login2'] . '</option>';
                    } else {
                        continue;
                    }?>
<?php
                }
?>
                </select>
                <br>
                <br>
                <h5>PAREJA RIVAL:</h5>
                <select name="parejaRival">
                    <option value="">Selecciona una pareja</option>
<?php
                while ($column = $parejasColumna->fetch_array()) {
?>
                    <?php if ($column['login1'] == $_SESSION['login'] or $column['login2'] == $_SESSION['login']) {
                        continue;
                    } else {
                        $parejaRival = $column['login1'] . '-' . $column['login2'];
                        echo '<option value=' . $parejaRival . '>' . $column['login1'] . ' - ' . $column['login2'] . '</option>';
                    } ?>
<?php
                }
?>
            </select>
            <br>
            <br>
            <!-- 
            <button  type="submit" value="submit">
                <i class='far fa-edit'></i>
            </button>
            -->
<?php
            //Boton APUNTAR GANADOR
            echo '<button class="btn btn-outline-primary"><span class="far fa-edit"></span></button>';

            //Boton para VOLVER al showcurrent del campeonato
            echo '<a class="btn btn-outline-secondary" href=\'../Controllers/CAMPEONATO_Controller.php?action=Showcurrent&idCampeonato=' . $idCampeonato . "'><i class='fas fa-undo-alt'></i></a>";
?>
        </form>
    <?php
            include '../Views/Footer.php';
        
    } //fin metodo render
} //fin clase
    ?>