<?php
class CAMPEONATO_Add
{
    function __construct()
    {
        $this->render();
    }

    function render()
    {
        include '../Views/Header.php';
        ?>
    <div class="addPartido">
        <form name='añadir' id="añadir" action='../Controllers/CAMPEONATO_Controller.php?action=Add' method='post' enctype="multipart/form-data" onsubmit="return validarADD(this)">
            <fieldset>
                <legend><?php echo $strings['Añadir Campeonato']; ?></legend>
                <br>

                <div class="form-group row">
                    <label for="fechaLimiteInscrip" class="col-sm-2 col-form-label"><?php echo $strings['Fecha Limite Inscripcion']; ?>*</label>
                    <div class="col-sm-10">
                        <input class="form-control tcal tcalInput" type="text" name="fechaLimiteInscrip" id="fechaLimiteInscrip" readonly placeholder="<?php echo $strings['haz click para cambiar la fecha']; ?>">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-sm-9 col-form-label"><?php echo $strings['* Indica que los campos son obligatorios']; ?></label>
                </div>
                <button type="submit" class="btn btn-outline-primary">
                    <span class="fas fa-check"></span>
                </button>
                <a class="btn btn-outline-secondary" href="../Controllers/CAMPEONATO_Controller.php?action=Showall"><i class="fas fa-undo"></i></a>
            </fieldset>
        </form>
    </div>
<?php
        include '../Views/Footer.php';
    }
}
?>