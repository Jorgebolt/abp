 <?php
    class CAMPEONATO_SeleccionarPareja
    {
        function __construct($datos, $idCampeonato,$nivel)
        {
            $this->render($datos, $idCampeonato,$nivel);
        }

        function render($datos, $idCampeonato,$nivel)
        {
            ?>
         <html>
         <?php
                    include '../Views/Header.php'; //Incluye la cabecera
                    ?>

         <body>
             <legend>

                 <?php echo "Selecciona Pareja"  ?>

             </legend>
             <table class="table table-hover table-striped">
                 <thead class="thead-light">
                     <tr>
                         <th>
                             <!-- Aquí tiene que estar cada visita-->
                             <?php echo "Login"; ?>
                         </th>

                         <th scope="col">
                             <!-- Aquí tiene que estar cada visita-->
                             <?php echo "Seleccionar"; ?>
                         </th>
                     </tr>
                 </thead>

<?php while ($row = $datos->fetch_array()) {
                    if ($_SESSION['login'] == $row['login'] || $row['login'] == 'root') {
                        continue;
                    }
?>

                     <tr>
                         <form action="../Controllers/USUARIO_Controller.php" method="">
                             <td>
                                 <?php
                                    $codigo = $row['login'];
                                    echo $codigo;
                                ?>
                             </td>

                             <td>
                                 <a class="btn btn-outline-primary" href=\../Controllers/CAMPEONATO_Controller.php?action=Inscribirse&login=<?php echo $row['login']; ?>&nivel=<?php echo $nivel ;?>&idCampeonato=<?php echo $idCampeonato ?>>
                                    <i class='fas fa-pencil-alt'></i>
                                </a>
                             </td>
                         </form>
                     </tr>
<?php
            } //Fin while
?>
             </table>
         </body>

        </html>
 <?php
        }
    }
?>