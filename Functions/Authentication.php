<?php
/*
Fichero PHP para comprobar si existe la variable de Session login
Autor: Mario Pérez Montenegro (Alias: ies2xh)
Fecha Creación: 17/11/18
*/

function IsAuthenticated(){//Función que comprueba si existe la variable de session login. Si existe devuelve true y sino devuelve false y redirige a la página de login
	if (!isset($_SESSION['login'])){ //Si no existe la variable de session login
		return false;
	}
	else{ //Si existe la variable de sesion login
		return true;
	}
}
?>

