<?php
/*
	Archivo PHP con la función que realiza la desconexión de la sesión
	Autor: Mario Pérez Montenegro (Alias: ies2xh)
	Fecha de creación: 20/11/2018
*/
session_start(); //Se inicia la sesión
session_destroy(); //Se destruye la sesión
header('Location:../index.php'); //Redirige al inicio de la página

?>
