<?php
/*
	Archivo PHP con la función para cambiar el idioma
	Autor:	Mario Pérez Montenegro (Alias: ies2xh)
	Fecha de creación: 20/11/2018 
*/
	session_start(); //Se inicia la sesión
	$idioma = $_POST['idioma']; //Recoge el idioma establecido en una variable
	$_SESSION['idioma'] = $idioma; //Incluye una variable de sesión con el idioma seleccionado
	header('Location:' . $_SERVER["HTTP_REFERER"]); 
?>