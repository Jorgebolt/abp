<!--
Documento php que contiene el código php con un array asociativo para hacer la tradución de la página al inglés
-->
<?php 
$strings = 
array(
	'ET4 IU CaritaAlReves' => 'ET4 IU CaritaAlReves',
	'MANTENIMIENTO S.L. desde 1923' => 'MANTENIMIENTO S.L. since 1923',
	'Nombre de usuario' => 'User name',
	'Añadir Contrato' => 'Add Contract',
	'CIF Empresa' => 'CIF Company',
	'CIF Centro' => 'CIF Centre',
	'Anadir Empresa' => 'Add Company',
	'CERTIFICADOR' => 'CERTIFIER',
	'CERTIFICADORA' => 'CERTIFIER',
	'MANTENIMIENTO' => 'MAINTENANCE',
	'MANTENEDORA' => 'MAINTENANCE',
	'ARREGLO' => 'ARRANGEMENT',
	'Eliminar Empresa' => 'Delete Company',
	'Vista Detallada Empresa' => 'Showcurrent Company',
	'Buscar Empresa' => 'Search Company',
	'Editar Empresa' => 'Edit Company',
	'ARREGLOS' => 'ARRANGEMENTS',
	'Importe' => 'Amount',
	'La categoria del contrato esta en funcion de la empresa' => 'The category of the contract is based on the company',
	'N Visitas' => 'Nº Visits',
	'Codigo'=>'Code',
	'Domicilio Fiscal' => 'Tax residence',
	'Fecha inicio' => 'Start date',
	'Anadir periodo visita'=>'Add visit period',
	'Fecha fin' => 'End date',
	'Categoria' => 'Category',
	'CERTIFICADOR' => 'CERTIFICATOR',
	'MANTENIMIENTO' => 'MAINTENANCE',
	'ARREGLO' => 'ARRANGEMENT',
	'Copia contrato' => 'Contract copy',
	'Estado' => 'State',
	'ACTIVO' => 'ACTIVE',
	'FINALIZADO' => 'FINALIZED',
	'Nº Visitas' => 'Visit number',
	'Privacidad' => 'Privacity',
	'Contacto' => 'Contact',
	'Blog' => 'Blog',
	'Compañía' => 'Company',
	'Hoy es día' => 'Today is',
	'Editar Contrato' => 'Contract Edit',
	'Eliminar Contrato' => 'Remove Contract',
	'¿Estás seguro?' => 'Are you sure?',
	'* Indica que los campos son obligatorios' => '* It remarks that the fields are obligatory',
	'CONTRATOS'=> 'CONTRACTS',
	'Contrato' => 'Contract',
	'Opciones' => 'Options',
	'Código' => 'Code',
	'* Indica que los campos permiten búsqueda parcial' => '* Indicates that the fields allow partial search',
	'Buscar Centro' => 'Search Centre',
	'categoria' => 'Category',
	'Anadir Centro'=>'Add Centre',
	'Vista Detallada del centro' => 'Showcurrent centre',
	'Editar Centro' => 'Edit Centre',
	'Domicilio'=> 'Address',
	'Buscar usuario'=>'Search User',
	'Pulsa en la columna para ordenar por este campo' => 'Click on the column to sort by this field',
	'Vista Detallada Contrato' => 'Showcurrent Contract',
	'Visitas Asociadas' => 'Associated Visits',
	'Código Visita' => 'Visit code',
	'Visitas asociadas' => 'Associated Visits',
	'Fecha Visita' => 'Visit date',
	'Visita Padre' => 'Previous Visit',
	'Resuelta' => 'Resolved',
	'Login' => 'Login',
	'Utiliza tu login' => 'Use your login',
	'Contrasena' => 'Password',
	'Letras y Numeros' => 'Letters and Numbers',
	'Registrarse' => 'Register',
	'Registro' => 'Registration',
	'DNI' => 'DNI',
	'Nombre' => 'Name',
	'Apellidos' => 'Surname',
	'Email' => 'Email',
	'Telefono' => 'Telephone',
	'Autorizador' => 'Autorizator',
	'Datos incorrectos o tu peticion ha sido rechazada' => 'Incorrect data or your application have been refused',
	'Se ha eliminado correctamente' => 'It has been successfully removed',
	'no se ha eliminado' => 'It has not been removed',
	'Edicion relizada con exito' => 'It has been successfully edited',
	'Error en la edicion' => 'Error en la edición',
	'No se ha podido insertar' => 'It has not been added',
	'Insercion realizada correctamente' => 'It has been successfully added',
	'buscar contrato' => 'Search Contract',
	'Si es autorizador no tiene centro asociado' => 'If they are autorizator they have not an associated Centre',
	'Aun no te ha registrado el administrador' => 'You have not been registered by autorizator yet',
	'Peticiones de Registro' => 'Registration Applications',
	'Nombre completo' => 'Full Name',
	'No hay peticiones de registro nuevas' => 'There is not new registration applications',
	'Elige un centro para alta usuario' => 'Choose a centre for adding user',
	'Usuarios' => 'Users',
	'DNI' => 'DNI',
	'Codigo contrato asociado' => 'Associated contract code',
	'haz click para cambiar la fecha'=> 'click to change the date',
	'CENTROS' => 'CENTRES',
	'No tiene visita padre'=> 'There isn´t Previous visit',
	'EMPRESAS' => 'COMPANYS',
	'Pulsa en Estado para ordenar por este campo' => 'Click on Estate for sorting by this field',
	'La categoría del contrato esta en funcion de la empresa' => 'Contract\'s category is associated with CIF Company',
	'Codigo visita asociado' => 'Code Associated Visits',
	'Visita' => 'Visit',
	'USUARIOS' => 'USERS',
	'Codigo visita asociado' => 'Associated visit code',
	'FUERA DE FECHA' => ' OUT OF DATE',
	'VISITAS' => 'VISITS',
	'Editar Usuario' => 'User Edit',
	'Para ordenar la tabla según un campo pulse el la cabecera' => 'To sort the table click on the header according to each field',
	'Añadir visita' => 'Visit Add',
	'Informe' => 'Inform',
	'Resuelta' => 'Resolved',
	'PROGRAMADA' => 'PROGRAMMED',
	'NO PROGRAMADA' => 'NO PROGRAMMED',
	'SI' => 'YES',
	'NO' => 'NO',
	'Fecha inicio periodo de la visita' => 'Start date of visit period',
	'Fecha fin periodo de la visita' => 'End date of visit period',
	'Eliminar visita' => 'Visit Remove',
	'Editar visita' => 'Edit visit',
	'Codigo Visita' => 'Visit code',
	'Buscar visita' => 'Visit Search',
	'Vista detallada de la visita' => 'Showcurrent Visit',
	"Te registraste correctamente" => "You have registered yourself correctly",
	"No te has podido registrar" => "An error have occurred while registering",
	'Vista detallada usuario' => 'Showcurrent User',
	'Eliminar usuario' => 'Remove User',
	'No hay visitas fuera de fecha' => 'No visits out of date',
	'Codigo Contrato:' => 'Contract Code:',
	'ESCUELAS' => 'SCHOOLS',
	'Escuela' => 'School',
	'Nivel' => 'Level',
	'Añadir escuela' => 'Add school',
	'Elija de 1 a 10' => 'Choose from 1 to 10',
	'Vista detallada de la escuela' => 'Detailed view of the school',
	'Alumno' => 'Student',
	'Edita los datos de la escuela' => 'Edit the school data',
	'Edita los datos de la escuela' => 'Edit the floor data',
	'Horarios' => 'Schedule',
	'Descripcion' => 'Description',
	'Añadir pista' => 'Add floor',
 )
;
 ?>
